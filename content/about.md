+++
title = "About"
+++

# About Me

Hi 👋, I’m Mandar Vaze. I’m a software engineer.

I love creating. ~~software, sketches, writing~~   

# About this site

This is my [E/N style blog](/post/e-n-style-blog/).
I called it microblog because the posts are too short and/or informal.

Till I learnt that such blog is called `E/N blog`.
 
Created using :

* hugo
* Theme : [Internet weblog](https://github.com/mandarvaze/hugo-microblog)
  * I've made minor changes to the [original theme](https://github.com/jnjosh/internet-weblog)
  * Earlier : [IndieFeed](https://github.com/mandarvaze/indiefeed) - my fork of now-defuct [indiefeed](https://github.com/dianoetic/indiefeed)
* Hosted on Netlify

# Other blogs

* [Personal Knowledge Management](https://pkm.desipenguin.com/)
* [Learn. Share. Improve](https://learnings.desipenguin.com/)
* [My Sketches](https://learnings.desipenguin.com/galleries/)
