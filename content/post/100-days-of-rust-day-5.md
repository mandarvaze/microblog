---
title: "100 Days of Rust : Day 5"
date: 2024-01-20T07:40:56+05:30
tags: ["rust", "learning", "programming language"]
---

I want to access Bard API via Rust, but it will take some time.

Here are the things I did today.

First, I tried (successfully) to access an URL via Rust code. ✅

I used the `reqwest` module.

I just added this dependency in my `Cargo.toml` and (like in other languages)
the dependency hell was let loose 😄

67 other modules were added to the `Cargo.lock`!

---

I also learnt to use global variables in Rust.

I used `const` (Read the documentation [here](https://doc.rust-lang.org/reference/const_eval.html))

Obviously, it is not as straight forward. 

See this [flowchart](https://uploads.sitepoint.com/wp-content/uploads/2021/06/1624192569flowchart.png)

[Here](https://www.sitepoint.com/rust-global-variables/) is the detailed article.
