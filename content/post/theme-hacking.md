---
title: "Hacking IndieFeed Theme"
tags: ["hacking", "meta"]
date: 2021-12-15T19:52:09+05:30
---

I started with [indiefeed](https://github.com/dianoetic/indiefeed) theme. While it is possible to make visual changes by creating `css/style.css` (as explained in the `README`) I'm thinking of making other changes as well.

So I started by forking the theme. So far my changes are only in `css/style.css`

I'm not a frontend developer. But I know enough HTML/CSS to be dangerous 😆
