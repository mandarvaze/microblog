---
title: "Tmuxinator: Manage complex tmux sessions easily"
date: 2022-01-12T11:50:20+05:30
tags: ["terminal"]
useMermaid: false
---

After using `tmux` for a while, I realized that whenever I create new session locally, I end up opening same set of panes.

- One for work project (Sometimes more than one, but usually one)
- One for some kind of blog (I have three Hugo projects at this time)
- One misc. I start this in `~`, I use this for running `brew` commands or standalone python session, or `bin/doom sync` (which I've been doing from inside doom emacs these days, so the last usecase has gone down)

Turns out *I'm not the only one*™️ with such use case.

There is a tool called `tmuxinator` (what a 🆒 name)

I've created a default profile which starts these three panes when starting a new `tmux` session.

`tmuxinator` has features to do a lot more.

I'll explore them slowly and update my `tmuxinator` config over the time. (Like everyone else 😄)
