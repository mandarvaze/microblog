---
title: "E/N Style Blog"
date: 2023-05-04T21:49:05+05:30
tags: ["meta"]
---

TIL that this is an `E/N style blog`

I didn't know that it was 😄

**E/N** stands for **Everything and Nothing**

The content of this site means *everything* to the publisher (me)
But it may mean *nothing* to some of the users (at some of the times)

Quoting from the origin [site](http://sawv.org/en.html)

> The website's author covers a myriad of topics. It's not narrowly focused.
> The author writes about everything or at least everything that's important
> to the author. The site might contain something useful for anyone who visits.

So at some point, it may mean nothing to you - the reader.

Yet, you are reading this post.

So I thank you 🙇🏽‍♂️