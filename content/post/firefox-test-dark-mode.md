---
title: "Firefox Test Dark Mode"
date: 2022-12-03T21:45:10+05:30
tags: ["webdev"]
---

My previous theme [indiefeed](https://github.com/mandarvaze/indiefeed)
supported both light and darkmode. The current one does not.

I'm in the [process of fixing that](/post/support-both-light-dark-mode/).

But I didn't know how to test it ? i.e. Depending on the time of the day,
Firefox would either select light mode or dark.

Turns out, Developer tool lets you switch it (for that page).

When in `Inspector` tab, look for icons for Sun (light) and moon (dark).

See the diagram below :

![Test Dark Mode](/image/firefox-dev-tools.png)
