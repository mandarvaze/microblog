---
title: "Emacs Bedrock : First impressions"
date: 2023-09-16T10:31:12+05:30
tags: ["emacs"]
---

As mentioned [earlier](/post/emacs-init-dir/) I tried new starter kit called Emacs Bedrock.

Getting started was easy with `--init-directory` option in Emacs 29

I also enabled a few mixins in `init.el` as recommended. Especially evil mode.

The `*Quick Help*` that shows up at the start is very useful. Especially for someone like myself who is not familiar with native emacs keybindings.

Enabling evil mode means I can at least edit the text.

But there are so many thing one does other than editing the text.

e.g. Opening a file, switching buffers.

I'm so much used to mnemonic keybindings (Doom, but spacemacs before that) that
I never learnt native emacs keybindings.

While I could learn emacs keybindings, I don't want to :)

Should I ? 🤔
