---
title: "D2 Layouts"
date: 2022-12-01T09:38:07+05:30
tags: ["D2", "tools"]
---

D2 allows me to generate diagrams from the same source file using different
layouts

It comes with two built-in layout engines :

`Dagre` is the default : 

![Dagre](/image/d2-dagre.svg)

Other one is `ELK` This needs to be specified explicitly as :

`D2_LAYOUT=elk d2 in.d2 out.svg `

and here is the output

![ELK](/image/d2.svg)
