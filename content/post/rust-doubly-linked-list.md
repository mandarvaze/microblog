---
title: "Rust: Doubly Linked List"
date: 2024-11-29T16:14:13+05:30
tags: ["rust"]
---

No, this is not a post about **how** to implement doubly linked list in rust.

Or ir could be a very short post, since doubly linked list in built-in the `std::collection` as `LinkedList`

Following functions are available:

* `new` : Creates new/empty linked list
* `push_back` : Adds an element to the list
* `push_front` : Since this is doubly linked list, element can be added at the
beginning as well. Nice!
* `pop_back` and `pop_front` : Remove an element from the list
* `clear` : empty the entire list
* `append` : Append one list to another to make bigger list. One that got appended becomes empty though.
* `contains` : Searches through the list for the element [^1]
* There are `iter`, `iter_mut` and `len` as well.

[Documentation](https://doc.rust-lang.org/std/collections/
struct.LinkedList.html) is easy to understand

[^1]: Since this is a Linked List, compute time is `O(n)`
