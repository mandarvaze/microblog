---
title: "Clippy"
date: 2024-02-08T10:49:48+05:30
tags: ["rust", "learning", "programming language"]
---

Clippy is a linter for Rust programming language.

If you are annoyed by the compiler (~~shouting at~~ telling you
how your code is wrong), wait till you install and use `clippy` 😄

Jokes apart, why I want to use clippy is it tells us about idiomatic rust
and can autofix issues (if we tell it to do so)

Couple of fun facts I discovered :

* First search result for clippy is not what I was looking for 😄 till I searched for `rust clippy`
* `clippy` can not be installed via `cargo install` (As I tried initially)
  * (As with rest of the rust ecosystem) there was a helpful error message with solution 😇
```shell
 error: Clippy is no longer available via crates.io

 help: please run `rustup component add clippy-preview` instead
```
