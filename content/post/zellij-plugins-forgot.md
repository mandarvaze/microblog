---
title: "Zellij Plugins : zellij-forgot"
date: 2024-06-25T21:40:59+05:30
tags: ["zellij", "terminal"]
externalUrl: https://github.com/karimould/zellij-forgot
---

I didn't even know Zellij had such extensive plugin system.

I'm going to explore more of those in coming days. 

Let me start with `Zellij Forgot`

While the main (original?) purpose was (I assume) to remember various Zellij
Keybindings, it can be extended to remember anything. [^1]

While default/out of the box config will load the existing keybindings, it is
a bit weird. It shows things like `Some(Down, None)` which seems more of Rust
internal code and may not be directly useful to the user.

Instead, (as the README suggested) I added my own "pairs" (This is also given in
the README, I just copy/pasted it)

By default, this **adds** to the existing list.

Not what I wanted.

Luckily, adding `"LOAD_ZELLIJ_BINDINGS" "false"` to the config will disable
automatic keybinding loading.

[^1]: The README has example of `buy eggs` 😄. Personally, I don't think it is
very good example. If you remember to look it up, you'll also remember to buy
eggs anyway. IMO, this is meant for things that one does not use frequently, and
needs to "look up"

