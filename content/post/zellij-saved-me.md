---
title: "How Zellij saved me"
date: 2024-05-23T17:53:15+05:30
tags: ["zellij", "terminal"]
---

*OK, title is a bit of clickbait. It saved me some inconvinence 😉*

Yesterday, Wezterm crashed.

At first, I was worried that all my "work" is lost (few blog posts were in-progress and open in helix)

But I had forgotten that I always start zellij these days. 

So all my "work" was indeed there.

All I had to do was `zellij ls` followed by `zellij a my_session1` (and `my_session2` and so on)

No one expects the terminal to crash [^1]. But if and when it does, you are glad you were using Zellij (or `tmux`)

[^1]: This is not to say Wezterm is unstable. I have been using it for a couple of years, and this is the first time it crashed in recent memory.
