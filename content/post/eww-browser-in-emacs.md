---
title: "text mode browsing in Emacs"
date: 2022-01-05T19:56:56+05:30
tags: ["emacs", "browser"]
---

I'm leaning more and more towards text based workflow.

Earlier I enabled `twittering-mode` in Doom Emacs, and tweeted from Emacs. How 🆒 is that.

But that is when I realized that twitter has lots of URLs. If I visit a URL from my Twitter feed, it opens up in external browser by default.

I have two problems with that.

1. My flow gets disturbed.
2. The browser already has gazillion links open. Once I go there, I get distracted.

Opening links within emacs addresses both these (I think. Too early to comment)

Since `eww` is already included in Emacs 24.4 onwards, no separate installation required.

I added the following to my `config.el`

```lisp
(setq browse-url-browser-function 'eww-browse-url)
```
🎉
