---
title: "Appflowy"
tags: ["exploring"]
date: 2021-12-16T19:50:20+05:30
---

Came across [appflowy](https://www.appflowy.io/) on twitter.
It is an open source alternative to Notion.

I have never used Notion, but everyone and their grandma rave about it.

Why have I not used Notion so far ? Because I did not want to sign up for yet-another SaaS that I know I won't use.

I use Dendron (these days) for my PKM. I use it because it is "local first" - meaning I always have access to my data.

One of the reason I created this microblog is for the same reason. Otherwise, Polywork seems nice for such "What am I doing right now" notes.

What if Polywork goes away one day ? Once bitten twice shy.

Back to Appflowy. Their website does not have enough information whether their Desktop app is UI for remote Saas.
But I would be happy if it is local-first (or local-only) Notion-clone 🤞

----

Update : Tried it by `brew install appflowy`. It is just their second release, and it shows.
Basic editing works (I assumed. Did not try) 
There is a Share button, which I expected will allow sharing in different formats.
It does. For now there are two options :

- Copy link : Says `Work in Progress`
- Markdown : No error, but does not work.

I have uninstalled it. I might come back again later when it has matured.

---

Edit : Appflowy should look at [Logseq](https://microblog.desipenguin.com/post/logseq/). 
Since I have never used Notion, I might call Logseq a Open source alternative to Notion, that is local-first
