---
title: "Respect and Authority"
date: 2023-07-13T08:36:20+05:30
tags: ["thoughts"]
---

Why do we "listen" to somebody ?

Here I do not mean *hearing* when I say *listen*

I mean obey or follow.

I think it comes from either respect or authority.

Authority maybe given by someone else

e.g. Teacher in school, boss at work, parents at home.

Lot of times, most people do what these people tell you to do, because of the authority inferred on them.

Because you are "supposed to" do what they say.

This does not last long though.

Sooner or later you leave the school, move out of your house or change jobs.

Respect on the other hand is earned.

You may respect leaders in their field. You may "follow" them on social media platforms, 
read their books/blogs/articles, listen to their podcasts.

Not because anyone told you to. (Ok, sometimes you read a book because an authority figure told you to, but it does not
have same gains)

Because you respect them, you are willing to at least try to follow their advise.

That is why I prefer the term "Respected in their field" over "Leading authority in the field"

**I will try to gain respect of my colleagues and my family** [^1]

----

Third reason why you obey/follow someone is because of *fear* .
Sometimes you fear someone because of the authority inferred by someone.
Sometimes it maybe because you are trying to avoid confrontation.

But sooner or later you'll try to get out of it.

----

**Authority is transient, respect is long lasting**


[^1]: Reason I wrote this is because during my morning meditation, it dawned on me that I have neither. I need to do better.

