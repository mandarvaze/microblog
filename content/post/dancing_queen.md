---
title: "Sketch: Dancing Queen"
date: 2022-01-29T17:38:39+05:30
useMermaid: false
tags: ["sketch"]
---

I published my [new sketch](https://learnings.desipenguin.com/images/sketches/dancing_queen.jpg)
This appeared on the back of one of the children's books. I think [tinkle](https://www.tinkle.in/)

It took me less than 5 minutes, but makes for a good break from work, and lets me keep practicing my art.

The girl appears so happy showing off her new dress (I think) or she is just happy and frolicking.
In either case, it is adorable.

----

*If you haven't, you can check my older sketches [here](https://learnings.desipenguin.com/galleries/)*
