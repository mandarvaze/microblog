---
title: "How to pretty-print in Rust"
date: 2024-08-06T15:40:00+05:30
tags: ["rust"]
---

Today, I accidentally found out that instead of using `{:?}` to debug print, if
one just adds an extra `#` like `{:#?}` the variable is pretty printed.

This makes sense for `struct` rather than simple data types like numbers or
strings.

The interesting part (for me at least) is how I "discovered" it 😄

When I was printing a `struct` (for debugging 🙈) VS Code (I think `rust
analyzer` plugin) showed a popup how the struct does not implement `Display`
[^1]

I usually ignore the popup. But this time words *pretty print* caught my eyes.

*Moral of the story : It pays to (carefully) read the error messages 😄*

---

The exact error is : 
```
= help: the trait `std::fmt::Display` is not implemented for `......`
= note: in format strings you may be able to use `{:?}` (or {:#?} for pretty-print) instead
```

This is mentioned in the [Rust book](https://doc.rust-lang.org/book/ch05-02-example-structs.html#adding-useful-functionality-with-derived-traits),
 but the specific got buried in the wall of text. Unlike the popup, which made me pay attention

[^1]: Standard error till one adds at least `#[derive(Debug)]` before the struct.
