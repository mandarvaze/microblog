---
title: "Jujutsu: Working With Git"
date: 2024-12-06T12:52:24+05:30
tags: ["tools"]
---

After reading (not done)
 [Steve's Tutorial](https://steveklabnik.github.io/jujutsu-tutorial/) and
 [Official doc](https://martinvonz.github.io/jj/latest/) [^1] (which
 mentions Steve's tutorial anyway) I decided that actually using it on real
 projects is the way to go.

Here is the workflow (after a few [^2] iterations [^3]) that works

1. `jj new` : Declare your intention to start new work.
2. `jj describe` : Intention alone is not enough 😀. What will you be working on ?
3. Now work. Make changes.
4. Time to make our "change" available in git. Get the change id from `jj log`
5. `jj bookmark set main -r <change_id_from_previous_step>`
6. `jj bookmark track main@origin` (or appropriate branch on `origin`). You'll need this step only once.
7. `jj squash -i` : Select the files that need to be "added" to the commit
8. `jj git push --allow-new -b main --remote origin` [^4] Done!

Finally, I must say that I'm blown away (and humbled) by the fact that "The
Steve Klabnik" who wrote the Jujutsu tutorial **and** the Rust book, 
helped me [^5] (Near real time responses) with my problem.

**Steve, I'm really grateful for your help** 🙏


[^1]: I suggest if you are eager (like me) read the [FAQ](https://martinvonz.github.io/jj/latest/FAQ/) which might answer most of your questions
[^2]: See [this](https://bsky.app/profile/mandarvaze.bsky.social/post/3lchc73yz622w) thread on bluesky
[^3]: Here is my [hack](https://bsky.app/profile/mandarvaze.bsky.social/post/3lckszrniss2z) before I learnt the proper way
[^4]: Since I was not tracking remote "bookmark", this command **told** me exact command needed to be run. How cool is that!
[^5]: [here](https://bsky.app/profile/steveklabnik.com/post/3lcktgjkdak2v)
