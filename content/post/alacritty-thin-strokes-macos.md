---
title: "macOS: How to render thin(ner) strokes in Alacritty"
date: 2024-06-11T17:56:25+05:30
tags: ["terminal", "macOS", "omakub"]
---

After going through omakub [^1] and its [source](https://github.com/basecamp/omakub), I wanted to recreate it on macOS as much as possible.

The first thing was to use Alacritty. 

I had tried it in the past, but moved to [Wezterm](/post/wezterm/).

The reason I stopped was because there was no support for panes or tabs (which
is by design) But now that I'm anyway using [Zellij](/tags/zellij/) for that, I
decided to give Alacritty another chance.

But default rendering looks fat (or bold) which I didn't like.

Turns out I'm not the only one. See [this](https://github.com/alacritty/alacritty/issues/4033) issue.

Problem is with the macOS (things are OK - apparently - on Linux)
Need to set `AppleFontSmoothing` to `0` like :

```shell
defaults write org.alacritty AppleFontSmoothing -int 0
```


[^1]: [Omakub](https://omakub.org/) is a (self) described as "An Omakase
Developer Setup for Ubuntu 24.04 by DHH"
