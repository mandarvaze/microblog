---
title: "Crystal Language: C Like Performance, Ruby Like Syntax"
date: 2023-05-04T06:27:00+05:30
tags: ["crystal", "programming language"]
---

I've been intrigued by Crystal Programming language for some time.

I've been programming in Ruby for almost an year now (Mostly Rails, which
I can't say I like - too much magic)

TBH, speed of interpreted language has not been an issue (I worked in
python for quite some time before Ruby) but Crystal also comes with
statically inferred types.
Since it is a compiled language - bunch of errors maybe caught at compile time, rather than at run-time.

Some possible downsides of new language is that ecosystem is not as mature as languages like python and ruby that has been around for 20 years or so.

But Crystal is now at 1.8, so it is definitely a mature language.

I'm really excited.

I've already created an [entry](https://pkm.desipenguin.com/notes/x8zip5vfxz7mqogpho1ms9v/) in my PKM
