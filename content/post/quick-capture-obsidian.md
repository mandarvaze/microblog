---
title: "How to use Google Keep for Quick Capture with Obsidian"
date: 2024-08-12T14:05:28+05:30
tags: ["note-taking", "tools", "obsidian", "productivity"]
---
## Problem

While I use [^1] Obsidian to capture everything while I'm at the desktop, it gets tricky when I'm away from the Desktop. Especially during my morning walk, I listen to Audiobook, and want to capture either a quote or some of my own related thoughts. [^2]

## Solution

Today, I came across a YouTube Video [^3] which addresses this issue.

Zsolt goes on to explain how see sought suggestions on Twitter [^4]

Important components of his solution are to use Google Keep on mobile (Voice assistant if need be)

Then open Google Keep on the Desktop, and review the notes. Move to Obsidian if required and delete from Google Keep.

But ...

The best part happened *afterward* 

Gourav Verma recommended `Custom Frames` plugin, which enables to embed Google Keep inside Obsidian's Sidebar. 🎉

## Summary

* Use Google Keep on mobile
  * Use Google Assistant to capture notes via voice (if required)
* Use Custom Frames plugin in Obsidian
* Show Google Keep in Obsidian Sidebar

References:

[^1]: realistically, I *try* to use 😉
[^2]: I used to use Zoho Notebook for this so far. But the problem is I (almost) never look at them again 🤦‍♂
[^3]: [YouTube Video](https://youtu.be/Eic-SmIMG2Y)
[^4]: [Twitter Thread](https://x.com/zsviczian/status/1539485302368129024)
