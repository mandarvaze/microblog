---
title: "Be Better Than You Were Yesterday"
date: 2023-04-26T08:41:34+05:30
tags: ["misc", "podcast"]
---
Few of the quotes from this[^1] interview, Danny Meyers

On why someone is motivated (From sports)
1. Likes to beat others [^2] - Muhammad Ali
2. Hates to lose  - John McEnroe
3. Beat own record - Usain Bolt

> Every day is an opportunity to be better than you were yesterday
> Perfection is stupid, receipe for unhappiness [paraphrased]

> Be a 51 percenter.

Of total 100 "marks" - 49% are for the technical skills required for the job
and other 51% are for "How did you make everyone else feel while doing it?"

In the context of restaurant industry, "others" mainly mean customers, but it
also includes your coworkers as well.

He also talks about "working for free, to learn the craft"
At one point, he had to pay to learn (as opposed to getting paid less, or no pay) in an internship (like situation)

Entire interview is great, if you wish to listen, the link in the footnotes.


[^1]: [Interview with Danny Meyer](https://tim.blog/2023/04/06/danny-meyer/)
[^2]: I assume beat as in `I beat him in the game of chess`, not physical violence. In case of Ali, it could also mean later, due to the sport :)
