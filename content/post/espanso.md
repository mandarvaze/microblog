---
title: "Espanso (Legacy)"
date: 2021-12-21T21:29:17+05:30
tags: ["exploring", "tools", "productivity", "espanso"]
---

Just as I was getting to know espanso, I came across a brand-new version of espanso that is way better that this (0.7.6) version, now called Legacy version.
Read about the new version [here](https://microblog.desipenguin.com/post/espanso-2/)

I have left the following as is, for posterity

<!--more-->

----

Long time ago, when I used Windows (provided by my employer), I used to use AutoHotkey.
Since then, I have not used Windows and never used text expander again.

Then today, I came across [espanso](https://espanso.org) -- open source text expander and more -- in logseq discard channel.

----
*Following does not apply with 2.0.0-alpha version*

~~At first, it was unclear to me that this is a terminal app. Mainly because I installed via Cask, which I've done only for UI apps.~~

~~Looking at espanso --help I was unclear what to do next, so I tried to start the daemon, which failed. There were no logs via espanso log either.~~
~~I had seen register command, but did not run because I thought it was register-the-product-by-providing-your-info 😆~~

~~Since nothing else worked, I decided to try it anyway. Turns out it registers the daemon with OS.~~
~~I got the permission dialog from the OS. I also got "This program is trying to control Desktop programmatically" prompt -- which I allowed.~~

~~After which espanso start worked 😄~~

~~To be fair, this is already documented, I just never read the documentation.~~

----

With a tool like this, one does not know where to start. Luckily, espanso comes with packages that one can install to get started with some required tasks.

The packages I installed so far are `misspell-en` and `all-emojis`

I also created my first abbreviation 🎉 (I added <- this emoji using `:tada:` 😄 -- which comes with `all-emojis` package)
