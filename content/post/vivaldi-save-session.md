---
title: "Vivaldi: How to save all open tabs (and Why ?)"
date: 2024-10-15T17:21:35+05:30
tags: ["browser", "vivaldi"]
---

For a long time, I'm considering switching to Linux full time since my existing
machine does not get OS updates from apple. [^1]

In preparation for this "move", I check whether all the software is available
on Linux.

Vivaldi browser in on that list (available on Linux ✅).

What about my already opened tabs ? Every few days I close the temporary/
unwanted tabs. [^2] but I do want others on Linux. Hence the search whether it
is possible. [^0]

Turns out this can be done (in theory) via Session Management feature.

Vivaldi can save all open tabs (across workspaces as well) in a session.

Opening it on other device may not be trivial. The suggested solution [^3] is to
back up entire profile folder, and restore it on the new device.

I looked inside the `Sessions` folder. But the files lack human readability. 😞

So trying it out is the only option. The folder size is currently ~145MB, so I'm
reluctant. None the less, I'll take this backup irrespective. 🤞

[^0]: I'm aware of `Sync` functionality - which requires account creation -
and I do no wish to create one more account. (At least my current thinking on
the matter 😄)

[^1]: unfortunately, I've not made the switch yet. I've been trying various
distros, and while I'm not getting newer versions of macOS, I do get updates to
existing version, and most things are working fine currently.

[^2]: Arc browser has this functionality built-in. But it is unlikely to be
available on Linux anytime soon (if at all)

[^3]: https://forum.vivaldi.net/topic/18028/how-to-export-all-the-settings-and-tabs/2?_=1728990641960
