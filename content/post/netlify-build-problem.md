---
title: "Interesting Netlify Build Problem"
date: 2023-06-26T13:01:04+05:30
tags: ["meta", "devops"]
---

After [build problems](/post/migrating-pkm-site-updates/) with my PKM site, today I ran
into another problem with this site.

What puzzled me was Deploy showed `Build` step failing but no useful error (other than `exit code 2` - which does not help much)

But in the process of debugging, I learnt [^1] about the environment variable `NETLIFY_BUILD_DEBUG` (set to `true` when needed)
It did not help 😞

I also upgraded hugo version (which is another environment variable) in the process.

That did not either.

For some reason, I looked inside `Deploying` stage- which was shown as `Complete` in green font with a tick mark next to it.
()`Building` step OTOH was shown as `Failed` in red font with cross mark next to it.)

`Deploying` had following error in it
> Invalid filename 'tags/c#/page/1/index.html'. Deployed filenames cannot contain # or ? characters

There you go. (This was not a problem locally though 🤷‍♂)

I changed the tag from `c#` to `c-sharp` and all was well 😄

[^1]: Relevant part of the [Support Guide](https://answers.netlify.com/t/support-guide-frequently-encountered-problems-during-builds/213)