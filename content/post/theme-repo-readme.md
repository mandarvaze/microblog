---
title: "Updated the README for the Theme Repo"
date: 2024-12-07T18:39:46+05:30
tags: ["meta"]
---

It has been little over 2 years since I [forked](https://github.com/mandarvaze/hugo-microblog) and updated jnjosh's [internet-weblog](https://github.com/jnjosh/internet-weblog) theme

While I made several changes to my fork, I never updated the README.

till now that is.

During last few days, I updated the README to match my fork.

These are minor changes, and majority of the README is still from the original (For the things that have not changed)

If you are curious, [have a look](https://github.com/mandarvaze/hugo-microblog) 😄
