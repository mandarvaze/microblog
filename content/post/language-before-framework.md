---
title: "Language Before Framework"
date: 2023-06-05T16:44:52+05:30
tags: ["learning", "programming"]
---

Several years ago, I came across a quote[^1] about how learning python *via* Django.
The author said that if you don't properly know python, you may not understand what is Django specific and what is not.

FWIW, even after working with python, I never used Django in any of my main project.
But I did learn python first before the frameworks.

Fast forward to few days ago. I am working on Ruby code to read from Azure Service Bus.

I have been working on rails project for a while now, and (indirectly) learned ruby via rails.

In rails, I am used to checking for `nil` via `.present?` and/or `.blank?` 
Turns out, both are provided by Rails (ActiveSupport)

Ruby only provides `nil?` and `empty?` (For Arrays, Hashes and Sets)[^2]

> There is a difference between knowing the path and walking it
>
> -Morpheus
[^3]


[^1]: For some reason, original post is unavailable. [This](https://web.archive.org/web/20231205115443/https://jeffknupp.com/blog/2012/12/11/learning-python-via-django-considered-harmful/) is the link via Wayback machine. 

[^2]: [nil?, empty?, blank? in Ruby on Rails](https://blog.arkency.com/2017/07/nil-empty-blank-ruby-rails-difference/)

[^3]: [Knowing the path](https://www.youtube.com/watch?v=Kz40vwcTGFo)
