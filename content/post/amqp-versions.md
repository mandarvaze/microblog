---
title:      "AMQP Versions"
date:       2023-05-16T21:59:55+05:30
tags:       ["amqp", "azure"]
---

As I had mentioned [earlier](/post/python-after-long-time/), at work I was "strongly encouraged"
to use Azure Service Bus, instead of RabbitMQ (which was the technology I had suggested.)

RabbitMQ has good Ruby support. I had chosen [Sneakers](https://github.com/jondot/sneakers) (Which uses [bunny](https://github.com/ruby-amqp/bunny) under the hood)

Since Azure Service Bus also supports AMQP, I was trying to see if we can use these libraries with Azure Service Bus.

But connection itself wouldn't work. I kept getting `FrameTypeError`

After a lot of head-scratching and searching, a colleague pointed me to [this discusion](https://groups.google.com/g/ruby-amqp/c/8aj6vVR3nh8/m/xrHZV1AZBgAJ
)

> AMQP 1.0 is not a "later version" of AMQP 0-9-1, it is a completely different protocol,
> despite the name and (in theory) the same working group that developed it. Bunny is an AMQP 0-9-1 client.

🤦‍♂

Azure Service Bus supports AMQP 1.0 while RabbitMQ [supports](https://www.rabbitmq.com/protocols.html#amqp-091) AMQP 0-9-1 🤷‍♂

While RabbitMQ can support 1.0 via plugin, bunny (client) does not.

So we could not use bunny/sneakers (my first choice) to connect with Azure Service Bus
