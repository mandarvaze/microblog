---
title: "100 Days of Rust : Day 8"
date: 2024-01-30T22:29:48+05:30
tags: ["rust", "learning", "programming language"]
---

Today I started reading 
[Command Line Applications in Rust](https://rust-cli.github.io/book/index.html)

Even though I have not finished reading "the book", I am (by now) familiar with
enough rust code that reading this book was kinda refreshing.

Few important things I picked up :

* `{:?}` in `println!` is called debug representation 
  (quite useful for .. debugging 😄)
* Custom data types can add support for `{:?}` for debugging and logging, 
  one needs to add a `#[derive(Debug)` above their definition. (Not tried it yet)

I'm gonna convert my "Temparature Converter" assignment using the stuff I 
learnt in this book
