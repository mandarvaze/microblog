---
title: "Rust Script"
date: 2025-01-20T13:19:53+05:30
tags: ["rust"]
---

After my [previous post](/post/rustc-over-cargo/) I came across other ways to "quickly test rust code"

Obvious being `rust-script`

It is easy to install via `cargo install rust-script` (But I had it already
installed. I think Espanso installed it, but not too sure)

This may be better than `rustc` because one can define the dependencies in the
 "script" itself, like :

```sh
#!/usr/bin/env rust-script
//! Dependencies can be specified in the script file itself as follows:
//!
//! ```cargo
//! [dependencies]
//! rand = "0.8.0"
//! ```
```

Start the file by specifying `rust-script` in the shebang line, so that shell knows what program to use for running this code

While the README on GitHub shows code with `fn main()`, it is not required.

Since we are testing throw away code, I write only the absolutely necessary code.

```rust
#!/usr/bin/env rust-script

use std::env;
match env::home_dir() {
    Some(path) => println!("Your home directory may be: {}", path.display()),
    None => println!("Unable to determine your home directory")
}
```

You also need to give it execute permission via `chmod a+x file.rs` if you want
run it as `./file.rs`

Else you can run it as `rust-script file.rs`, in which case shebang is not
required, neither is execute permission.
