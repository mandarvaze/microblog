---
title: "MacOS Upgrade : The Conclusion"
date: 2023-03-28T17:45:30+05:30
tags: ["misc"]
---

*[previously](/post/macos-upgrade/) .. our protagonist made all the preparations, and yet his two attempts to upgrade the OS failed due to what seems like hardware failure. Will he succeed, or give up ? Let us find out in this thrilling conclusion aka Part 2*

So in place upgrade did not work. I looked at my SSD. It was partitioned in two. One for OS and other for data. I do not remember if I did that when I tried to install linux on this machine. 🤷‍♂ (Unlikely, I usually boot from external USB disk and try.)

Since I had the exact error, I decided to search and ay least one solution suggested reformatting.

While I dreaded it at first, I had taken the backup (I did one more round, and found some files were not backed up. Good to double check 😄) Reformatting would also serve as a forcing function. I would not have manually deleted whole lot of data that I had accumulated over the years.

In order to install macOS Monterey on a recently formatted drive, I would need a way to boot the installer via external source.

This required me to download the installer as an app. While it is possible to download it via App store, I chose to do it via the command line [^1] (because why not ?)

Once the app is downloaded, I needed to create a bootable media. [^2]

I had an extra SD card for such purposes - macOS images tend to be large. The SD card is 32GB.

Once I booted via external source [^3] - I selected `Disk Utility` option, and formatted the disk. (Somewhere it mentioned that I needed to delete the `container` rather than format individual volumes. So I did just that)

After that OS install was uneventful. Answer bunch of questions (language, timezone, create user etc.) and wait while installer does its thing. (prepare drive, copy stuff etc.) (I didn't actually wait-wait. Focused on work, rather than worrying "will it work, or fail again" 😄

Finally, it was great to see myself greeted into macOS Monterey. (This is the final version supported on this hardware)

[^1]: Download installer app from terminal `softwareupdate --fetch-full-installer --full-installer-version 12.6.4`
[^2]: Create bootable Disk `sudo /Applications/Install\ macOS\ Monterey.app/Contents/Resources/createinstallmedia --volume /Volumes/MyVolume` [Ref]( https://support.apple.com/en-us/HT201372)
[^3]: Option (⌥) or Alt: Start up to Startup Manager, which allows you to choose the drive from which to boot from
