---
title: "How to define task dependencies in justfile"
date: 2024-11-16T22:00:10+05:30
tags: ["devops", "programming"]
---

It has been more than 20+ years since I used `Makefiles` seriously.
I did use them later, but they were very simple.

Then I found and moved `just` task runner, which is supposed to be replacement for `make`

My `justfile`s were also simple.

and today, I needed to define a task that we dependent on another.

Turns out, it is same as `Makefile`

```justfile
task1:
  @echo "Task1"

task2: task1
  @echo "Task2"
```

In the above example, task2 depends on task1, and thus when we say `just task2`,
task1 will get executed, as it is a dependency for task1

