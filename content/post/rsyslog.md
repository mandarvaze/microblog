---
title: "Configure `rsyslog` "
date: 2021-07-14T17:07:13+05:30
useMermaid: false
tags: ["linux", "devops"]
---

Today, I configured `rsyslog` on Ubuntu server to collect logs from remote application server.

syslog using UDP seems straight forward, but I wanted to set up using TCP (TCP being more reliable and all) 

At first, it did not work because I needed to tell `SysLogHandler` that we are using TCP using `SOCK_STREAM` as optional param. (Default is UDP)
So in a standalone test script, remote logging worked.

Turns out because of how TCP works, the logs are not flushed to the remote server till the application server closes the socket connection.

Considering application server works 24x7, it will not close socket, and thus it appears as if logs are not reaching the remote server.

So settled back on UDP (full circle?)

But learned things in the process. (There may be a way to use TCP and flush the log immediately, but for now moving on)
