---
title: "100 Days of Rust : Day 6 (Ownership)"
date: 2024-01-24T16:46:03+05:30
tags: ["rust", "learning", "programming language"]
---

When I started reading about Ownership, I was thinking 

> I have done C. I understand memory

But Rust book explains :

> If you are familiar with systems programming, you might think of memory at 
> a low level like "memory is an array of bytes" or "memory is the pointers
> I get back from malloc".
> ..
> The low-level model is too concrete to explain how Rust works. 
> Rust does not allow you to interpret memory as an array of bytes, for instance.

Reading further, I'm so glad rust does not allow "manual memory management"
 (like `malloc` and `free` in C)

------

Learnt about [Cloning avoids Moves](https://rust-book.cs.brown.edu/ch04-01-what-is-ownership.html#cloning-avoids-moves)
Useful when we need to reference the "original" data after it the ownership is moved

`.clone` create a copy (which is used to move the oownership,leaving the original intact)

