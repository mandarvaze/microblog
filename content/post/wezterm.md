---
title: "Wezterm"
date: 2022-08-09
useMermaid: false
tags: ["exploring", "terminal", "tools"]
---

I had installed Wezterm long time ago. I think when I started looking for alternatives to `Alacritty`.
But I settled on `kitty` (I think I could not get `wezterm` to work)
  * [ ] 
Few days ago, I stumbled upon it again, and I wanted to give it a try.

So these days, I'm using `wezterm` as terminal. I specifically liked the built-in [multiplexing](https://wezfurlong.org/wezterm/multiplexing.html) feature. I haven't tried it yet 🤷‍
