---
title: "Ameba: Rubocop for Crystal Language"
date: 2023-06-02T21:58:21+05:30
tags: ["crystal", "learning", "programming language"]
---

[Ameba](https://github.com/crystal-ameba/ameba) seems like quite mature linter for Crystal language.
As I start my first real world code in Crystal language, the real tools are very useful.
*(Side note: I didn't write any non-real-world crystal code. I'm not sure it helps. REPL sessions are not code. they don't count)*

It automatically uses built-in formatter in the `--fix` mode.

There is also awesome emacs integration as well. Check [ameba.el](https://github.com/crystal-ameba/ameba.el)
