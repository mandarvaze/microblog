---
title: "Changing Emacs Look-N-Feel"
date: 2023-06-06T10:14:14+05:30
tags: ["emacs"]
---

I've been using [nano emacs theme with minemacs](/post/nano-minemacs/)
I got bored today, and wanted to try something new.

- I'm trying [circadian.el](https://github.com/guidoschmidt/circadian.el)
  * modus-operandi during the day
  * doom-tokyo-night during the night
* Iosevka font (Have not settled on exact variation)
* Also trying `org-modern` but I [use denote](/post/trying-denote/) - so I'm not sure whether it matters
