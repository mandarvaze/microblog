---
title: "D2 : A modern diagram scripting language"
date: 2022-12-01T05:50:56+05:30
tags: ["exploring", "tools", "D2"]
useMermaid : true
---

I came across [D2](https://d2lang.com/) on Mastodon I think. (Like most things.
But I can't find the original toot 🤷‍♂)

I used mermaid.js earlier. In fact, I also
[added](https://github.com/jnjosh/internet-weblog/commit/f42b8fc01880c64cbd265a323a7698a659187c37)
mermaid support for this theme.

Benefit of Mermaid is that since it is generated by `mermaid.js`, I don't need
to save and include the image in my blog (and worry about mismatched filename
and/or path, resulting into broken image.)

On the other hand, not many hugo themes support mermaid, but link to an image
is supported by hugo and every other SSG.

Since I have not used mermaid extensively, I need to refer to the documentation
for mermaid anyway. So learning D2's syntax isn't any worse.

But I think D2's syntax is much easier.

See this D2 syntax for reference :

```
content: Write contents in markdown
content -> source git repo : git push
Site in docs folder -> netlify deploy : git push
content -> Site in docs folder : Dendron: Site Build
 
```
and here is a generated SVG file.

![D2](/image/d2.svg)

Now compare this with mermaid syntax:

```
graph LR;
    A{ Write Content in markdown }
    A -->| git push | B[ source git repo ]
    A -->| Dendron: Site Build | C[ Site in `docs` folder ]
    C -->| git push | D[ netlify deploy ]
```

and generated image :

{{<mermaid>}}
%%{init: {'themeVariables':{'lineColor': 'lightblue', 'arrowheadColor': 'lightblue'}}}%%
graph LR;
    A{ Write Content in markdown }
    A -->| git push | B[ source git repo ]
    A -->| Dendron: Site Build | C[ Site in `docs` folder ]
    C -->| git push | D[ netlify deploy ]
{{</mermaid>}}
