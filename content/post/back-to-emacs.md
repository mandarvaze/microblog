---
title: "Back to Emacs"
date: 2022-08-17
useMermaid: false
tags: ["emacs", "tools"]
---

If you are reading my posts, you would have realized that I had switched to `nvim` and went down the rabbit hole with different configurations etc.
I think I got it to a stable state, and then I stopped tinkering.

But when I recently started coding again in Ruby, I realized that `nvim` is good as an editor, but at least I was not able to configure it as IDE.
It has all the right tools (lsp, syntax highlighting, packages) yet something didn't work 😞

That is when I came back to emacs, and realized that in emacs ecosystem, `robe` was better than `solargraph` 
Something about *dynamic better than static analysis* for Rails.

Then I started noticing more issues with `nvim`. e.g. Simple syntax highlighting did not work for markdown files.

I still use `nvim` for quick editing (I've been using `n(vi)m` for 25 years 😄, so I'm not about leave it. Even in Emacs, I use evil mode)

------

I realized that instead of [Telekasten](/post/telekasten/) I should be using `org` - native to emacs, and more versatile. But markdown is universal 😄
