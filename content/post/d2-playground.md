---
title: "D2 Playground"
date: 2023-01-02T09:14:36+05:30
tags: ["tools", "D2"]
externalUrl: "https://play.d2lang.com/"
---

I'm not sure whether they launched this recently, or it was always there (As in
since I looked at D2 couple weeks ago)

Nevertheless, it is quite nice.

If one is not used to CLI and/or does not wish to (or can not, due to 
permission on say work machine) install it locally, then playground the best
option.

It has all the options the CLI provides (At least the most widely used options)

One can 
- export the images
- Change Engines
- Change Themes

What else do you need ? 😄

The playground also has sample code right below it, so one does not need to
refer to the documentation (Especially during initial days when one is still
figuring things out)


