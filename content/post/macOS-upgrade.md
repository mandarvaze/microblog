---
title: "MacOS Upgrade : The beginning"
date: 2023-03-27T22:38:27+05:30
tags: ["macOS"]
---

As I intended and mentioned [here](/post/resetting-phone-2/) - I have started upgrade to macOS Monterey.
I had already taken backup. (I don't expect the entire data to be lost, but still ..)
Just before I clicked the `upgrade` button - I created a list of software I need to re-install after the upgrade.
I also went ahead and deleted apps left-right and center from the `Applications` folder.
Nice (and not so nice) stroll down the memory lane.
There was so much junk accumulated over the years.

Good opportunity to clean up.

Currently, the progress bar shows few minutes shy of 1 hour.

🤞

----

Edit 1: First attempt failed :( Something about disk. After reboot, it went straight for OS upgrade - good.

Edit 2: Second attempt also failed with same error `Storage system verify or repair failed: -69716` 😭
