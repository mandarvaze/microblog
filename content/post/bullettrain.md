---
title: "Bullet train: Rails based framework"
date: 2023-01-04T09:57:18+05:30
tags: ["ruby", "rails"]
externalUrl: https://bullettrain.co/docs
---

I came across this from (now defunct) [rails app](https://railsapps.org/) site.
They seem to provide a lot of stuff in their open source version.

Seems like a good framework to get started on the side project or MVP

Other paid features are worth paying when your app starts making money.

I need to spend more time on digging deeper, and actually trying it out.
