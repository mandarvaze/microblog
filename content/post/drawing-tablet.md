---
title: "Drawing Tablet"
date: 2022-12-10T05:51:47+05:30
tags: ["art", "hobbies"]
---

I was always fascinated with devices with stylus, where I could draw
electronically (Not sure if the word *digitally* was being used back then.)

My first such device was Palm III.[^1] It came with Stylus.
That was in the last century.

Needless to say, I stopped using after year or 3. (I had it long time after that, and then I finally gave it for electronic waste/recyling program 😢)

Then after my elder son was born (which has no significance -
just that I remember it as such) I purchased Samsung Galaxy Note 8 [^2]

I still have it. I use it primarily for reading kindle books. My children use
it for watching Youtube kids.
Just recently it has developed some problem and isn't charging anymore. 😢

Unfortunately, after initial enthusiasm [^3] dwindled, I hardly took the stylus out 😞

I think the issue was writing on glass does not feel natural and while 8" tab
was much better than handheld device like Palm III, it was still smaller than
A4 paper I mostly used for analog drawing/sketching.

That brings us to my latest [purchase](/post/xp-pen-deco-mini-7/)

It is too early (only days) to comment.

I'm still figuring out the software that works for me.

But it has a feel of writing on paper (sometimes it makes a noise like pencil
on rough paper, I wonder if it is haptic feedback) better than feel of writing
on glass.

Bigger size of the screen also addresses some problems (?) I had earlier.

I hope to make much better use this time around 🤞

[^1]: If you are wondering what is Palm III, see [this](https://i.ebayimg.com/images/g/HpEAAOSwC3lhm-DH/s-l1600.jpg)
external link (Link may become 404 🤷‍♂)

[^2]: There was an 8-inch [tab](https://www.gsmarena.com/samsung_galaxy_note_8_0-5252.php),
before there was [phone](https://www.gsmarena.com/samsung_galaxy_note8-8505.php) by the same name 🤷‍♂

[^3]: See my initial sketchnotes, drawn on Galaxy Note 8 [here](https://learnings.desipenguin.com/galleries/sketchnotes/) oldest 5 were digital.