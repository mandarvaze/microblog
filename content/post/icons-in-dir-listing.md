---
title: "Icons in Directory Listing"
date: 2022-01-13T20:21:19+05:30
useMermaid: false
tags: ["terminal"]
---

Would you like to see pretty icons in your terminal for directory list ?

Then install `lsd` from [here](https://github.com/Peltoche/lsd)

Check this pretty screenshot of `lsd --tree`

![lsd](/image/icons-dir-listing.png)

In case you are wondering, I'm using [nord](https://github.com/eendroroy/alacritty-theme/blob/master/themes/nord.yaml) theme with Alacritty
