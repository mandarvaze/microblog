---
title: "Working Offline"
date: 2022-05-05T17:21:28+05:30
useMermaid: false
tags: ["personal", "misc"]
---

Today around 10am (or a bit earlier) we lost electricity at our house.
Today is Thursday, and power loss isn't surprising.

As far as I remember, my city always had scheduled power loss on Thursday. This used to happen every week when I was growing up. So much so, that my engineering college had Thursday as a weekly off, due to this. Over the years, things improved, but Thursday remained the day when local electricity company ~~would~~ could schedule the  maintenance task.

Every now and then, we still have power loss for 20-30 minutes. It was after a long time we had a day long power loss (5.30PM as of this writing, and power has not returned)

I could not work the whole day. At first, I read the books. I completed two books today (I had started reading both earlier)

Ironically (?) one of the book was science fiction novel in Marathi called "Virus". It is about how an advanced race from distant planet sends a virus via radio signals, and wants to cripple the modern world. (Earth wins! don't worry : ) In the novel there are references to how we are dependent so much on computers for everyday life. I was thinking the same, but about internet. Due to battery, we could use our computers (for few hours) without electricity, but almost nothing can be done without the internet.

That is when I started thinking about the things I could do "offline", and decided to write this post.

-----

While I was able to write this post, I can't publish it without the internet  
