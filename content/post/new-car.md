---
title: "New Car"
date: 2023-11-10T00:02:43+05:30
tags: ["misc"]
---

Today we purchased a new car!

*Thank you (Here I assume you are congratulating me 😄)*

It is a great feeling.

Some quick thoughts (in random order)

The process took about 2 hours, even though we were waiting just 10 minutes for our turn. 

Tomorrow onward there will be a lot of rush due to Diwali

Kids got bored towards the end, when the executive was giving me the demo/explaning the features.

My sister-in-law's family left (their 5 year old got cranky after waiting around so much)


This is our second car after 15+ years.

Technology has changed a LOT.

Our version (or may be all cars these days) has digital dashboard.

When we were refuelling (first thing after we got out of the dealership premises) 
the dashboard showed animation of fuel getting filled (liquid level increasing) 
rather than mechanical indicator going up (in our old car)


This time we went to automated transmission 

Last time I had driven one was in US, about 20+ years ago

and it was a norm there.

My car in US had all the features (except digital display and phone pairing) that
we have now, including cruise control.


Our model has key less entry and push button start.

So there is no real "key". 

It is just a electronic key fob that was remote open functionality.


Car talks a lot 😄 

Seriously.

Due to all the newness (Although I didn't fumble switching to auto transmission)
one is still cautious/extra alert.

and the car wants to talk to you.

I was able to ignore the audio alerts, still...

When we reached home, I though I turned off the car (There is no key), and as
I was getting out, car started shouting *key not found*

What?

Then I pressed the start/stop button once more, little harder.

That was the "power off" (to borrow the term from computing)

Here is what our car looks like, if you are curious

![This is from company's site](https://cars.tatamotors.com/images/punch/colors/meteor-bronze-thumb.jpg)