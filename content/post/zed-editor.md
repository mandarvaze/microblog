---
title: "Zed Editor"
date: 2023-03-21T09:30:59+05:30
tags: ["tools", "editor"]
externalUrl: "https://zed.dev"
---

My colleague told me about this new editor written in rust yesterday.
The feaure page mentioned vim-mode, so I was OK to try it.

It seems collaboration is their USP - I don't see using that feature personally anytime soon.

So what about rest of the features ?

My first impression is that it can be *light* alternative to VSCode. It has similar UI structure, default keymap as VScode. It supports few languages Out of the box, Ruby being one of them, so I may try it at work as well.

Coming back to vim-mode - the reason for which I even was ready to try it.
It was not easy to enable it. I had expected to find it under keymap.

But vim-mode is restricted to modal editing, so one needs to enable it via
`settings.json` (similar to VScode) and set `"vim-mode": true` there.
Other things - not related to modal editing, but I have come to rely upon -
like `:` commands to save (`:w`), or `:1` to jump to the beginning of the file, do not work.[^1]

Opening new file wasn't intuitive. It closed the existing file.
There is no `New tab` either.

But after adding the `folder to the project` and opening anotehr file from
the same project, opened it in separate tab. 😌

In conclusion :
1. Good attempt to provide VScode alternative. Because it is a native app, it will be faster than Electron app
2. This is not meant to be terminal app anyway. So for modal editing in terminal, I will stick to [helix](/tags/helix/)

[^1]: `Cmd+S` to save the file, and `^G` to goto line/column
