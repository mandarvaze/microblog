---
title: "Where Are My (Ruby) Gems ?"
date: 2022-01-26T16:38:12+05:30
tags: ["ruby", "learning", "programming language", "vim"]
useMermaid: false
---

As I mentioned [earlier](/post/lunar-nvim/), I'm trying Lunar nvim starter kit.
Latest version of nvim comes with LSP support, and while learning a new language LSP could be really helpful (I think)

I had heard of `solargraph`. Running `:LspInstall` in nvim gave me option of `sorbet` as well.
But I decided to stick with `solargraph`.


nvim was successful in installing `solargraph`, or so it told me. But it did not work.
`LspInfo` told me that it did not find `solargraph`
Just to be sure I `gem install solargraph` from the terminal, before I tried to install LSP from nvim *again*

No dice.

So decided to investigate.

Being new to Ruby, I didn't know the default location where gems get installed. 

In the process, I learnt about `gem env` where following seemed wrong

```shell
 - INSTALLATION DIRECTORY: /usr/local/Cellar/tmuxinator/3.0.1/libexec
```
*Eventually I learnt that I could also use `gem env home`*

I had no idea why `tmuxinator` would even be in the picture.

So I decided to `brew uninstall tmuxinator`

I also `asdf uninstall ruby` (I think I went too far 😆 but I wanted to be really sure)

OK, now I had clean slate.

Now `asdf install ruby latest` followed by `gem env` and I got: 
```shell
 - INSTALLATION DIRECTORY: /Users/mandar/.asdf/installs/ruby/3.1.0/lib/ruby/gems/3.1.0
 
 ➜ which solargraph
/Users/mandar/.asdf/shims/solargraph
```

Looks better.

In the process, I also learnt `gem which solargraph`, which is a bit different (Points to `solargraph.rb` file, rather than bash script)

Now `LspInstall` also worked in lvim

But `LspStart` fails. But with different error. (Investigation and resolution of that in another post)


