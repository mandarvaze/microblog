---
title: "Org for TODOs"
date: 2023-05-10T06:37:18+05:30
tags: ["emacs"]
---

Yesterday, I started using org-mode for tracking my TODO items.

I had tried it a long time ago. Org is very powerful, and it can list
the `TODO` items across multiple files.

But that gets (at least in past) overwhelming.

So I started simple.

I started with setting `org-agenda-file` to `~/org/work.todo`

That is the only file I'm tracking my work items.

Then I use `org-agenda` to list all the open items.

When done, I mark them as `DONE` using `org-todo` command.

I also added subtasks and added `[/]` at the end of the main task.
Taking the cursor to `/` and pressing `C-c C-c` updates the list to
something like `[1/5]` depending on how many are done.

The subtasks use checkboxes like `- [ ]` for not done, and `- [x]` for done subtasks.

~~For some reason, updated tasks aren't updating the `[/]` correctly.~~

~~But for now, I'm ignoring that.~~ This is now [fixed](/post/subtasks-in-org)
