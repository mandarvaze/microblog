---
title: "Neovim From Scratch"
date: 2022-05-11T06:37:51+05:30
useMermaid: false
tags: ["vim", "tools", "neovim"]
---

On the [Vim Telegram group](https://t.me/VimUsersGroup), I came across this starter kit called [Neovim from scratch](https://github.com/LunarVim/Neovim-from-scratch)

But this starter kit is different from others. Others are opinionated (with a good reason). This one seems like purpose built.

Purpose being - to teach the users what goes under the hood. So it comes with a YouTube series.

But ones doesn't need to follow the YouTube series to use this. One can just clone the git repo and get going.

One this that made this more attractive is that it is created by the same person (or at least one of the people) who created LunarVim. 

I've been already using LunarVim for some time. But never quite got how it worked. Also some weird choices of keybindings e.g. They have key-binding for moving to next buffer `:bNext` but not for `:bprevious`.

So I started with backing up the existing working config - as the README suggested (and as a precaution)

I had to `rm -rf` the `~/.cache/nvim` and `~/.local/share/nvim` before I could start my new config.
I ran into couple of issues, and I ~~almost~~ went back to working LunarVim config.

But then I found workarounds for the problems in the project's github issues. (Open source FTW)

While following the YouTube is not mandatory, it is highly recommended.

With the YouTube series, I already modified the various options e.g. I prefer relative line numbering which was turned off by default. I also did not like the colorschemes that it comes with. So I added by favourite [Nord](https://github.com/shaunsingh/nord.nvim) theme.

I have only finished up to part 3 of the series, but learned a lot in the process.

-------

*FWIW, this post was written in my new setup.*
