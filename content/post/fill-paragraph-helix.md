---
title: "fill-paragraph in Helix"
date: 2024-06-11T22:05:10+05:30
tags: ["helix", "tools"]
---

Emacs [^1] has this command `fill-paragraph` (Usually `M-q`) which will auto
format long uneven lines to make them look even by adding a hard wrap.

But I use helix for writing these blog entries. That is how I discovered
`reflow` command in Helix.

It is simple really

Select the blocks of text you want to format (by typing `x` and repeating for as
many line as you have). Then `:reflow`. That's it!

[^1]: I learnt about `gq` in vi (Yes `vi` not `vim`. This was the time when
`vim` did not exist. Yeah, I've been using `vi` that long) That time we used to
use it format "text" where formatting text mattered. Eventually, I forgot about
it, till I started using vi/Emacs/helix for writing more text than code.
