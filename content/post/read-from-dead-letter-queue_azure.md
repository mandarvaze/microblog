---
title:      "Read from Dead Letter Queue"
date:       2023-06-20T08:31:31+05:30
tags:       ["azure"]
---
As mentioned in the [earlier](/post/amqp-versions/) post, 

> at work I was “strongly encouraged” to use Azure Service Bus, instead of RabbitMQ 
Unfortunately

SDK has `FormatDeadLetterPath` [functionality](https://learn.microsoft.com/en-us/dotnet/api/microsoft.servicebus.messaging.queueclient.formatdeadletterpath?view=azure-dotnet#microsoft-servicebus-messaging-queueclient-formatdeadletterpath(system-string)) but it is not available to HTTP REST API [^1]

Append `/$DeadLetterQueue` to the queue name.[^2]

Refer to this SO [comment](https://stackoverflow.com/questions/22681954/how-do-you-access-the-dead-letter-sub-queue-on-an-azure-subscription#comment131543948_34343745)

[^1]: Readers would [remember](/post/python-after-long-time/) that since Ruby is not officially supported by MS, we had to resort to using REST API to access the Azure Service Bus functionality.

[^2]: It has to be exactly as it is with capitalization and all. Took me some time to realize that. MS Docs refer to it in all-lower-case, which does not work.