---
title: "Do Something"
date: 2023-02-10T13:10:11+05:30
tags: ["books"]
---

These days I'm reading a book "The Subtle Art of Not Giving a F*ck"

In it, Author suggests that if you are stagnated, just "Do something" and
it will get you out.

I had not posted here in a long time. High priority things at work, 
late nights, that sort of thing.

While high stress situation ended, the lethargy continued.

Till I posted about [Org-roam](/post/org-roam/) earlier today.

and now I'm posting my second post of the day 😄

The Advice of "Do Something" does work 
