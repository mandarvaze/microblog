---
title: "Creating Youtube Banner"
date: 2024-07-08T14:10:26+05:30
tags: ["youtube"]
---

Today, I was trying to create a banner for my (upcoming) YouTube channel.

I used Canva to get started. It has a lot of templates to get started.

This can be a rabbit hole 😄 But I finalized the one I liked.

After customizing the template to my liking, I was ready to upload it to
youtube.

But ..

Turns out, depending on the device where are watching YouTube, different parts
of the banner are visible.

YouTube creator studio [^1] was upfront about which part of the image will be
visible based on the device.

The image is fully visible only on TV. On Desktop, only middle (third?)
portion is visible.

I had chosen the template based on the background image, which is almost never visible unless on TV.

Without the background, the banner didn't look as impressive as I had imagined
😆

Back to the drawing board

This time I didn't focus on the background. Instead, I focused on the middle third of the banner.

I had to modify the image a few times.

Each time, I would 

* Adjust the elements in Canva
* Download the image
* Upload to YouTube
* Does it look OK (on non-TV devices ?)
* If not, back to Canva

I think I finally got it right.

[^1]: Or simply, the `Channel customization` page 😄
