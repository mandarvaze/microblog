---
title: "Using Telekasten with Nvchad"
date: 2022-06-23T12:00:54+05:30
useMermaid: false
tags: ["vim", "tools", "neovim"]
---

[NvChad](https://nvchad.github.io/) is yet another neovim starter kit.

Creator of NvChad is on the [VimUser's Telegram Group](https://t.me/VimUsersGroup). So I was intrigued to try NvChad. (I had tried it a while ago, but decided to visit it later. That "later" is now)

One of the plugin I use is [Telekasten](https://github.com/renerocksai/telekasten.nvim). It reminds me of Dendron, another tool I used to use (requires VSCode, which I consider too heavy. But Dendron now works without VScode as well. Also, "daily notes" is just one of the features of Dendron. That is the feature I use with Telekasten)

Unfortunately, I could not use it "out of the box". NvChad uses `Telescope`, but it lazy-loads it. `Telekasten` is dependent on it.
So it was weird that `telekasten` would work after I use `Telescope` at least once. (Cause by using Telescope, nvim would load `telescope.nvim`)

After failing to get it working for couple of days, it finally worked. See the discussion in this [guthub issue](https://github.com/NvChad/NvChad/issues/1255)

```shell
mandar in ~/.config/nvim/lua/custom on  main 
➜ cat plugins/init.lua
return {
  ["renerocksai/telekasten.nvim"] = {},

  ["nvim-telescope/telescope.nvim"] = {
      module = "telescope",
  },

  ["simrat39/symbols-outline.nvim"] = {}
}

```

-----

There is another manual workaround. `:PackerLoad telescope.nvim` before using Telekasten for the first time. But above solution is better.
[Here](https://github.com/NvChad/NvChad/issues/1255#issuecomment-1162678094) is my failed attempt to automate my solution. But somehow it broke telescope itself (while Telekasten worked)
