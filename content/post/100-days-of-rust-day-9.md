---
title: "100 Days of Rust : Day 9 (Testing)"
date: 2024-02-03T22:01:26+05:30
tags: ["rust", "learning", "programming language"]
---

I continued reading 
[Command Line Applications in Rust](https://rust-cli.github.io/book/index.html)

Learnt that testing is easy. Any function that has `#[test]` above it, will be
found (across any files) and used by `cargo test`

Couple of interesting crates :

* [exitcode](https://crates.io/crates/exitcode) 
  * It has quite well defined exit codes. They come from FreeBSD
  * I wish other languages / frameworks had something similar
* [proptest](https://docs.rs/proptest)
  * is a property testing framework 
  * Based on python's [Hypothesis](https://hypothesis.readthedocs.io/en/latest/index.html)
  * I need to spend time actually trying this
* [human-panic](https://crates.io/crates/human-panic)
  * Generates report file on `panic`
  * Shows nice (if a bit long) message to the user, asking them to (optionally)
    email the report file to the developer 🤯

----

Things to explore:

* Write code across files.
  * So far my code is contained within `src/main.rs` 😄
* Then move the tests in a separate files.
