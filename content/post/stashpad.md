---
title: "Stashpad"
date: 2022-11-26T19:20:39+05:30
tags: ["exploring", "tools"]
externalUrl: "https://www.stashpad.com"
---

I came across stashpad during [this](https://stackoverflow.blog/2022/11/04/going-from-engineer-to-entrepreneur-takes-more-than-just-good-code-ep-503/)
Stackoverflow podcast.

What caught my attention was when Cara mentioned

> Everyone has `untitled.txt` open that they use every day

That is me!! 😆

It is really easy. `Cmd + N` to create a new empty file, and start typing.
I use BBEdit for this. I've seen my colleagues use Sublime text 
I have also used Sublime text for exactly that and only that purpose for
a long time, since it saves these unnamed files across reboots/application
restarts. One less headache. I don't have to think of what to name the file,
where to save it, format (this is easy, I'll just save it as `.txt` or `.md`)

Later I came across BBEdit, which does exactly same, so I switched.
My workflow did not change.

Stashpad promises to make the same workflow better.

I have just installed it, and haven't used it yet.

----

Ironically, I started typing my thoughts about Stashpad as a Stashpad note,
with intention that I'll copy this into a blog post "later". But decided that
why not directly write it as a blog ? 😆
