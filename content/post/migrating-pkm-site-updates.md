---
title: "Migrating PKM Site : Updates"
date: 2023-06-24T16:38:37+05:30
tags: ["misc"]
---

As mentioned [here](/post/migrating-pkm-site/) and later [here](/post/new-pkm-site/)
I recently moved PKM from Dendron to obsidian-zola

The original code seems abadoned (No updates in little over a year). I understand this since
it is likely built for personal use, and it works for them (FWIW, the demo site itself
is not updated for exactly same duration)

Turns out people forked it and improved it.

At first I tried [this](https://github.com/Yarden-zamir/obsidian-zola-plus/) fork, renamed as `obsidian-zola-plus` (as it made it better than the original)
This one seemed active (updated little over 2 months ago. Yet 2 PRs are unmerged 🤷‍♂)

But for some reason, my builds started failing on netlify. Problem did not appear related to this fork, as things worked well locally.

That is when I came across [this one](https://github.com/orditeck/cheap-publish)

It does not carry either Obsidian or Zola in the name (But repo mentions it)

What I liked is that they got rid of the landing page, which did not serve any purpose. (Just showed some fancy buttons and clever title)

