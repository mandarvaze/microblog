---
title: "Jujutsu : git compatible but better DVCS"
date: 2024-11-30T13:00:55+05:30
tags: ["tools"]
---
I learnt about Jujutsu from the "Rust in Production" 
[podcast episode](https://corrode.dev/podcast/s03e04-gitbutler/) about 
Git Butler

One of the attractive quality about it is that it works with existing git repos [^1]

First superpower : Start using it with existing git repo **locally cloned** using
`jj git init --git-repo=.` [^2]

This creates a `.jj` folder in the existing repo. `.jj` and `.git` co-exist
peacefully

But I didn't know that.

So for the first project to try Jujutsu, I `jj git clone`d my existing repo in a
new folder, so that I have "old" git repo as well as jujutsu repo.

I'm still reading the documentation and [Steve's Tutorial](https://
steveklabnik.github.io/jujutsu-tutorial/) to understand the concepts.

I've already started using `jj`.  But git interop is still a bit confusing.

One important/useful feature is that all the changes are available in git as
well, as `detached HEAD`

So I can easily switch (in git) between `main` and `detached HEAD`

I still need to learn how to commit just one file to git, since `jj` does not
have a concept of untracked changes [^3]

[^1]: unlike [fossil](https://fossil-scm.org/) which does lot more (built-in Wiki, issue tracker, forum and web interface) **but** is not compatible with git.

[^2]: I immediately added this repo to `jj` - CLI for Jujutsu

[^3]: It suggests adding files to untrack to `.gitignore` followed by `jj file untrack <PATHS>`
