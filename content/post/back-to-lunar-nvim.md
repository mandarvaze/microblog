---
title: "Back to Lunar Nvim"
date: 2022-02-16T05:53:50+05:30
useMermaid: false
tags: ["vim", "tools", "neovim"]
---

After my [problems with Doom NVim](/post/doom-nvim-maintainer-leaving/), I decided to go back to LunarNVim.

I had never really left, but since both Doom and Lunar are NVim based configurations, they may overwrite the configurations (like from `~/.local/share/nvim/`)

So I completely removed both Doom and Lunar and reinstalled.

I also installed `ltex` LSP for makrdown, which I could not do with Doom Nvim.

My other requirement was projectile equivalent. I found that there is `:Telescope projects` built-in with Lunar.

I'm now spending more time learning minor differences in keybindings e.g. Window related tasks are done via <kbd>Ctrl+w</kbd> rather than <kbd>Space+w</kbd> that I was used to.

<kbd>Space+w</kbd> *saves* the file (I think `w` for `write` rather than `window`)

----

*While writing this entry, I was surprised to find that I get auto-completion for file paths. Isn't that nice* 😄
