---
title: "Logseq : Take 2"
date: 2024-01-22T22:48:42+05:30
tags: ["note-taking", "tools"]
---

I wrote about [logseq](/post/logseq/) almost two years [ago](/post/dynalist-to-logseq/)
and then I think when I switched job, I stopped using logseq.

When I updated macOS, I forgot to install it, and forgot about it overall
(I started using `denote` - in Emacs for some time)

Recently when I started using [obsidian](/post/obsidian/) while trying l learn
more about it, I came across logseq again.

I tried it again (after 2 years)

I spent time learning basics (again)

This time I learnt about `Properties`

I installed some plugins.

Initially I installed too many.

Then removed them all. 😄

Now I have only 4 plugins.

