---
title: "Ruby Debugger"
date: 2022-01-28T10:49:29+05:30
tags: ["ruby", "learning", "programming language"]
useMermaid: false
---

While there are multiple ways to use Ruby debugger, for my first Ruby script (?) I found starting the script via `rdbg` to be the easiest.

Like `rdbg myscript.rb`

Debugger has all the basic command I have used elsewhere.

* `n` for next
* `pp` for pretty print 
* `q` for quit

For subsequent times, using `require 'debug'` followed by `binding.break` may be better.

----

This [README](https://github.com/ruby/debug) has all the details.
