---
title: "What is a micropost ?"
date: 2023-03-20T09:25:00+05:30
tags: ["meta"]
---

> You know what makes it so easy for many people to just dump their thoughts into a silo like Twitter instead of writing a post on their own site?
> **You don’t have to come up with a title for your post.**

[Source](https://matthiasott.com/notes/brain-dump)

This site has two types of posts. The normal ones with title and tags. Other are `micropost`s 
I wrote about them [here](/microposts/trying-new-theme/)

**micropost is simply an entry without a title (and tags)**
