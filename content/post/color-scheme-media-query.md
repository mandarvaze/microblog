---
title: "Color Scheme Media Query"
date: 2021-12-20T19:34:19+05:30
tags: ["hacking"]
---

It surprised me at first to notice that this site looks different based on the time of the day.
Upon debugging (aka using Browser Developer tools) I realized that the CSS has media query for `prefer-color-theme`
Learn more [here](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme) to learn more.

I made some minor changes to the theme, but I could only test one variant based on the time of the day.

That when I remembered that I had set `system auto` theme for firefox, which enabled dark mode during the evening/night.

So for testing, I enabled the `Light` theme, and was able to test my changes to the light version of the theme.
