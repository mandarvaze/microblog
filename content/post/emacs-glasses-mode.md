---
title:      "Emacs Glasses Mode"
date:       2023-06-15T15:22:44+05:30
tags:       ["emacs", "c-sharp"]
---

Now that I'm [working on](/post/dotnet-package-management/) .NET and C#, I enabled `csharp-mode` in Doom Emacs.
While exploring minor modes related to csharp-mode, I came across `Glasses` minor mode.

Here is the [EmacsWiki](https://www.emacswiki.org/emacs/GlassesMode) page.

TL;DR: 

> The default setting is to separate the Capped bits with an underscore, so EmacsIsStudly shows as Emacs_Is_Studly.

I too, prefer to see the code `as-is`, but it is good to know that Glasses mode exists. I can see it being useful.
