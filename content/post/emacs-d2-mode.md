---
title: "Emacs D2 Mode"
date: 2022-12-15T13:11:11+05:30
tags: ["tools", "emacs", "D2"]
---

It was only a matter of time before there was Emacs mode for [D2](/post/d2-diagram/) 😆 

and [here](https://github.com/andorsk/d2-mode) it is.

I'm yet to try it though.
