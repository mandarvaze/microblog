---
title: "100 Days of Rust : Day 0"
date: 2024-01-09T23:02:03+05:30
tags: ["rust", "learning", "programming language"]
---

In this new year, I have decided to learn `rust`

All my recent experience has been in dynamic languages (90% python, 10% ruby)

But I started my software development career in `C`, and did that for probably
10 years. So I'm not new to compiled languages.[^1] But things have changed a lot
 (for the better) in these years.

Additionally, `rust` is getting a lot of attention from bigwigs like MS.

----

On Day 0, I set up the rust tool chain and went through the `Hello world` 
program.

Nothing much, but at least got started.

[^1]: I came across some youtube video explaining `rust` to JS programmers,
 and it started with "Program needs to be compiled before it is executed 😄"
