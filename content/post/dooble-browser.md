---
title: "Dooble Browser"
date: 2022-01-02T13:12:34+05:30
tags: ["exploring", "tools", "browser"]
---

I came across Dooble browser via one of the Hacker news post.

The browser use Qt library and thus biased towards Linux. Or so I thought.
There is a pre-built `dmg` file for macOS.

It worked well in short test-run I did. 

I wish there was a way to set it as a default browser, so I could have tested it more thoroughly.

FWIW, it did not register itself as a browser with macOS -- which seems OS specific requirement.

Without setting it as a default browser, I need to open the links explicitly/manually in this new browser, which I am unlikely to remember during the normal workday.

One of the thing that did not work in this browser is automatic detection of dark mode for this site.

In Firefox, this site always opens in dark mode because I have set Firefox to always use Dark mode. In Auto mode it switches to Dark mode in the evenings and nights. That did not happen here.

But it is not a dealbreaker.

Yet another thing that we have come to expect is pre-installed search engines. Dooble comes with some, even the ones I had never heard of. But they are not enabled by default.
I manually added Google and DDG.

But I [can't search directly from address bar](https://github.com/textbrowser/dooble/issues/81). It felt silly. In 2022 who goes to the search engine site to search 😄

The way the integration works is user can highlight a text, and search for it using right click menu. Other browsers also have this functionality, so it is not novel.

You find more from their [github page](https://github.com/textbrowser/dooble) including downloading a release to try it for yourself.
