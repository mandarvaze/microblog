---
title: "Espanso 2.x (Alpha)"
date: 2021-12-22T17:09:52+05:30
tags: ["exploring", "tools", "productivity", "espanso"]
---

While reading the documentation on Espanso's main site, I noticed a Search bar feature.

To be sure, after some time, one is unlikely to remember all the shortcuts one has added, along with ones that came with packages.

So search bar is a welcome feature. But shortcut mentioned on the site <kbd>Alt+Space</kbd> did not work.
I was not surprised, because this key combination triggers Raycast on my machine. 

So I went looking for way to change the hotkey, and reached an issue which mentioned that search bar is a feature of `2.x Alpha` 💡

I quickly removed the old version and installed the new one, which happens to be normal macOS app (Earlier I had written that I did not realize it is a Terminal app, but that was for Legacy version)

Starting the app for the first time, pops up a nice wizard that walks you thru all the steps.

The wizard even migrated my old configuration as well.

`espanso package list` now showed the packages from legacy version, as `(legacy)`. I uninstalled and reinstalled the newer versions of both the packages -- just to be safe.

The documentation for this alpha version is still in-progress. But I found both the documentation and the app stable. 👍

As I added a `:+1:` above, a popup showed up to resolve a possible conflict. (There are two versions/shades of the thumbs up emoji)
How 😎 is that ?
