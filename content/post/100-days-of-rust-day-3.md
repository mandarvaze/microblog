---
title: "100 Days of Rust : Day 3"
date: 2024-01-12T21:46:17+05:30
tags: ["rust", "learning", "programming language"]
---

Today I finished `Functions` and `Control Flow` etc.

While these are not new "concepts" for me, one always learns something new
when learning a new language.

Two things I learnt:

1. I liked the fact that rust does not evaluate anything other than boolean 
  as `truthy` like both python and ruby
2. loop can have labels, and can be used with `break` and `continue` to 
  indicate where should the execution continue

