---
title: ".NET Package Management"
date: 2023-06-14T06:23:53+05:30
tags: [".net", "programming"]
---

I recently started writing C# code at work. While I can read and understand
the sample code (and modify partially per my requirement) I still trip up
by new-ness (to me) of the ecosystem.

So while I intuitively understand that `using` is equivalent of `require` 
in Ruby (or `import` in python), I didn't know which ones are "standard library"
and which ones are not.

The sample code worked with standard library, but writing to Azure Service bus
requires external library. Just adding `using Azure.Messaging.ServiceBus` is
not enough. One also needs to install the package.

.NET has packaging system called `NuGet`, but irrespective of the name, there 
is intuitive `dotnet` command line program to manage the packages.

I've added the TL;DR version of the commands on my [PKM](https://pkm.desipenguin.com/)

Search for `dotnet` (I can share the direct link here, but since Dendron URLs 
contain note-ids which are UUIDs, they are likely to go out-of-sync, hence 
searching is better.)