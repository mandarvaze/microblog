---
title: "Telekasten"
date: 2022-08-13T19:13:57+05:30
useMermaid: false
tags: ["vim", "tools"]
---

I had written about Telekasten [earlier](/post/telekasten-nvchad/), but in the context that I was unable to get it working with NVChad.

But I must confess, I almost forgot about it. 

I should use it daily (as intended) to capture things I'm doing, where am I blocked, what unblocked me, make it as part of my daily workflow.

I have seen my colleague keeping a simple `daily.txt` file open in Sublime, and it works for them (I hope)

It is not about the tool, more about the habit.
