---
title: "Resetting Phone : Part 1"
date: 2023-03-15T08:43:55+05:30
tags: ["misc"]
---

My Android phone started acting weird few months ago. Some apps started crashing.
I started seeing `.. app keeps closing` 😞

Then Google Play services wouldn't connect - even when phone was connected to the internet.

I found out that uninstalling the updates (for Google Play) gets it working.
But since phone was connected to the internet, it would "auto update" and would break again.

It started getting worse last few days.
I had no option but to reset my phone.

----

I started by taking backup of the data. Mainly photos.

Turns out I had not taken backup since mid-2021 😱

I also took backup of some other data files like documents and Music.

I had two authenticator apps that I needed to connect to various services at work. So I was worried
about reseeting the phone.

I considered switching to Authy - which is supposed to be compatible with Google Authenticator.
But I could not install Authy 😢 (Google Play Service broken)

But during that research, I learnt that Google Authenticator allows to export all the accounts via QR code that it generates.

I took a photo of that QR code using wife's phone, and sent it to myself on Telegram (which I can access from Desktop as well) [^1]

Then I disabled 2FA from google (*goes and turns 2FA back ON*)

Finally, I was ready to factory reset my phone.

I was bit anxious - but there was no other option.

Delaying wasn't gonna help.

So mustered all the courage, prayed the deity and clicked `Factory Reset` option from the settings.
I was asked for confirmation couple of times, and then it was done 😌

The Phone was shut down, and then I saw `Erasing` animation - gulp

and then after some time, I was greeted with the Setup.



[^1]: Later I realized that I could have also done screen capture from my own phone (Power+Volume down together - IIRC) and sent it to myself. Oh, well.
