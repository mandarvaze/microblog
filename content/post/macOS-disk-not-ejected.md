---
title: "macOS : Possible workaround for `Disk Not Ejected` error"
date: 2024-10-23T22:21:58+05:30
tags: ["macOS"]
---

See this [thread](https://discussions.apple.com/thread/254793541) titled :

**Disk Not Ejected Properly -- AGAIN AGAIN AGAIN AND AGAIN...**

I have started seeing this problem a LOT

TL;DR: Something breaks when macOS goes to sleep

Solution : Do not allow macOS to sleep when using external disk.

> I have had to resort to using an application called Amphetamine (earlier Caffeine)
> and set the 'Drive Alive' mode to prevent sleep. All the problems went away.
> It has nothing to do with 'frayed cables' or 'drives going bad'.

Based on above, I've installed the app, and trying it 🤞 [^1]

Other suggestions also suggest turning off screen saver (or other ways to 
prevent macOS from sleeping)

[^1]: It helped to some extent. But not fully. As I 
[mentioned](/post/copy-files-with-progress) I used `rsync` to copy files, and
noticed that I would get this error **while** the copy (rsync) was in progress.
so it maybe more than macOS going to sleep.
