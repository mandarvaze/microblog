---
title: "CPR for Work"
date: 2023-04-12T18:21:40+05:30
tags: ["misc", "podcast", "work"]
---

I came across this in the 1thing podcast [^1]

C-P-R stands for Communication, Planning and Resources

Nick mentions that how we sort the laundry into separate drawers so that
when we need, we can easiy find socks in their drawer, rather than looking
for then in a pile of clothes.

Similarly, at work if team members know where to look for something, it would
save them time in the long run (even though it feels like it takes time to do
that initially)

Another example : It might take an hour to fix a leaky pipe, and takes just
minutes to wipe the water from the floor, but these minutes add up, and in
long run you would be spending more than an hour, if the pipe is not fixed.

Coming back to "where do I look for .." he suggests :

**Communication** 
  - Use email only for External communication.
  - Everything else goes to either slack or teams (or equivalent tool)
  - Now when you are looking for something you know where to search, rather
    than searching emails as well as slack.

**Planning** 
  - "Tony, Welcome to the team" is an internal communication - belongs to slack
  - "Can you get me the TPS reports by 5PM on Friday" is a commitment, which 
     should never be over slack. It should go to task tracker like Asana

**Resources** 
  - Knowledge base
  - Worth writing down once rather than explaining to multiple people over
    a period of time. (Fix the leaking pipe, than moping the floor)
  - Also avoids "Only Peter knows about it, and is on vacation" (Or left the org)

[^1]: [Purposeful Productivity](https://the1thing.com/podcast-archive/393/)

