---
title: "Affirmations"
date: 2023-02-15T08:54:10+05:30
tags: ["books"]
---

1. Every day, in every way, I am getting better, better and better.
2. Negative thoughts, negative suggestions, have no influence over me at any level of mind.

-----

I came across these in a famous book [The Silva Mind Control Method](https://amzn.to/3YTe6VY)[^1]
The book is quite old. I used to see this
(along with "The word power made easy") at every bookseller while I was 
growing up. (English not being my native tongue, I ignored these back then)

Few days ago, I came across this book at the library.

I'm still going through the book, but it seems interesting.

-----

About these affirmations: The book itself is quite old, and it attributes the
first one to Dr. Emile Coué. 
I'm reading this book in early 2023, but I have heard variations of the first
one in various forms. Sometimes "in every way" is broken into specific areas 
of life like health, relationships etc. 



[^1]: Affiliate link