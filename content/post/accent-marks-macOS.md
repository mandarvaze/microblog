---
title: "How to type accent marks in macOS"
date: 2023-02-15T09:31:16+05:30
tags: ["tips", "macOS"]
---

In my [previous post](/post/affirmations/) I needed to type letter `é` but
didn't know how. (So I just copied it from the browser page that search
result returned, explaining how to type accent marks on macOS 🧠)

But later, I searched and got it working.

We use what is called as `Dead Key` method. In Keyboard viewer, pressing 
the `ALT` (or `Option`) key, shows the dead keys.

So to type `é`, I press <kbd>ALT+e</kbd>, this produces the accent, then (after
releasing `ALT`) I press `e`. This adds `é`

* `ALT+e e` : `é`
* `ALT+o o` : `ô`
* `ALT+n o` : `õ`
