---
title: "Better History Search"
date: 2024-11-18T12:39:31+05:30
tags: ["terminal", "tools"]
---

No, I'm not talking about Atuin. [^1]

I improved my search experience with just one tool - `fzf`

As you might know, `fzf` is a general purpose fuzzy finder. [^2]

But adding just `source <(fzf --zsh)` at the end of `~/.zshrc` (and/or running
it in your existing session) improves your `Ctrl+r` experience.

See the [demo](https://junegunn.github.io/fzf/shell-integration/) yourself.


[^1]: I must confess that I'm tempted multiple times. But I think their USP is history sync, which is not my use case right now. For now, I'm happy with my current solution.

[^2]: It can be used for fuzzy finding git objects. Out of the box, you get directory completion `(Alt+C)` and filename completion `(Ctrl+T)` - although it clashes with Alacritty for me (worked with Konsole)
