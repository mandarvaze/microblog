---
title: "100 Days of Rust : Day 1"
date: 2024-01-10T23:15:59+05:30
tags: ["rust", "learning", "programming language"]
---

Today I finished the `Number guessing game`

During the early part of the code, my mind started thinking "How will comparing
string input from the user, work with a number? 🤔" (💪 of having gone through
such issues numerous times over the years)

Of course, it was explained later.

I was a bit disappointed that the code `panic`ed upon entering non-number 😞
especially after the `expect` clause.

Turns out that is expected behaviour.

The code will still panic, but `expect` helps the (end) user with slightly 
better error message

I also made changes in the `loop` section, that led to an error, which I was
able to resolve (more based on the intuition, rather than logic 😅)

----

Initially I used `nvim` ( I was mostly copying the code from the book, 
and/or making minor edits) but soon realized that [helix editor](/tags/helix/)
is better suited for rust development.

So set it up.

Getting LSP working took tinkering with `languages.toml` , but got it working!
