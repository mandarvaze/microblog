---
title: "Understanding Mastodon"
date: 2022-11-22T22:00:35+05:30
tags: ["mastodon"]
---

After Elon Musk's takeover, there has been a lot of discussion about leaving twitter.

TBH, I am not that active on twitter, and I am not sure if charging for blue tick affects me 😄

Yet, I have been burnt in past by having my data lost, because it was on someone else's platform.

I do not consider my tweets that valuable, yet it seems I should have a back up, just in case.

Since everyone and their grandma are moving to Mastodon, I decided to check it.

(*I also checked [Koo](https://www.kooapp.com/feed), but it seemed more like Facebook than Twitter. Not for me*)

Mastodon is decentralized. Some people consider it as its strength.

~~Maybe~~. It is.  For those who are worried about one company having control over their account. 
Accounts do get banned on Mastodon as well. Read Wil Wheaton's [post](https://wilwheaton.net/2018/08/the-world-is-a-terrible-place-right-now-and-thats-largely-because-it-is-what-we-make-it/)

Another strength that is mentioned is "You can even host your own instance"
but normal people do not have expertise to do that, and some that have the 
expertise may not have time and inclination to have one.

I certainly don't (have the expertise. And while I could read up the documentation
and set something up for fun, I don't want to worry about server upkeep, Thank you very much)

But it is what it is.

So now the question becomes where do I create an account.

At first, it was not clear. Analogy given is that of Email. Anyone can send email from gmail.com to yahoo.com
Similarly, you can have account anywhere, and you can follow people on other servers.

A lot of the servers are for specific topic they care about. Like Reddit communities.
But my account is on Reddit.

Here my account is on server specific to Emacs, or Ruby language.

While I might be using both these now, I do not wish to be identified by the use of my editor or programming language.

I do change both. (20+ years vi user, 5+ on Emacs, now trying Helix. After 10 years of Python, Ruby for the last 8 months)

There are some generic (sounding) ones like https://mastodon.social/ and https://mstdn.io/

But neither of them are accepting new users.

(That is another problem of hosting your own servers. Who pays for the infra cost?)

Finally, I found https://indieweb.social/ 🎉

I've applied for account.

The request will be reviewed and approved 🤞

----

Read [this](https://lucumr.pocoo.org/2022/11/14/scaling-mastodon/#footnote-reference-1)
footnote in Armin Ronacher's post about how decentralized banking is better than decentralized crypto.
