---
title: "Memos: A lightweight, self-hosted memo hub"
date: 2023-02-14T08:49:21+05:30
tags: ["note-taking", "pkm", "tools"]
externalUrl: https://usememos.com/ 
---

I'm not sure what the main feature of this app.

Memos can be anything 

Somewhere it says "Twitter like", and it does have different visibility settings.
Default being "Only visible to you", but it can be changed.

It also has user management, so it may be useful for say family with 3-5 users,
may be smaller start up 🤷‍♂

I think the best part is ability to self-host. Maybe on Rpi, or in house server
(If considering for family)

For a small startup, cheap host may be better.

All the data is stored inside `~/.memos` folder locally, which is mounted inside
docker container.

Docker seems to be the only officially supported/documented/the easiest 
option to get started.
They also have well written document for deploying to render (I might try it
if I stay with it for a while - or not 😄)

Since all the data is stored in `~/.memos` (in a SQLite DB, if it matters) it
is very easy to migrate to different machine.

Currently, my *real* `.memos` folder resides inside Dropbox, and a symlink
from `~` points to this real folder. So I can access/update the same data
from two different machines (I tested it, it works)