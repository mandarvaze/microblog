---
title: "Neovide with LunarVim config"
date: 2022-04-19T21:38:28+05:30
useMermaid: false
tags: ["vim", "tools", "neovim"]
---

So I tried VimR for some time, but it always felt odd. Sure all the LunarVim keybindings worked.
But VimR wasn't just Neovim client. It comes with its own features like the side panes for Files, Buffers, Markdown etc.

Also, it won't really quit. `:q!` would destroy the window, but it would remain in the dock.

So for main work, I used Emacs.

Then today in Doom Emacs Telegram group, someone started talking about Vim. 
After some time, I ended up subscribing to Vim Telegram group, and asked about better neovim GUI.

First suggestion : VimR 😆

When I explained, someone suggested Neovide. I think I had looked at it earlier. But thinking that I need to compile from the source, I decided not to try it.

Turn out they have binary releases for macOS

and they have a new release just 7 days ago (As of this writing)

Getting it working was simple. 

I needed to set the font correctly. Out of the box, it looked ugly.

I had to set `vim.opt.guifont = "FiraCode Nerd Font:h13"` in `~/.config/nvim/config.lua`

All is working well. 

I think Neovide is better than VimR
