---
title: "Using VSCode"
date: 2022-12-10T08:44:24+05:30
tags: ["tools", "vscode", "editor"]
---

I installed and used VSCode on office provided MBP yesterday.
It is not like I have not used it in past. I tried vscode-vim plugin
for modal editing. It allows you to embed neovim in VSCode (So all the
configuration of neovim is available here.)

But on my old machine, it was slow.

(I also tried VSCodium, thinking it might be faster. It wasn't)

I used it exclusively for Dendron (my [PKM](https://pkm.desipenguin.com))
cause it is mainly VSCode plugin (While it has command line tools, the main 
strength is as VScode plugin)

But never for coding.

Till yesterday.

Confession : It quite liked it. 

I mainly needed it to go over large RoR project codebase.

I tried `solargraph` LSP with helix (and Emacs. `robe-mode` is slightly better)
But it is documented NOT to work with Rails.

It doesn't work with VSCode either.

But `Find in files` on my newer machine is quite fast (Goto definition still
does not work 😞)

I occasionally started with `insert mode` 😆 - but non-modal also worked.

Confession : I didn't mind it (Maybe enjoyed it a little bit)

There.

Confessed!

----

I'm unlikely to switch to VSCode completely.

Both helix and Emacs have modal editing, and after 2 decades of muscle memory
I don't want to go back to non-modal.


