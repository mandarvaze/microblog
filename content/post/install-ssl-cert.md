---
title: "Install LetsEncrypt certificate"
date: 2021-07-24T17:00:18+05:30
useMermaid: false
tags: ["security", "devops", "linux"]
---

Today I installed LetsEncrypt certificate on one of the production server.

I can not believe it is so simple to secure a website running on an Ubuntu server

```shell
sudo apt-get install certbot python-certbot-nginx
sudo certbot --nginx -d mydomain.com -d www.mydomain.com
```

It even configures nginx (or apache) server for you. 

To manually renew run the following:

```shell
certbot renew --quiet
```

But you don't have to, cause `certbot` is so awesome that it automatically adds an entry into crontab so that certbot runs twice daily. There is an entry under `/etc/cron.d/certbot`

Run the following to check the status of renewal service :

```shell
sudo systemctl status certbot.timer
```

Finally,

> If the automated renewal process ever fails, Let’s Encrypt will send a message to the email you specified, warning you when your certificate is about to expire.
