---
title: "Added GA Support to Indiefeed Theme"
date: 2022-02-08T19:51:07+05:30
tags: ["hacking", "meta"]
useMermaid: false
---

I started this blog using indiefeed theme. But soon realized that the original creator has moved on. They have marked the repo as read-only on github.
I had already started tweaking it to my liking, but now changes are more than tweaks. e.g. The original theme did not have good image support. i.e. only images used were in the Author profile image, and it has rounded corners/circular shape, which does not work when image is part of the post itself.

So I fixed that.

I also added Mermaid diagram support.

Since I have been writing consistently on this blog, I was curious to see if consistency pays of, and how.

My [other blog](https://learnings.desipenguin.com) uses Google Analytics to track this. So at first I thought : "I'll just create another property and start tracking"

But it had two problems:

1. Indiefeed theme did not support Google Analytics
2. GA has come up with GAv4, so creating property is a bit different.

Second of the two was easy to overcome.

Hugo supports both versions. `v4` and `v3` (But it occurred to me late) But to be safe, I create Universal Analytics using `Show Advanced Options` button.

First one took minor code changes to the theme.

Hugo supports GA "out of the box", but theme creator needs to enable it.

I needed to include the built-in template in the `<head>` section. If interested, see [here](https://github.com/mandarvaze/indiefeed/commit/e4214af4ea1b686a6cbd7e140ad013d0cbcbcf8e#diff-96dd75a968976edd5e03170268ed9085733f75c3fb24f992ae613c89e6de42dcR54)
