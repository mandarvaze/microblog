---
title: "How to find people to follow on Mastodon"
date: 2022-11-25T08:30:25+05:30
tags: ["mastodon"]
---

After creating an account on Mastodon, next question was how to find people
to follow ? 🤔

While mastodon has existed for a while, only recently it started getting
attention. So lot of people you follow are *not* on Mastodon. Yet.

I followed a couple of approaches.

First, I searched for topics I am interested in. I started with Emacs and Ruby.
Found a few accounts to follow.

Then I literally searched `find tweeple of mastodon`. It turns out a lot of
sites like Wired, PCMag, HowToGeek are writing about it. Most of these articles
are from less than a month old 😄

One tool that worked flawlessly for me was [Fedifinder](https://fedifinder.glitch.me/)

Finally, I searched for exact same twitter handles on Mastodon.
Chances are people select one handle, and as long as it is available, they'll 
prefer that everywhere.

This worked surprisingly well.

----

Two things to remember though :

1. Mastodon is still not as popular, so people you follow, on Twitter may not be on Mastodon. Yet.
2. Mastodon search isn't that great. Could be because of technical reasons. Or same reason as previous one 😄

