---
title: "Zsh Autosuggestions"
date: 2022-12-06T09:35:09+05:30
tags: ["terminal", "zsh"]
---

I used `zsh` for couple of years, and then switched to `fish` mainly for
its built-in autocompletion.

fish is a modern shell (just like helix) and thus (I think) it does several
things differently. For someone using various shells for over 2 decades,
resisting muscle memory does not come easy. [^1]

I was used to `export KEY=value`, so in fish I always had to look up `How to 
set an environment variable in fish` 😆 - even after 2 years.

Third party programs do not always work with `fish` - usually I was able to
find a way, but if not, I would just invoke either bash or zsh and get work
done. (and switch back to fish when done) But I would miss the auto completion
when I switched.

So when a colleague told me about zsh auto suggestions, I was thrilled.
But I lost the link.

When searched, I came across [zsh autocomplete](https://github.com/marlonrichert/zsh-autocomplete)
which seems to be under active development. I tried it for few days, but I
did not like it much. It seemed too intrusive.

Then I asked the person who had suggested it earlier, and got 
[this](https://github.com/zsh-users/zsh-autosuggestions)

Turns out, I had come across it earlier in my search, but the repo isn't updated
since mid-2021 😞 - so I had ignored it.

Turns out, it works very well. Just like `fish`

Maybe it is stable enough that it does not need code changes 😄

Installing it via `oh-my-zsh` is trivial (and documented).

So I've switched back to `zsh`


[^1]: I did it multiple times with various degrees of success. zsh -> fish,
vi(m) to emacs (Though I continued using evil/modal editing) -> helix, and
python -> ruby (struggling)


