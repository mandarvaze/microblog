---
title: "Linux Mint"
date: 2025-02-16T06:58:46+05:30
tags: ["linux"]
---

I've been using [Linux](/tags/linux/) on my old MBP since November of last year.

I started with [OpenSUSE](/post/opensuse-on-mac/)

After using it for a month or so, I switched to Tuxedo OS 4
It is based on Ubuntu 24.04 LTS but ships with latest KDE Plasma 6.
 
I meant to write about it, but didn't  ¯\_(ツ)_/¯

I used it for 3+ months.

It was more mainstream than OpenSUSE (because of its Ubuntu base)

But there were paper cuts. Things that are not show stoppers, but minor
incoviniences that add up over time. (I'll write about it separately, maybe)

Then I came across ParrotOS, which is based off Debian Stable (with focus on
security) I tried that Live ISO, and liked it.

It is based on Gnome. Tuxedo had KDE Plasma (which I still think is better
but I understand it is subjective.)

I didn't dislike it. I was about to install ParrotOS on my laptop, but then
decided against it

Because the distro itself is not main stream enough. (Most Debian instructions
 would work, but if they didn't they may not be much support)

So I looked at Linux Mint. I had tried the Live ISO long time ago, but didn't
like it. But now my opinion had changed.

I tried the Cinnamon version of it. It worked well.

So as of three days ago, I replaced Tuxedo [^1] with Linux Mint 22.1

It (Cinnamon version at least) does not support Wayland (says experimental).

I tried it, and it shows. The right click menu on the try icons showed up
in the middle of the screen, not near the icon  ¯\_(ツ)_/¯

To be honest, I'm not sure whether (and how) Wayland is better for the end user.
Whatever it maybe, I'm on X11 with Cinnamon.

Visually, I see no difference.

On the other hand, X11 being older of the two, if well supported by apps like
espanso for example.

On Tuxedo+Plasma+Wayland, I had to manually compile espanso, and while it
worked (for most) there were issues. Specifically, there was a problem with
Alacritty.

Expansions did not work in the terminal at all.

and they did not work elsewhere either if Alacritty was started after espanso.
(which was the case all the time)

So I had to manually stop and start espanso **after** alacritty started.

espanso works well with X11.

and now I'm using Ghostty instead of Alacritty (Cause Ghostty did not work
with Tuxedo)

expansions work well inside the terminal apps 🎉

I could not get [kinto](https://kinto.sh) to work with Linux mint.

It turned out to be blessing in disguise.

I discovered Toshy in the process of troubleshooting the issue.

I'm very happy with Toshy 😄

It has been only a few days since my switch. So it maybe too early, but overall
I'm happy  with my switch.
 
[^1]: To be honest, I miss KDE Plasma's Quality of Life features.

