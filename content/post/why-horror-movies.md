---
title: "Why do people watch horror movies?"
date: 2022-12-28T09:46:29+05:30
tags: ["podcasts"]
---

Today, I listened to this [episode](https://freakonomics.com/podcast/why-do-people-love-horror-movies/)
 of NSQ - No stupid questions - podcast.

Angela mentioned that she has watched 4 out of top 10 horror movies (from some list)
Dubner was 0/10

Turns out, Angela watched all those only during her twenties or earlier.

Some study noticed that audience for such movies comprises mostly young adults.
(Because they are seeking thrill ?)

Psychologists Paul Rozin calls this "benign masochism"

------

In the book "Illusions", Richard Bach asked Donald Shimoda the same question,
and was answered as :

> Because they think they deserve it for horrifying somebody else
> Or they like the excitement of horrification


------

Two ways to look at the same question 🙂
