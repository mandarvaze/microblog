---
title: "Rust: Improve readability in default arm of match statement"
date: 2025-02-04T20:00:16+05:30
tags: ["rust"]
---

#TIL in #rustlang we can use any variable name (starting with _) in
ignore path of match

<!--more-->

Instead of

```rust
let x = 1;

match x {
  1 => println!("one"),
  2 => println!("two"),
  3 => println!("three"),
  _ => println!("anything"),
}
```

We can write

```rust
let x = 1;

match x {
  1 => println!("one"),
  2 => println!("two"),
  3 => println!("three"),
  _ignore_this => {},`
}
```

Sure, this is a silly example
(Original [example](https://doc.rust-lang.org/book/ch18-03-pattern-syntax.html#matching-literals)
from the Rust book)

See [this commit](https://github.com/mandarvaze/snb/commit/1f6e31a1d56102c71b03f68bea42b047002efadc)
 for real-world usage.

As you can see, I removed the (now defunct) code comment, by using better variable name over just `_`

If we don't want to ignore the value (I know this also seems like a made up
example, still ..) then

```rust
let x = 1;

match x {
  1 => println!("one"),
  2 => println!("two"),
  3 => println!("three"),
  unexpected => println!("Got unexpected number : {}", unexpected),
}
```

No need for `_` at the beginning 😄 
