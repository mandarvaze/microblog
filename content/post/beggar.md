---
title: "Beggar"
date: 2022-11-27T11:26:04+05:30
tags: ["misc"]
---

Yesterday, after I came out of a shop, some elderly woman was begging.
Near that shop, it is not uncommon.

She did not look like a beggar. Yet she was.

At first, I gave her Rs. 50 - which maybe more than what people usually give
beggars (I think).

I was surprised that she said I don't want money, I want rice and daal
(lentils) instead. I was positively surprised. Not many beggars ask for things.
I told her to buy grocery herself. There is a grocery store next door. 

She said, the shopkeeper won't let me purchase stuff.

This wasn't uncommon.

So I went with her to grocery store, and asked the shopkeeper to get her rice 
and daal as she had asked. I didn't want to get into the transaction, I just
wanted to make sure shopkeeper does not shoo her away. Also, Rs. 50 won't get 
her the stuff she needed, so I wanted to also pay for groceries.

When shopkeeper was done packing rice and daal, I asked him how much I owed
him. 

At that time, the lady said : "I also need sugar"

I was taken aback, but didn't say anything.

Shopkeeper went to pack sugar.

and then she said "cooking oil" as well.

Shopkeeper was now wondering what to do.

At that time, I told him whatever he has packed is enough.

I ended up paying upward of Rs. 300

As soon as I got my change back, I left.

-------

I kept thinking :

* Did I do the right thing ? (Yes, I was glad I helped with her with groceries.
I think what took me by surprise was that she asked for more and more stuff once
at the shop.)
* I didn't know how much it costs for bare minimum groceries. (I didn't pay for
cooking oil.) I am aware that there are shops which sell at lower price. Still..
* Is paying Rs. 10/50 to beggars right thing to do ? I know most of them might
buy street food, which is easily available for that amount. It is not healthy
probably not nutritious either.

------

I did notice when I left that even though she mentioned that she did not want
money, she kept the initial Rs. 50 I gave her 😆
