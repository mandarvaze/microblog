---
title: "Import data from Dockerized Postgres to Elasticsearch"
date: 2022-06-08T06:29:31+05:30
useMermaid: false
tags: ["elastic", "published a blog post"]
---

After setting up Elasticsearch [locally](post/setup-elastic-search-docker/) I need to seed it with some dummy data.

I came across [this article](https://ericlondon.com/2018/05/12/export-data-from-postgresql-as-json-and-import-into-elasticsearch.html) that explain all the steps in details, including populating the postgres with dummy data to begin with.

But I could not use it as-is because I use dockerized postgres.

I also ran into problem with `https` and `elasticdump`

I wrote about the problems and solutions on my other blog. Read it [here](https://learnings.desipenguin.com/post/postgres-to-elastic/)
