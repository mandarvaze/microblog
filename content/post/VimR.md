---
title: "VimR"
date: 2022-02-23T19:38:40+05:30
useMermaid: false
tags: ["vim", "tools", "neovim"]
---

After setting up LunarVim, I wanted to get it to work with a NeoVim GUI 

I had tried some in the past, but did not like any.

Then (I think on Discord) I came across some discussion that they got LunarVim (`lvim` henceforth) to work with Neoclide.

I couldn't even get it to run 😞

I tried VimR, and it worked. i.e. I got it running. But I didn't like their default look.

Since I was already happy with LunarVim, I thought why not use VimR GUI with `lvim` config ?

Apparently it is not so straight forward.

It is/was a known issue, which is apparently [fixed](https://github.com/LunarVim/LunarVim/pull/1777)

But I'm still unable to get it to work.
