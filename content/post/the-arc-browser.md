---
title: "The Arc Browser"
date: 2023-03-06T11:06:44+05:30
tags: ["tools", "browser"]
externalUrl: https://arc.net/
---

Finally, got the access to the Arc browser.

Quick overview of what it is.

It is build on top of Chromium browser engine. But adds difference experience altogether. Benefit of using Chromium is that most of the extensions that work with Google Chrome already work. In fact, one installs the extensions from Google Chrome webstore itself 😄

Most awesome feature that made me want to try it is that it will *Auto-close inactive tabs* after 12 hours (default - can be increased to 24 hours, 7 or 30 days)

They have and publicize the keyboard shortcuts. All browsers have keyboard shortcuts, but other the usual ones to open and close new window/tab, I hardly use any. 

I also tried `Notes` that can be shared with anyone. I'm unlikely to use the share functionality. But the `Easel` - which allows drawing (and sharing) - seems interesting.

The concept of `Little Arc` is interesting. It is a smaller (hence `Little`) floating browser window.

They also have `split view` which I'm yet to explore. Seems useful when using bigger monitor.

-----
If you want to try it yourself, [here is an invite link](https://arc.net/gift/bf73b46)
