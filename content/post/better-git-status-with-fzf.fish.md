---
title: "Better Git Status With fzf.fish"
date: 2022-10-03T22:01:07+05:30
useMermaid: false
tags: ["terminal", "fish"]
---

Towards the beginning of the year, I had written about [Fuzzy Search in fish](post/fzf-with-fish/)
For the better part of the year. I only used `Ctrl-R` functionality, that too occasionally.

Then today, I came across the same again, and after going through the README, I noticed at least one more useful keybinding.

`git status` normally would just list the modified files etc. To see what has changed, one needs to `git diff <filename>`

with `fzf.fish` one can combine these two functions with `git status` followed by `Ctrl+Alt+S` (`S` for status, easy to remember)

Now one sees, the list on the left hand side, and the diffs on the right.

This seems relatively new functionality.

Refer to the [relevant part of the README](https://github.com/PatrickF1/fzf.fish#modified-paths) for details.

