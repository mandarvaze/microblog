---
title: "IRC"
date: 2022-01-15T17:40:11+05:30
useMermaid: false
tags: ["terminal", "emacs"]
---

I could not believe myself, that I finally created an IRC account.

Lot of people have asked how come I am not on IRC. They told me that we get to interact with smart people on IRC, including creators of great software, libraries, framework.
But for some reason, I resisted. It felt complex, compared to modern communities like Telegram, Discord etc.

Today someone suggested it again on the Doom Emacs Telegram channel, and this time I decided to give an honest try.

So here I am.

### Choosing a client

While there are a lot of IRC client apps to choose from, I decided to continue with ERC built into Emacs 😄
This means I don't need to switch to a different app. Plus IRC being text based, it makes sense it belongs to an ~~editor~~ app like Emacs 😉

I had connected to IRC servers in the past, only briefly. So I know there is a nickname on IRC.
But like any other social app, you don't need an account (usually paired with an email and password)

Account is optional.

I wasn't sure how that works.

So I went ahead and [registered](https://libera.chat/guides/registration)

### Registration

Since IRC is text based protocol, the registration is via :
`/msg NickServ REGISTER YourPassword youremail@example.com`

The password in plain text. But I assume it is only in my Emacs.
Even in browser, technically it is visible till sent over the wire
so 🤞

I got email confirmation email, and verification was again a command just like above
`/msg NickServ VERIFY REGISTER YourNickName YourVerificationCode`

Email contained exact command to paste. 👍

### I don't know how to Quit 😄

`I don't know how to quit` may be a motivational line, Or SO question about vi/vim
But in my case, I didn't know how to exit out of `ERC` session.

Following are actual attempts 😆

```
<mandarvaze> quit
No target

<mandarvaze> exit
No target
```

Finally, I RTFM and figured `/quit` to disconnect from a server.

### Joining (and leaving) a channel

`/join #pythonpune`
`/part #pythonpune`

I found basic IRC guide [here](https://libera.chat/guides/basics)

----

*In case you are curious, IRC stands for Internet Relay Chat*
