---
title: "Setup Elastic Search Locally via Docker"
date: 2022-06-01T16:01:57+05:30
useMermaid: false
tags: ["elk", "tools", "elastic"]
---

This is the first time I'm setting up `elasticsearch`

While one can set it up natively via `brew` I have learnt longtime ago to use docker for complex, server type applications.

Elastic search's official document also suggests the docker in their [Getting Started](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started.html).

I've made summary of the steps on my [PKM](https://pkm.desipenguin.com/). Search `elastic` on the site, since URLs are likely to change.

Important thing : Create a local user (I called it `dev`) and make it `superuser`

This is useful in case you misplace the auto-generated password for built-in superuser user named `elastic`
