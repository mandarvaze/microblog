---
title: "Make constraints useful"
date: 2025-01-22T10:53:04+05:30
tags: ["misc"]
---

Recently, I came re-listened to a podcast episode where they discussed about
constraints.

Everyone thinks that constraints are bad.

But having some constraints may be good.

<!--more-->

If there are no constraints, it may lead to analysis-parallysis.
It is not easy to choose when there are too many choices.

Let us say group of friends (or colleagues) want to go out for dinner.

"Where should we go" may take forever, if there are no constraints.

But if you put conditions like say "No pizza" (or "we are going for a pizza, now
where should we go?") Or "Vegetarian only" now, choices should be limited, and
hopefully you'll decide faster 😄

Same for "What to wear to office ?" (Remember famous Steve Jobs rule about
Turtleneck and Jeans ?)

Back to the podcast episode.

They talked about "Story in only 6 words", "A book without letter E".

Those are extreme constraints, but could be fun.

Personal example of using constraints to my advantage [^1]

My machine suddely loses network [^2] Only solution (so far) is to reboot.

So when this happened (again) this morning, I decided to see how much time
before I **must** have internet.

I used this time to:

- Read ebook that was already downloaded on my machine
- Solve some rustlings problems [^3]
- Write this blog post
- I also did some pending `git commit`s. The `git push` can wait till after
  the reboot.

I'm sure there can be more. Since this happens every now and then (This is not
the first time I delayed the reboot, but writing about it just now)

[^1]: Truth be told, the constraint was forced, not by choice. But there are examples of people choosing devices without internet connection to avoid distractions.
[^2]: I have found some possible solutions, yet to implement them. I wonder if I should delay "fixing" the problem 🤔 😄
[^3]: Hint asked to read rust documentation, but then I remembered that `rustup doc` lets me access rust documentation locally without internet 🎉 

