---
title: "Finally got Auto Complete working in Helix"
date: 2024-12-27T12:55:23+05:30
tags: ["helix", "tools", "rust"]
---

I have been using Helix as an editor, not as an IDE.
One of the USP of Helix is `LSP support out of the box`

Recently, I've been programming in rust.

Helix is also written in rust.

So I think it is natural that they would support rust LSP `rust-analyzer`
and it does.

But I wasn't getting auto completions 😞

<!--more-->

I had `rust-analyzer` installed (or so I thought) and helix health check also
passed.

```sh
$ hx --health rust
Configured language servers:
  ✓ rust-analyzer: /home/mandar/.cargo/bin/rust-analyzer
```

Yet, I did not get auto-complete. helix log showed some error related to
`rust-analyzer` not being there.

How can it be ?

Let us check if the file really exists.

```sh
$ file ~/.cargo/bin/rust-analyzer
/home/mandar/.cargo/bin/rust-analyzer: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 2.6.32, with debug_info, not stripped
```

OK, path listed by Helix health exists, and it is not a link. It is a proper ELF
64-bit binary.

Maybe permission issue, or something else 🤔

Let me try to run it without argument.   

```sh
$ ~/.cargo/bin/rust-analyzer
error: Unknown binary 'rust-analyzer' in official toolchain 'stable-x86_64-unknown-linux-gnu'.
```

What ??

This is same error listed by helix logs.

No wonder auto completons do not work.

After some searching, I found the solution 🎉

**`rustup component add rust-analyzer`**

But did I have `rust-analyzer` already in my `~/.cargo/bin`

A kind soul on reddit pointed me to [this](https://github.com/rust-lang/rustup/issues/3846) issue.

As of this writing, the issue is still open. But it helps to know I'm not the
only one with this issue 😌

-----

*Turns out I've already written about similar error but for Emacs [here](/post/rust-analyzer/). Doh!*
