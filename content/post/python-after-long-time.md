---
title: "Used Python After Long Time"
date: 2023-05-12T13:36:14+05:30
tags: ["misc", "python"]
---

Today I wrote (OK, copy/pasted sample code and modified) python after more than
a year. (Is it `a year` or `an year` - confusing. I think either works)

I had to start from installing python.

Why did I use python ?

At work, we are trying to connect to Azure Service Bus over AMQP.
Ruby is not officially supported by MS anymore.

Python is.

But we wanted to try non-azure client. So went with qpid-proton (which also has
Ruby version, but I was unable to get it even installed - on macOS - after 
struggling a lot)

Python sample code worked - but only because of [this](https://www.johndehavilland.com/blog/2017/07/05/Python-ServiceBus-AMQP.html) article.

John de Havilland: God bless you for mentioning `allowed_mechs` options specifically. 🙏
