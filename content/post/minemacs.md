---
title: "Minemacs"
date: 2023-01-31T09:17:38+05:30
tags: ["emacs", "tools"]
externalUrl: https://github.com/abougouffa/minemacs
---

I came across this new starter configuration, described as
"Minimal Emacs config for daily use" via Sacha Chua's Weekly Emacs Newsletter [^1]
If you are Emacs user, you should subscribe. You don't have to share your email
unless you want to. She shares the links on Mastodon[^2] (and other social apps)

Back to Minemacs 😄

It supports emacs versions 28.2 and above (But I got it working with 28.1[^3])

It is kinda refreshing. 
Doom Emacs is quite solid, and I've tweaked it to my liking over the years, but 
wanted to try something new.

The developer is very responsive.

I also discovered org-roam by perusing through source code. (Hopefully another
post about org-roam soon)

I think you should give it a try.

[^1]: [Weekly Emacs News](https://sachachua.com/blog/category/emacs-news/)
[^2]: [Sacha Shua on Mastodon](https://emacs.ch/@sachac)
[^3]: [Minemacs on emacs 28.1](https://github.com/abougouffa/minemacs/issues/11)