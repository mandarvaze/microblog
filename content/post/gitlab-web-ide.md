---
title: "Gitlab WebIDE"
date: 2023-02-27T11:12:44+05:30
tags: ["meta"]
---

This week I'm travelling for work, and hence carrying only my work laptop.
So far all my microblog entries were written from my personal laptop

I have option to clone the repo on my work machine, or not publish anything
for this entire week (it is not like I publish every day, still ..)

I prefer not to clone my perosnal repo on work machine, and missing publishing
also something I don't want.

Then I remembered someone mentioning `CMS for static sites` and how it is
just an editor in browser, that is when I struck me that this may be a good
opportunity to try `Gitlab WebIDE`

-------

It looks like `VSCode in browser` although Market place is disabled.

For a simple markdown, I do not need any fancy IDE anyway. 
Decent syntax highlighting (which works) is enough.

I do miss modal editing (I use helix for these posts) but that is OK.

So here is my first post not written using native editor 😄