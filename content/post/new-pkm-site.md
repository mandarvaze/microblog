---
title: "New PKM Site"
date: 2023-06-18T17:01:52+05:30
tags: ["misc"]
externalUrl: https://pkm.desipenguin.com/
---

Finally migrated my PKM from Dendron to Obsidian-Zola

Things I needed to do were :

1. Remove Dendron specific frontmatter
2. [In Progress] Restructure flat file names that were like `Dev.DB.mysql.md` to `Development/Databases/mysql.md`
3. Pushed this branch as `main`
4. Make `main` as default and protected branch
5. Unprotect `master`
6. Renamed old `master` as `dendron` (just in case)- without unprotecting old `master` I could not rename it.
7. Remove Dendron specific settings from netlify. Since this time there is `netlify.toml` so no additional settings needed.

I would say it was pretty smooth transition 😄