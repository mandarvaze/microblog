---
title: "guifont in Spacevim"
date: 2022-05-27T15:55:05+05:30
useMermaid: false
tags: ["vim", "tools", "neovim"]
---

In my tour of Neovim starter kits, I landed on [SpaceVim](https://spacevim.org/)

I started my Emacs journey with Spacemacs, and used it for couple of years before settling on Doom Emacs.

Spacemacs inspired SpaceVim.

I had liked the mnenomic keybindings in Spacemacs (and now in SpaceVim)

But Spacevim is different from how other starter kits handle configuration.

Last week, I tried DoomVim (inspired by Doom Emacs) and NVChad (and "Neovim from scratch" by same folks as LunarVim)
They all had similar configuration, based on `lua`

But SpaceVim has its own style.

For starters, it uses `init.toml` for the configuration. 

Also, it uses `bundle` instead of `packer` (which I had grown used to)

Nonetheless, using SpaceVim was pleasant experience.

But I was unable to set `guifont` and thus saw empty square at one place, and font was too big for me.

If I didn't use Neovide i.e. use nvim in terminal, I could see the icons in place of empty square, but I want to use GUI.

Turns out there is a bug (somewhere)

(At least) Neovide returns `0` for `has('gui_running')` so `guifont` value is never set internally.

See [this](https://github.com/SpaceVim/SpaceVim/blob/9f4a0ddbb394b2f5d20836754bcecbb675d047b0/autoload/SpaceVim.vim#L1552)

Then I unconditionally set guifont in my local `SpaceVim.vim`

For now, it works.

-----

Let us see how long I stick with SpaceVim.

I got LSP working for Ruby, more tweaks like `ltex` are pending. (and SymbolOutline, and IndentBlankLine and .....)
