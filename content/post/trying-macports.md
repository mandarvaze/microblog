---
title: "Trying Macports"
date: 2023-07-05T10:55:47+05:30
tags: ["macOS"]
---

I was aware of macports for quite some time (since I moved to macOS about 8 years ago)
But most of the tools seem to be suggesting `brew install`, so I didn't give macports
a second thought.

8 years later, my MBP is old, and soon will not be supported by Apple and `brew`

While I [upgraded](/post/upgrade-to-monterey/) to macOS monterey, this is going to be last version supported on this hardware.

Someone on mastodon [suggested](https://indieweb.social/@m2m/110509820378135216) macports are supported better on older versions of macOS

So today I decided to give it a try.

I still have brew packages (everything so far) and I'll be using macports side-by-side.

First impressions:

* Uses GUI installer, as opposed to how `brew` installs itself. Considering that `port` itself is to be
  used via the CLI, GUI for installer seems odd.
* Requires `sudo` to install the packages, since it requires write permission to `/opt/local`

Other than these, it is too early to have any other impressions 😄

(The first package I installed in `nushell` - something else to try out)
