---
title: "Doom Nvim"
date: 2022-02-10T19:43:04+05:30
useMermaid: false
tags: ["exploring", "vim", "tools", "neovim"]
---

Those who know me, probably already know that Doom Emacs has been my daily driver for a last years now.

and that recently neovim has piqued my interest.

I already wrote about [Lunar NVim](/post/lunar-nvim/)

Today I discovered Doom-Nvim

Port of (sorta) Doom emacs to neovim ecosystem.

What is not to like ? 😄

As I wrote in Lunar NVim post, I am happy with Doom emacs. But this might make me switch back to (n)vim 😄

But seriously, I have used it only for couple of hours [^1]

# Things I miss

* ltex-ls : LSP for text ? Gives me grammar tips (and spell checks)
* projectile : Switch between different projects easily, have multiple projects open in different tabs at the same time.
* magit : There is neogit, and it is bit behind in terms of maturity I think. e.g. I could not add new file from `git status` window.

There may be equivalent for both of these in neovim ecosystem, I just have not looked yet.

So while I won't switch away from Doom emacs tomorrow, there is a high chance that I'll look for equivalents here, and give an honest try to make this my new IDE

But I find this more aligned with my muscle memory, so I'm definitely switching away from Lunar NVim

(Ironically, I found out about Doom Nvim in a reddit thread reviewing Lunar Nvim 😆

----

[^1]: I wrote this entire entry in Doom-nvim. Talk about meta!
