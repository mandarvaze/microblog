---
title: "Bullshit Jobs"
date: 2023-05-02T13:37:09+05:30
tags: ["misc"]
---

[Bullshit Jobs](https://en.wikipedia.org/wiki/Bullshit_Jobs) are meaningless jobs. Jobs which if taken away, won't matter to anyone (Maybe except to the person who is/was in that position)

While responding to a listener question, DHH mentioned [^1] that it is actually a book, based on a study of such jobs.

He mentioned that BigTech <sup>TM</sup> hires talent so that their competition can not hire the same person, even if the hiring company does not really **need** the highly talented individual. (Or have enough/great work to fulfill such a person)

Sometimes I feel like that in my current role. 🤔 (The Pay is Good <sup>TM</sup> though 😉)
It is not like I'm twiddling thumbs or bored to death. But unlike some of my previous roles, I wonder if it would matter if I were not there.
Would they **need** to replace me by another individual, or would it be better not to have anyone in my current role.

[^1]: [Rework Podcast Listener Questions Part 2](https://37signals.com/podcast/listener-questions-part-2/)
