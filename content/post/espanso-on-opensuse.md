---
title: "Installing Espanso on openSUSE"
date: 2024-11-01T17:28:54+05:30
tags: ["linux"]
---

After the basic setup was done (I made a list of tools to be installed on line
before I installed Linux itself), I wanted [Espanso](/post/espanso/)

While Espanso is well supported on X11, for wayland, it needs to be [compiled
from the source](https://espanso.org/docs/install/linux/#wayland-compile)

I ran into [this issue](https://github.com/espanso/espanso/issues/995) but [solution](https://github.com/espanso/espanso/issues/995#issuecomment-1048576915) was found in the same issue.

While install went through, I ran into issues related to capability grant.

Following worked:

```shell
sudo zypper install libcap-progs
sudo setcap "cap_dac_override+p" $(which espanso)
espanso start --unmanaged
```

I also needed to add the following to `config/default.yml`
```
keyboard_layout:
  layout: us
```

I finally got Espanso working!
