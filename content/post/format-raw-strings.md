---
title: "Format raw strings in rust"
date: 2024-03-21T23:15:58+05:30
tags: ["rust", "learning", "programming language"]
---

When refactoring code to make API call to Gemini, I learnt how to format
raw strings in rust.


```rust
fn main() {
    let var1 = "test1";
    let formatted = format!(r#"var1 is: {}"#, var1);
    println!("{}", formatted);
}
```
This gives output as : `var1 is: test1`

If you want `{` in the output, then it needs to be escaped with additional `{`
 like:

```rust
fn main() {
    let var1 = "test1";
    let formatted = format!(r#"{{contents: {}}}"#, var1);
    println!("{}", formatted);
}
```

This gives output as : `{contents: test1}`
