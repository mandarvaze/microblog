---
title: "Better Tabs Kitty"
date: 2022-06-10T16:27:53+05:30
useMermaid: false
tags: ["terminal", "tools", "kitty"]
---

Some time ago, I started using kitty terminal emulator.
I used Alacritty for a long time. But Alacritty is meant to be minimalistic in a sense it does not even have concept of having different tabs.

The suggestion/solution is to use `tmux` (or like) to manage different shell instances.

And I did that. 

And learnt a great deal about tmux in the process.

I don't regret it one bit.

But sometimes, tmux gets in a way of how terminal based programs look (and behave) when executed inside tmux.

Sometime, there is a way around by tweaking `tmux.conf`

But I wanted to use real terminal with tabs.

So I tried [kitty](https://sw.kovidgoyal.net/kitty/).

Everything was great, but default tab style was blurry, that I could hardly see two different tabs.

Turns out one can change the `tab_bar_style` - Default one is `fade` (No wonder I would not see text properly)

Currently I have it set to the following: 

```
tab_bar_style powerline
tab_powerline_style round
```

`tab_powerline_style` has few other options. You can read about them [here](https://sw.kovidgoyal.net/kitty/conf/#opt-kitty.tab_powerline_style)