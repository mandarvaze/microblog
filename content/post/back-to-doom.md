---
title: "Back to Doom : For work"
date: 2023-04-13T10:39:39+05:30
tags: ["emacs", "editor"]
---

I updated Minemacs few days ago, and it broke `magit` 😱 [^1]

I had suspected that it is not problem with Minemacs

But I can't have `magit` broken, so on my work machine I switched (back) to Doom
FWIW, the issue is already fixed on Doom [^2]

This time I noticed doom now installs itself under `~/.config/emacs` rather than `~/.emacs.d`
I also updated my `init.el` and `config.el`

Now that I'm back, all my muscle memory started kicking in.
Also `robe` is much better for working in Ruby/Rails - `goto definition` works a lot of times 😄

I never got it to work with `solargraph` 😞

I have still kept `minemacs` on my personal machine (I'm editing this in minemacs)
The upstream issue will get fixed eventually, and updating the package will resolve the issue.

[^1]: [Minemacs Issue](https://github.com/abougouffa/minemacs/issues/49)
[^2]: [Doom emacs issue](https://github.com/doomemacs/doomemacs/issues/7191)
