---
title: "`#[cfg]` attrib vs `cfg!` macro in Rust"
date: 2024-05-19T12:46:46+05:30
tags: ["rust", "learning", "programming language"]
---

When I published my [previous post](/post/debug-assertions-rust/) on mastodon,
[Sebastian](https://mastodon.online/@teotwaki) pointed out [^1] that using `#[cfg]`
is better than `cfg!` macro.

[Documentation](https://doc.rust-lang.org/std/macro.cfg.html) explains that :

> cfg!, unlike #[cfg], does not remove any code and only evaluates to true or false. 
> For example, all blocks in an if/else expression need to be valid 
> when cfg! is used for the condition, regardless of what cfg! is evaluating.

Thanks, Sebastian!

[^1]: See this [thread](https://mastodon.online/@teotwaki/112460605011727547) 
for the original discussion.
