---
title: "Why I switched to Zellij"
date: 2024-02-24T22:33:15+05:30
tags: ["rust", "terminal", "zellij"]
---

I had tried switching to `tmux` for local shell sessions in past, but never
truly understood why I might need it.

I extensively used `tmux` for remote sessions. But why might I need it locally ?

Then slowly I stopped using `tmux` and switched to `wezterm` which provided
multiple tabs.

Fast-forward several years later.

[Recently](/post/oxidise-your-life/) I came across Zellij. I decided to give it a go.

When I had tried `tmux` it took some time to get used to the keybinding.

Not so with Zellij[^1]. The keybindings are always shown just below the window.
So learning them wasn't difficult, plus one can always look it up. It is right there 😊

But that was not enough.

What blew my mind, and made me switch was that

It remembers sessions even after I've exited from the session.🤯

At first, it was confusing, but later I noticed how powerful that is.

It also remembered that I was running `hx` in that session when I exited.

When reattached (we can reattach even to dead session, unlike tmux) it showed `Waiting to run 'hx'` and options like `Press Enter to run 'hx'` and `Esc to drop to shell`

How cool is that. 😎

## Configuring Zellij

Zellij switched to KDL recently (Earlier it used YAML like everyone else)

Creating initial file is easy.

```
mkdir ~/.config/zellij
zellij setup --dump-config > ~/.config/zellij/config.kdl
```

Since KDL is also a text format, updating it isn't a problem.

The auto generated file comes with loads of comments and commented config

One can just uncomment appropriate sections and that is it.

TBH, I didn't need to setup my config till I needed to set theme for Zellij

Define theme colors in the `config.kdl` file like shown [here](https://zellij.dev/documentation/themes#hexadecimal-color-themes)
then specify the theme in the config file as `theme "nord"`

For now, I'm quite happy with Zellij.

Usually, I'll have a few sessions defined one per project.

Any of the sessions can be easily detached, or exited (as well as restarted
 if/when required. So no need to keep them running, unless you really need to 
switch between projects.)

[^1]: Zellij means tiles in some Arabic language
