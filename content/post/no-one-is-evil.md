---
title: "No One is (really) Evil"
date: 2024-11-26T07:41:43+05:30
tags: ["podcast"]
---

[NSQ episode](https://freakonomics.com/podcast/what-is-evil/) about evil led me
to thinking about this.

## Takeaways from the episode
* Evil is attributed only to humans.
  * If an animal attacks and kills human(s) we do not label the animal as Evil.
* Intentionality matters.
  * Accidental killing/death does not mean Evil
* People do bad things because their brain is not "well"
  * Amigdala is the part of the brain that is responsible for fear and morality.
  * Should bad people be locked away longer (cause they may be threat to the society)
*  Phil zimbardo prison experiment
  * Bad barrel (Vs Bad Apple)
  * People become evil due to surrounding [^1]
* Stanley Milgram Experiment
  * People are OK doing bad things if
    * They are in position of power **and**
    * Are told by their superiors to "punish" others
    * 2 out of 3 volunteers shocked the (the actors playing) students upon giving wrong answers, because they were "instructed" to do so [^2]

## My thoughts
* Only movies/TV has pure evil person
  * In good movie/TV, they have some explanation about why the person become "evil"
* When people do bad things to you, do not hate them
* Consider some of the points above.
* What made them behave the way they did
* (Most of the times) it is not personal [^3]

[^1]: One can choose to become Oskar Schindler in Nazy Germany. Do the right thing even if the surroundings are wrong. (But takes courage)
[^2]: No one was actually harmed, but people giving electrocal shocks didn't know. Actors shouted, mentioned heart attack, begged to stop, but "shocks" continued.
[^3] That does not make it better, but helps avoid hatred
