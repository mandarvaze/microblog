---
title: "Magit fixed"
date: 2023-04-15T14:45:02+05:30
tags: ["emacs", "editor"]
---

As I wrote earlier, I was [unable to use magit](/post/back-to-doom/) because of the third party issue.
Doom fixed it within hours (bigger community)

But minmacs also fixed [^1] it (only one person)

I had to follow a slightly complex set of instructions, but I'm glad it all worked out in the end.

I will continue to use Minemacs on my personal machine.
Working in markdown is pleasure in minemacs.
I want to get `robe` working with minemacs, and `codium` and `chatgpt`

`robe` already works with Doom (hence I'll continue using it for work)
`codium` does NOT work with doom (apparently - per Telegram group)

[^1]: [Minemacs fix](https://github.com/abougouffa/minemacs/issues/49)
