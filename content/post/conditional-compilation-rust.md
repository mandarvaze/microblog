---
title: "Conditional Compilation in Rust"
date: 2024-05-16T09:28:40+05:30
tags: ["rust", "learning", "programming language"]
---

Today I learnt that certain rust code can be marked such that it is compiled
only for specific platform.

This makes sense for low level libraries that provide platform-specific functionality
that is not available on other platforms.

This is achieved by tagging the function with `#[cfg(target_os = "xyz")]`

Here `xyz` can be one of the following :

* "windows"
* "macos"
* "ios"
* "linux"
* "android"
* "freebsd"
* "dragonfly"
* "openbsd"
* "netbsd"

Similar to `target_os`, here are other options :

* target_feature
* target_arch
* target_family
* target_env
* target_abi
* target_endian
* target_vendor
* target_pointer_width
* target_has_atomic

~~Some~~ Most of these are obvious. For others which are not, read the :[Rust documentation](https://doc.rust-lang.org/reference/conditional-compilation.html) for the details.
