---
title: "Type accent marks in Wezterm on macOS"
date: 2023-02-15T09:47:48+05:30
tags: ["tips", "macOS", "terminal"]
---

The tips from my [previous post](/post/accent-marks-macos/) didn't quite work
while writing the blog post using `helix` editor.

But as much as I can guess, it is a feature of the terminal and not the editor
itself. So no point searching "Accent marks in Helix" 😄

Turns out initially[^1] wezterm didn't support accent mark without
 special configuration. `('use_ime = true')`, but since then it is enabled by default.

Yet, it didn't work for me. 

Turns out the bug[^2] is specific to left Opt/ALT key 😆

[Solution](https://github.com/wez/wezterm/issues/2920#issuecomment-1371466131) to
get it working with both left and right ALT key is to add the following to `~/.config/wezterm/wezterm.lua`

```lua
	send_composed_key_when_left_alt_is_pressed = true,
	send_composed_key_when_right_alt_is_pressed = true,
```

Tested. Works! 🎉


[^1]: [Dead keys not working on macOS](https://github.com/wez/wezterm/issues/300)
[^2]: [Opt/LeftALT key not useable on OSX](https://github.com/wez/wezterm/issues/2920)
