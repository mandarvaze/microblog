---
title: "Helix New Release (and how to go to the last line in a file)"
date: 2022-09-05T12:34:40+05:30
useMermaid: false
tags: ["helix", "tools"]
---

Noticed that new version `22.08.1` of helix is now available.
See [this](https://github.com/helix-editor/helix/releases/tag/22.08.1)
Upgrade via your preferred package manager.

------

After the upgrade, used `hx` after a long time.

I also signed up for the matrix channel. Turns out that is where all the action is.

I'm member of (almost-dead-but-not-quite) Telegram group for a while. But matrix channel seems active.

I got immediate reply to my query. Nice!

------

My query was : `How do I go to the last line of the buffer ? In vi/m, I could use :$ which does not work in Helix`

TL;DR: `ge`

Long answer : There is a separate [goto-mode](https://docs.helix-editor.com/keymap.html#goto-mode) in helix, and then there are different sub-commands(?) in that mode.
`goto_last_line` is mapped to `e`, hence `ge` (i.e. First switch to `goto-mode` and then `goto_last_line`)

