---
title: "Omakub : Lazyvim"
date: 2024-06-28T13:26:08+05:30
tags: ["terminal", "omakub", "neovim"]
---

As I wrote [earlier](/post/omakub/), I skipped over neovim initially, but then
got curious.

I installed Neovim and configured it to use Lazyvim.

I was blown away by how nice it is.

My last serious affair with neovim was [two years ago](/tags/neovim/). [^1]

Lot has changed since then.

Lazyvim wasn't even born when I stopped using neovim [^2]

It is quite polished.[^3] The hotekys are mnemonic and intuitive (coming from
doom emacs, at least)

I have not used it a lot. 

Since I'm using Helix (and Emacs), I'm unlikely to use it seriously (But never
say "never!" 😄)

But I'm not removing/discarding it yet.

[^1]: Then I realized I'm spending too much time configuring (and fighting
packer and errors from various extensions/plugins) and stopped using neovim
[^2]: I checked. First commit for Lazyvim was on Dec 30, 2022. Few months after
I stopped using neovim
[^3]: At the same time, it seems too much as well. (Initial impression.)
