---
title:      "Microsoft's Confusing (or Incorrect?) Documentation"
date:       2023-06-13T07:21:09+05:30
tags:       ["Microsoft", "docs", "azure"]
---

At work, I've been using REST API to connect to Azure Service Bus because
officially Microsoft does not support Ruby SDK (It is retired since 2015)

The documentation related to accessing Azure Service Bus via REST API is very 
limited, and at time difficult to use.
 
e.g. [This](https://learn.microsoft.com/en-us/rest/api/servicebus/unlock-message#request) documentation
about `How to unlock a message in the (Azure Service Bus) queue` mentions this URL pattern to be used
for the REST API call : 

> `http{s}://{serviceNamespace}.servicebus.windows.net/{queuePath}/messages/{messageId&#124;sequenceNumber}/{lockToken}`

I was not sure what is `&#124` is. It turns out to be `|` (Vertical bar) sometimes used to indicate `or`

So it may mean use either `messageId` or `sequenceNumber`. But they could have just explicitly mentioned so.

The example is as follows :

> `PUT https://your-namespace.servicebus.windows.net/HttpClientSampleQueue/messages/31907572-1647-43c3-8741-631acd554d6f/7da9cfd5-40d5-4bb1-8d64-ec5a52e1c547?timeout=60 HTTP/1.1  
`

So it is unclear that they are using **only** `messageId` (`sequenceNumber` is an Integer that looks like 1,2,3... etc.)

Also, `timeout` is not documented (except in the example)

Another variation that uses topics and subscription, (and example) mentioned `|HTTP/1.1` , but I'm not sure what that is. 
Also, if they can use `|` here, why not earlier ?

I had a lot of heartache chasing this.

Finally, using only `messageId` worked.

But I'm still unsure then why is there `sequenceNumber`
