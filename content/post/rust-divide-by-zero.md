---
title: "rust can Divide by Zero"
date: 2024-05-02T18:36:45+05:30
tags: ["rust", "learning", "programming language"]
---

At least for floating point numbers, it does not crash/`panic!` 🤷‍♂

```rust
fn main() {
    let x = 10.0;
    let y = 0.0;
    println!("{:?}", x/y);
}
```

This above code returns `inf`

But if we change the number to `int`, compiler catches this [^1] at shows the following error:

`error: this operation will panic at runtime`


[^1]: This is a contrived example. Instead of static values, if these were passed at runtime, it would (should?) `panic` 
