---
title: "How to run multiple postgres docker instances on local machine"
date: 2024-11-25T22:28:59+05:30
tags: ["docker", "postgres", "devops"]
---

When working on multiple projects, I run separate postgres instance for
each of them.

Since these are docker instances, this is not a problem as long as they are not
running **at the same time**

If I'm actively working working on multiple projects parallelly, then there are
two problems :

1. Resources : My laptop becomes slow. There is no solution for this [^1]
2. Assuming you have LOTs of memory, second problem still remains. Both
instances try to bind to local port `5432` the default port. Which does not
work. So you need to bind other postgres instances to different local ports like
`5433`, `5434` and so on. This is controlled via `-p` in the docker command.

The format is :

`-p <host_port>:<container_port>` (Easier to remember that way)

So you'll have docker commands like `-p 5433:5432` and `-p 5434:5432` etc.

and local ports 5432, 5433 and 5434 will all be listening on different postgres
servers.

Off course, if you do not have local `psql` and instead use `docker exec` (like
I do) you have no problem.

On the other hand, if you use local `psql` you'll need to specify `p 5433` and
`-p 5434` to connect different postgres instances. (Probably along with `-h
127.0.0,1`)


[^1]: Other than adding more RAM, but till Apple allows adding more memory after-market, paying more money upfront is the only choice. Which I had not done last time (About 9 years ago)
