---
title: "Espanso With Helix Editor"
date: 2022-05-31T06:30:11+05:30
useMermaid: false
tags: ["helix", "tools", "productivity", "espanso"]
---
I wrote about espanso in past [here](/post/espanso/) and [here](post/espanso-2/), and I've been using it ever since.

Since helix is (for now) terminal-only editor, my espanso shortcuts work very well with it. 

e.g. I can type `:smile:` and get 😄, and type `thru` and get `through` (Automatic correct the usually misspelled words 🎉)

-----

Espanso works well with neovim-in-terminal (and Emacs), but I use Neovide GUI, where they don't work.
