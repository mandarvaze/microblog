---
title: "Bruno: Local-first Postman Alternative"
date: 2023-10-12T23:50:07+05:30
tags: ["api", "tools"]
externalUrl: https://www.usebruno.com/
---

I think I came across this tool via Hacker News 🤔. But may be not.
Anyway, it does not matter.

Why it appealed to me ?

At work, we have a shared Postman workspace, which often breaks.

Because anyone and everyone can modify it (which is some times needed)
But then people click the `Save` button, because it is easy to 😄

and now, it is broken for others.

OK, there is a concept of `fork` in Postman, but then everyone has their own fork.

It is same as `git` fork. One has to keep the fork up to date.

Here comes bruno.

First thing I liked about it is that we can import from Postman. So that one does not
need to create entire collection from scratch.

Once imported, it becomes a script/text, which can be version controlled

Next thing is secrets.

I have not yet figured out where and how they are stored. 
But secrets is one of the reasons, I was attracted to `bruno`

With local first, developers are free to use their own passwords on their
machines.

I was able to get APIs working from Postman to bruno without much friction.

I did have to create environments from scratch. Not sure why they were not exported by Postman
Maybe because they contain secrets ? 🤔

But once I added all the variables to `Local` environment, I can just copy-and-modify
to create other environments.

At work we have a LOT of environments (At least 10. Yeah, I know)

I'll be exploring `bruno`'s capacities in coming days. Like `Bruno CLI`, tests etc.