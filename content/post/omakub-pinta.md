---
title: "Omakub : Pinta"
date: 2024-06-28T12:04:32+05:30
tags: ["omakub"]
---

While Omakub was mainly intended for developers (and thus has focus on terminal
based programs like alacritty, zellij and neovim), it does come with few GUI
programs.

I think this is mainly because DHH was trying to switch to Linux as his primary
machine, and requires some non-terminal tools.

Choice of Pinta and Xournal app were interesting, so I installed both of them.

I assumed Pinta to be MS Paint replacement.

While it does have tools like brush and pencil, it seems it was more of photo
editing tool [^1] (that also allows tiny bit of drawing and paining capabilities.)

e.g. When Pencil tool is selected, there are no options like changing the
thickness of strokes. (Paint brush has those options)

Also, when using macOS, the app looks ugly. (I understand that this is not a
native macOS app) There are several better drawing apps on mac.

For those reasons, while I gave it a try, I uninstalled it.

[^1]: I had fun playing with the Effects menu though 😄
