---
title: "Deployed Microblog"
date: 2021-12-17T11:43:17+05:30
tags: ["deployed a site", "meta"]
---

Finally, deployed **this** site.

It took a while because, at first I was [modifying](https://microblog.desipenguin.com/post/theme-hacking/) the theme to my liking.

As most theme README state, I started with `git submodule`, but it gets confusing. That is when I came across `hugo modules` in [this](https://chrisshort.net/from-git-submodule-to-hugo-modules-using-netlify/) post.

But it did not work for me for multiple reasons.

First, I did not understand the difference between specifying theme from repo Vs local, and `hugo mod init`

The docs keep referring to *my repo*, which means the repo for **this** site, but I confused it with theme repo.

It some trial and errors. But finally it was really easy. [These](https://github.com/theNewDynamic/gohugo-theme-ananke#installation) instructions were all I needed.

Deploying to netlify was no-op. I have about 5 sites on netlify. 😄

At first the CSS et al did not work. That is because the `baseURL` was example.com 😆
Changing that, setting custom domain on netlify (and https certificate) was all it took to get the site up it all its glory.

I do want to tweak the theme more.

I also want to post more.
