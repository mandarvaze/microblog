---
title: "Considering migrating PKM Site"
date: 2023-06-16T11:14:04+05:30
tags: ["misc"]
---

My [PKM](https://pkm.desipenguin.com) is currently based on [Dendron](https://wiki.dendron.so/)

Main appeal of Dendron was local-first. At the same time, it indeed was loosely tied to VSCode editor.

Over the years, they had a command line tool to create new notes. 
Still, its strength always was as a VSCode plugin

Then today, I came across [Obsidian-Zola](https://github.com/ppeetteerrs/obsidian-zola)

It splits the task of taking notes and publishing it.

* Obsidian for note taking
* Zola for publishing

What I liked about it is `Search` functionality (To be fair, my current PKM build on Dendron also has it)

I had heard of Obsidian, but never committed to it because publishing is a paid option (Also not local-only option any more, I think)
I may not even use Obsidian app to note taking (Or maybe I will. too soon to comment)

### Dendron
* ❌ (Mostly) VSCode extension
* ❌ Uses UUID in the URL when published. This makes it harder 
* ❌ No good theme support
* ✅ Hierarchical notes taking, and publishing
  * A note with filename as `dev.db.pg.psql` will be displayed like a tree, just based on the file name alone.
  * I miss this in Obsidian+Zola

### Zola
* ✅ Obsidian-Zola supports both Light and Dark themes (which Dendron does not)
* ✅ Zola itself has decent themes support (which I don't need right now, but could be useful in the future)


For now, I'm migrating my notes from Dendron. What this really means is: 
* Remove the frontmatter - which is Dendron specific
* Delete Empty files. These are required for the hierarchical note taking/publishing.
* Rename Notes, so that titles show up as I want, rather than auto-camelcased-from-the-filename 😄
  * So far I don't know how to add frontmatter, so that filenames are separate from the title

I also ran into problem when running this locally on macOS
`obsidian-export` binary shipped in linux specific. I replaced that with [this](https://github.com/zoni/obsidian-export)
