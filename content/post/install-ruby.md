---
title: "Installing Ruby"
date: 2022-01-13T09:31:55+05:30
tags: ["ruby", "learning", "programming language"]
useMermaid: false
---

There are multiple ways to install ruby on your machine.

Official recommendation is to use your OS's package manager like `apt`, `pacman` or `brew`

If you are using a version manager, `rvm` (Ruby Version Manager) seems to be popular.
But in the past, I started using `asdf`, so I've decided to stick with it.
It is also mentioned (first in the list) on the official [Installing Ruby](https://www.ruby-lang.org/en/documentation/installation/#managers) page 😄

```shell
asdf plugin add ruby https://github.com/asdf-vm/asdf-ruby.git
asdf list all ruby
asdf install ruby latest
```
*At the time of this writing `3.1.0` is the latest*

It took little over 13 minutes. 😱 I think it could be because this was the first time I installed any Ruby version.

I would like to believe that subsequent installation will be faster. 🤞

-----

Before I could use `irb` (which is what everyone suggest we do, when learning Ruby. It is REPL for ruby) I had to `asdf global ruby 3.1.0` [^1]

Without it, I kept getting the following error: 

```shell
$ irb
No version is set for command irb
Consider adding one of the following versions in your config file at /Users/mandar/.tool-versions
ruby 3.1.0
```

Once done, `irb` worked. We are off to the races 😄

----
[^1]: *I could have also used `asdf local` instead if I wanted different ruby version for different projects*
