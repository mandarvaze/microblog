---
title: "Nu Shell"
date: 2024-02-04T23:23:21+05:30
tags: ["terminal", "rust"]
---

[Earlier](/post/oxidise-your-life/) I wrote about various utilities written
in `rust`. `Nu shell` is one of the most important of them (It is an entire shell
after all, not just single utility)

## Installing

Turns out I had installed nu-shell earlier, but via [`macports`](/post/trying-macports/)

and I had forgotten about macports (and nu shell)

## Mysterious upgrade failure (Or so I thought)

When I installed nu-shell via `brew` I got the latest version, but `nu` kept invoking older version.

Little digging revealed, it was being invoked from `/opt/local/bin`, which is
not normal location for brew (on Intel macs)

I tried unlinking and re-linking from `brew`, but did not help.

## Mystery solved

That is when I somehow remembered that I may have installed it via port.

Removing port itself took a long time.

`port` kept telling me to `selfupdate` which kept failing due to permissions issue

finally I decided to remove everything including macports itself.

[These](https://guide.macports.org/chunked/installing.macports.uninstalling.html#installing.macports.uninstalling.users) instructions helped.

I still am unable to remove `/opt/local/var/*` - I keep getting `Permission denied` (I've given up, for now. May be next reboot will solve the problem)

## Starship

So finally I got latest nu shell working. Turns out the prompt is ugly.
But who customizes prompts these days when they can use starship (which works
across shells - so same look and feel is available across shells)

The instructions were straight forward, but I kept getting error around `let-env` in the `init.nu` file.

Turns out [I'm not the only one](https://github.com/starship/starship/issues/5662)

To be fair, once you have a stable config, that is tweaked to your liking one does not 
keep updating the tools (At least, I don't. That is why my emacs/doom is not updated
for 6 months at a time. Same was the case with starship)

Anyway, problem identified is problem solved 😉

So now I have good looking prompt. I can start using nu shell.
