---
title: "Uninstalled Instagram"
date: 2023-10-02T21:43:12+05:30
tags: ["misc"]
---
Few hours ago, I uninstalled Instagram from my phone.[^1]

Ironically, earlier today I watched a reel on Instagram saying "Instagram is like Chakravyuh, and we are like Abhimanyu. We can go inside, but not come out".[^2]

I think **that** made to take this action.

I noticed that I had not written much in the entire month of August 2023, and hardly 2 entries in September. While some of it attributes to work pressure, my instagram addiction may be equally responsible.

As a lot of us know, we start looking at Reels, "just for 10 minutes" and when we look at the watch, about an hour has passed. I had heard of doom scrolling, but did not know it first hand till Instagram.

At times, I could observe (as if I am an observer, watching myself) that I "decide" to stop, and yet after 10 minutes, I'm still scrolling.

No more. 🤞

[^1]: When I mentioned this to my family, my 10 year old was worried. "What will happen to the pictures you uploaded ?" I told him that I merely uninstalled the app, not remove my account. So I can access my account whenever I want.
[^2]: According to Mahabharata,Abhimanyu learned to break into chakravyuha from Krishna while still inside his mother's womb. However, he was unable to hear how to exit the chakravyuha, and this incomplete knowledge later contributed to his demise.
