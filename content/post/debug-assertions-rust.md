---
title: "Rust : Use println! only in Debug Build"
date: 2024-05-17T22:08:49+05:30
tags: ["rust", "learning", "programming language"]
---

We all know we shouldn't use print debugging, and yet we all do 😉 [^1]

Jokes apart, when I'm still developing the code, I use the debugger where possible.
But sometimes, I want to keep certain print statements to verify runtime 
behaviour, especially when rolling out new feature and when there 
are too many variations (some of them unknown) in incoming data.

I'm aware, logging is the right way to handle this (with loglevel set to
 `debug` or something), but it seems too much when developing toy projects.

I use logging in my project that needs to be "deployed"

Anyway, today I decided to learn if and how this could be done in rust.

Luckily, instead of spending hours, I asked Codeium coding assistant about this.
It suggested that I wrap the print statements with `debug assertion`

```rust
if cfg!(debug_assertions) {
  println!("Running debug build);
}
```

[^1]: Irrespective of the programming language. So `console.log` for JS and `println!` in rust are in the same category
