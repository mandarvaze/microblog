---
title: "Logseq"
date: 2021-12-18T22:03:10+05:30
tags: ["exploring", "note-taking", "pkm", "tools"]
---

I came across [logseq](https://github.com/logseq/logseq) long ago, of all the places on Dendron's discord channel.
It has other-projects channel just for this purpose.

Logseq was still early back then (compared to Dendron at least). 

I tried, but did not like it. Plus Dendron was much better.

Today, I came across Logseq again, when I was perusing my colleague's twitter feed and noticed that he is using it.
So I checked it again. They now have a Desktop app, plugins and decent community.

So I installed the Desktop app. 

What I did not like back then was the dark green color theme. It has not changed. But there are theme support (created by the community)
So I might try one that I find better.

They also have light theme, which is white. Better than dark green. For now, that is what I am using.

I plan to use it for their journal feature. If I like that, I might continue it that way.

I do not plan to use it for knowledge management. At least not now anyway.

Interestingly, I came across a YouTube video where the person said: 

> I'm going to use Logseq for Technical notes, and keep other personal stuff in obsidian 

Another (potential) benefit of Logseq is that is supports Org-mode, and there is a plugin for logseq from within Emacs. (It is too early for me understand that what means 😄)

---

If you have not yet visited my [PKM](https://pkm.desipenguin.com) powered by Dendron, please do.

