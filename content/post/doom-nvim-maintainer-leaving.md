---
title: "Doom Nvim Maintainer Leaving (?)"
date: 2022-02-15T10:41:27+05:30
useMermaid: false
tags: ["vim", "tools", "neovim"]
---

Just I was getting excited about Doom NVim, today I read (on their Discord server) that Alejandro aka [NTBBloodbath on github](https://github.com/NTBBloodbath) has lost interest in Doom Nvim

To quote him :

> I'll slowly try to finally make Emacs my primary code editor (always failed in each attempt haha), Neovim's ecosystem isn't going in the direction I would like and I think it never will be so I'm very frustrated and sad with it.

> Maintenance will still be "active" but a bit more slow from my side ............I'll accept PRs, work on issues and everything will be as usual but a bit more slow as I told before.

This is sad 😞

I also keep getting a lot of errors from TreeParser (in what I would call minibuffer in Emacs terms) This is distracting.

I tried (maybe not enough) to set projectile equivalent (telescope project plugin) and ltex-ls, but no success.
