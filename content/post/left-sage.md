---
title: "Left Sage"
date: 2024-03-05T23:15:42+05:30
tags: ["career change", "personal"]
---

Today was my last day at [Sage](https://www.sage.com) (I joined Lockstep, which was acquired by Sage)
I spent a little over two years working with the wonderful engineering team there.
I learnt a lot from everyone on the team, and made some new friends (I knew *one* person before I joined) during my stay.

Most of the team was junior to me (in terms of number of years of experience.)
But as I worked with them, I noticed that every single one of them were exceptional engineers. Without exception.[^1]

When I joined, I did not know Ruby/Rails.

I'm sad to say that even after 2 years, I can't say I "befriended" RoR (like I did python) 😄

I also got opportunity to work on C# - but not enough time to feel comfortable.[^2]

-----

## Contributions

* Generic Webhook Framework, such that adding a new webhook was a matter of adding an entry in the lookup table and writing a Sidekiq Job
* Resilient Webhook Framework, using Azure FunctionApp, Service Bus queue, Trigger Functions - Oh my!
* I did some feature development as well, but that is specific to the product and may not be of the interest to those outside the organization.

## Praise

* Co-founder of the original company said "Mandar, you write very good code"
  * I interepretted it as "readable" code with code comments explaining "why"
    * There was culture of "no code comments" and PR comments of the nature "Please add code comments" were ignored ¯\_(ツ)_/¯
    * So code that could be understood on first read may have been "refreshing"
  * Other possibility is that usually people with many years of experience, do not write code at all, so my code may have been unexpected (hence the praise)
* QA team members praised me about me taking time to explain the feature to them.
  * Unfortunately, QA teams are considered second class citizens (I doubt this is unique here 😞)
  * Despite QA teams raising this issue several times, it hasn't changed much.
* People from other sqauds (teams working on same product but different focus, under different manager) thanked me for taking time to help them.

## Reflection

Although it is not a revelation (to me) - I am not happy in MNC. There just seems to be a lot of overhead, red tape and lot of wastage of energy.
When I joined Lockstep, it was a small organization which was growing. I think people were happy (I hope some still are) 

I thrive in smaller teams with autonomy and shared goal.

I understand that no organization *plans* to be all these "less than optimal" things (I'm really struggling to find positive words 😄) but
as the organization grows, there is bound be some inefficiencies. Teams must be broken into smaller units, adding levels of management 
and additional communications. Some of these could be addressed by willing managers within their "sphere of influence"[^3] but most things 
aren't within their control.

[^1]: Even few people who were "let go" due to performance reasons were real smart people with good engineering skills.
I think if it was not fully remote setup, some of them could have done better. Sometimes juniors need a 
little hand-holding (not spoon feeding) and they can shine. Alas! That did not happen.

[^2]: After so many years, I feel confident that I can get a "working" code in most of the languages, even the ones I don't know before starting the work, within couple of weeks.
But I would rather learn the new language till I feel comfortable, and not be bogged down by the deadlines to deliver working code.

[^3]: One manager from other squad replaced the daily stand up with written updates. He shows up for the daily stand up, and anyone who is "stuck" can join and discuss/seek help. 👏
