---
title: "Better Icon for Kitty"
date: 2022-06-09T20:04:30+05:30
useMermaid: false
tags: ["terminal", "tools", "kitty"]
---

Official instructions are [here](https://sw.kovidgoyal.net/kitty/faq/#i-do-not-like-the-kitty-icon) but it did not work for me.

I did not understand `Drag kitty.icns onto the application icon in the kitty info pane` part.
I tried dragging the icon, but could not make it work.

I gave up. I can live with the default icon.

But then I came across the following instructions [here](https://github.com/megalithic/dotfiles/blob/main/config/kitty/update_icon.sh#L36).

Clear instructions that I can execute from inside kitty 😄

```shell
cp /tmp/kitty.icns /Applications/kitty.app/Contents/Resources/kitty.icns
rm /var/folders/*/*/*/com.apple.dock.iconcache
touch /Applications/kitty.app
killall Dock && killall Finder 
```

FWIW, I am using [this icon](https://github.com/hristost/kitty-alternative-icon)