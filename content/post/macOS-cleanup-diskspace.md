---
title: "MacOS : How to Clean up the disk"
date: 2024-10-15T22:48:18+05:30
tags: ["macOS"]
---

In my plan to shift to Linux, I considered dual booting.

Turns out I may not have enough disk space. 

I have reached at about ~~12~~ 15GB free space. At first I thought, if live session can fit into 4GB, then ~~12~~ 15GB should be enough, but looks like that may not be the case. [^1]

So I'm shelving the plan to dual boot.

I'll go with full disk install. That way linux gets entire 128GB SSD 👍

When I started, I had only 5GB space.

So I started finding solutions

[This answer](https://superuser.com/a/851256) from 10 years ago is still valid.

Clear the caches.

I did not realize how much space it takes.

There are caches for the apps that I tried and eventually uninstalled.

The space used by `.app` was freed (hopefully 😆) but the cache remained.

`~/Library/Caches/Homebrew` took the most space.

`brew autoremove` and `brew cleanup --prune=30` helped some.

Then there was entire Electron framework inside `Ollama.app` (Under `~/Library/Caches` not actual `.app`)

Other command that helped : `sudo du -hd 1`  [^2]

Rather than running it at `/` , I ran it manually inside specific folders like `~/Library/Caches` and `~/Downloads` etc.

In all, I learnt a lot and reclaimed about 10GB of disk space.

Not bad for an hour spent searching plus cleaning up.


[^1]: [This discussion](https://www.kubuntuforums.net/forum/newbie-support/help-the-new-guy/674357-system-requirements) says : Minimum 25GB of disk space.
[^2]: Later I learnt about easier way to do this without using the terminal. Apple menu -> About This Mac -> Storage -> Manage. (This differs from macOS versions, but you'll be able to figure it out)
