---
title:      "Trying Denote for Note Taking"
date:       2023-05-04T07:01:29+05:30
tags:       ["emacs"]
---

I've been using `org-roam` for some time for my note taking.
One of the practical problem is I'm used to Markdown so much, that switching
to `org` syntax does not come naturally to me.

The other day, I had to look the syntax for `begin_src` - which is just 
3 ticks in markdown.

So I checked to see if there are note taking packages in emacs that support 
taking notes in markdown, and thus I came across `denote`

To my surprise, the frontmatter used by `markdown-yaml` filetype in denote,
is very close to the frontmatter used by `hugo` SSG. (Denote has an extra
`identifier` which is not there in hugo.)

---

So here is what I did after above was written as a "note" in `Denote`
I copied the file which has following components

* timestamp
* title
* tags

So original filename looked like `20230504T070129--trying-denote__emacs.md`

I copied the file to the `content/post/` folder and renamed it to retain
just the title.

When previewed with local hugo server, it worked flawlessly 🎉

I removed the `identifier` attribute (even though it worked with it, doesn't
mean I should keep it)

I plan to add a tag `blog` in denote - which I'll remove when I move the file.

I have not explored denote much (at all?) but the first impression is good.



