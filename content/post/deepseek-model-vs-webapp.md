---
title: "Deepseek : Model vs Web app"
date: 2025-01-29T10:54:22+05:30
tags: ["llm", "ai"]
---

There was a post on LinkedIn about how asking DeepSeek to list states of India
is not answered. [^1]

First of all, why would you ask such a trivial question to it , when you can
just search.

We need to consider the difference between search engine and "thinking model"
like DeepSeek

It is like asking Einstein help with 10th class Physics exam 😃 - while he may
be able to help, his abilities are better suited for more difficult tasks.

<!--more-->

Now that it is out of the way, I also tested the same (after seeing the LinkedIn
post) What I noticed is :

**It starts answering the question, and then it stonewalls us.**

Since the responses are streamed, it is easy to see the first part of the
response, before it is quickly replaced.

At first, I saw the answer that started with "My knowledge cutoff date
is October 2023 ..." and then the text was replaced with "Sorry, that's beyond
my current scope. Let’s talk about something else."

I asked why it is out of scope, it said :

> I can provide information and discuss topics related to any country in the
> world, as long as the conversation adheres to ethical guidelines, avoids harmful
> or inappropriate content, and respects cultural sensitivities. If you have
> questions about a specific country or topic, feel free to ask, and I'll do my
> best to assist!

I said :

> Listing the states in India isn't unethical, harmful or inappropriate

I **definitely** saw answer starting with

"You are right...." and then it was replaced with

> Sorry, that's beyond my current scope. Let’s talk about something else.

Here is what I think is happening:

The "model" understands that "Listing states of India" is not harmful/unethical
etc. So it starts responding accordingly.

Then the "frontend webapp" built by DeepSeek the company - intercepts and
removes what it considers "risky"

The "model" is just software - it is not impacted by human's perception (Even if
it is trained with guard rails in place) But DeepSeek - the Company - needs to
worry about public perception.

My idea can be verified (by someone else, I do not have capable enough hardware)
by running DeepSeek model locally, where the "web app" won't intercept the
responses.

[^1]: Unfortunately, I lost the post i the feed, else I would have shared the link.
