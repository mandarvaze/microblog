---
title: "Markdown vs Orgmode"
date: 2023-07-16T09:01:58+05:30
tags: ["opinion"]
---

My thoughts in the context of [this](https://mastodon.social/@danyork/110694878213460306) thread.

I did try org for some time (initially when I switched to Emacs) as mentioned [here](https://pkm.desipenguin.com/about/)

But eventually I realized that markdown is more pervasive. 

I am aware that Org is supported outside of Emacs as well.

But compared to markdown, that is nothing.

But that is not necessarily the point.

e.g. When I do code review, most of the git service providers support markdown.
I was surpised that even Azure git supports Markdown (So does gitlab, 
probably bitbucket as well, it as been a while since I used bitbucket.)

I am aware github *renders* org formatted files, but I am not sure if supports
org syntax in code review comments.

As the original thread started, obsidian natively supports markdown (org mode
supported via third party plugin)

Then there is hugo, blog engine that powers all my blogs. Yes, I'm aware hugo
supports org-mode, but markdown is native.

Then there is aptly named [Markdown Here](https://markdown-here.com/) - allows 
you to use markdown syntax in most of the browsers. I use this wit gmail
For the most part, my emails do not need any markup, but once in a while, I may
want to emphasis certain word/phrase. Markdown to the rescue.

Others tools I use that support Markdown : Telegram, Elk (Mastodon client)

Coming to Emacs ecosystem, I currently use `denote` for my notes. It supports
markdown (I think org is  default option - understandably, but it also natively
supports markdown) This is very useful.
Lot of times, I can just remove `identifier` from the frontmatter (rename the
file if required) and publish it as-is on my blog.

------

Argument against markdown is that it is not a "standard", but the way I use it
I really just need syntax for italics, bold, headers and bullets. I think
that is the lowest common denominator. Works the same everywhere.

I don't use GFM (Github flavoured markdown) or any other extensions.
So I'm safe (I think)