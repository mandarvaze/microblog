---
title: "Mystery of Un-upgradable KeepassXC"
date: 2022-05-02T07:26:56+05:30
useMermaid: false
---

For some time now, my KeepassXC browser extension keep informing that I'm using older version of KeepassXC
I usually upgrade immediately, so I did. Or so I thought.

I saw the same warning couple of days later. I thought there was new version, but there wasn't. So installed it again.

This happened one more time, and I realized that even after upgrade, KeepassXC remained at older version (2.66, but as you'll see it doesn't matter)

In order to be sure, I first uninstalled existing KeepassXC rather than installing on top of existing version.

That didn't help.

I searched. Didn't help.

I looked through their issue list on github. No luck.

Then I thought maybe uninstall isn't really removing the app. (But how can it be 🤔)
But apparently that is what it was. (But not exactly)

So I wanted to find out where was the one that kept running. I had explicitly moved the one from `/Applications` to trash.

On the terminal I would have used `which` command. But how does one find out the same for GUI app ? That too on macOS, because `.app` is usually a folder. The real binary is inside `/Content/..` path.

Anyway, turns out there is no technial problem that has not been asked (and resolved) on stackoverflow.

Here is the [proof](https://superuser.com/questions/1558195/how-can-i-find-a-path-to-a-running-application-on-mac-os-x)

Long story short, there was another version of `KeepassXC.app` under `Utilities` folder. 

Once I moved that to trash, all was OK.
