---
title: Integrated terminal in Emacs
date: 2021-12-07T14:02:12+05:30
tags: ["published a blog post", "emacs"]
---
I spend most of my time in Emacs and Terminal. I make code changes in emacs and switch to terminal to run the tests. 
It would be nice to do that without switching between the two. 

In the past, I had tried `shell-mode` and `inferior shell` (python) for this, but it didn't work, till I found `vterm`
<!--more-->
[How to use Emacs as an IDE with Integrated Terminal](https://learnings.desipenguin.com/post/emacs-vterm/) (Instructions for Doom Emacs, but should work for other configurations as well)

