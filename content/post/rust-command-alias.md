---
title: "Adding alias to CLI built in #rust"
date: 2025-02-28T17:57:20+05:30
tags: ["rust"]
---

Turns out it is very easy if using `clap`

<!--more-->

I wanted to add command alias to a tool I'm building [^1]

I didn't know how to do it.

I wasn't actively working on it either, just one of the items on the TODO list.

Then I was looking at the source code of some completely unrelated to my
project, and I found the answer.

`#[clap(name = "mycommand", visible_alias = "alias1", visible_alias = "a1")]`

There can be multiple alias.

Usually one is shorter version of another, but it does not have to be that way.

I was trying to point to exact documentation for `clap` but I could not.

Closest I found is [this](https://docs.rs/clap/latest/clap/struct.Command.html#method.visible_alias)

[^1]: will be announced here soon. I'm finishing up minor tasks before publishing to crates.io
