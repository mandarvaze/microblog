---
title: "Now individual posts too show title, date and tags"
date: 2023-01-04T09:49:18+05:30
tags: ["hacking", "meta"]
---

After I switched the theme for this blog, for the longest time I had not
noticed that when reader is viewing specific post, they do not see the
metadata like title, date etc.

When I noticed, I tried some time debugging, but couldn't figure out why this
was happening. So like a good developer that I am, I created 
a [github issue](https://github.com/mandarvaze/hugo-microblog/issues/2) so that
I can fix it later (and not forget it)

Yesterday, I chose to fix it, and after doing some RCA, I (think) I found the
[reason](https://github.com/mandarvaze/hugo-microblog/issues/2#issue-1478322422)
why it was not working.

I still didn't know how to fix it, and it was late.

This morning, I searched based on my understanding, and within few minutes
I was able to fix it.

🎉