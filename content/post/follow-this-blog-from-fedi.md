---
title: "You can now follow this blog from Fediverse"
date: 2024-05-13T22:24:05+05:30
tags: ["meta"]
---

Put `@microblog.desipenguin.com@web.brid.gy` in the search box.

Thanks to [Bridgy.fed](https://fed.brid.gy) this site automatically gets it's own mastodon account.

Following this account is like subscribing to the RSS feed (I think 😄)
