---
title: "How to Support Both Light and Dark Mode for your app"
date: 2022-12-02T19:15:51+05:30
tags: ["webdev"]
---

Add something like the following to your css

```css
@media (prefers-color-scheme: light) {
	body {
		background-color: #f0f0f0;
	}
}

@media (prefers-color-scheme: dark) {
	body {
		background-color: #0f0f0f;
	}
}
```

I'm just learning this. But this above doesn't look scalable.
My previous theme, defined colors as variables, per color-scheme
and in the other css, use these variables instead of actual colors.

That way, you need to modify colors only at one place 🎉

See [this](https://github.com/dianoetic/indiefeed/blob/master/static/css/vars.css)
