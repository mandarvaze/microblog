---
title: "Using Projects With LunarVim"
date: 2022-05-03T21:47:36+05:30
useMermaid: false
tags: ["exploring", "vim", "tools", "neovim"]
---

Today I spent some time getting `projectile` equivalent working with LunarVim

At first I went with builtin/core plugin `projects.nvim`. Enabling it was quite straight forward.
I wasn't sure how to add new project.

Then I tried `telescope-projects`. Even here I could not add new project (Per their documentation, it is `Ctrl-a` in Insert mode, and just `c` in normal mode) But I just opened the project root I wanted to work on, and it got added to the list of projects.

Now that project was enabled, everything worked in project context, like finding file, finding text.

This is what I wanted.

I came here looking for `fzf-projectile` equivalent from Emacs, but `fzf` is already integrated here. So default `Find files` means `Find files in project using fzf`   

------

Next stop : Running all git commands from inside nvim (Just like I do in Emacs)
