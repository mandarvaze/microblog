---
title: "NANO Minemacs"
date: 2023-03-09T09:09:49+05:30
tags: ["emacs", "tools"]
---

In my [earlier post](/post/nano-emacs/) I had mentioned that I might try
combining NANO emacs features with Minemacs - which is my current Emacs setup.

NANO emacs README has instructions for
 [manual install](https://github.com/rougier/nano-emacs#manual-installation) 
which is what I ended up doing.

There is a [separate repo](https://github.com/rougier/nano-theme) just for the 
NANO theme, but somehow it did not work for me.

Instead, cloning the NANO emacs repo, and adding "just enough"™️ configuration
to Minemacs worked for me.

Removing tool-bar, and setting the Dark theme directly took a bit of tinkering,
but finally I got it working.

I noticed that because NANO emacs is cloned, and not installed using 
package manager, it takes a little longer for the NANO parts to kick in.

Minemacs complies all the packages hence they are quick to load.

I may need to look into how to do that. 

AFAIK, straight allows to install from git repo, so that might work 🤷‍♂
