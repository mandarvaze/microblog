---
title: "Improve Markdown output: Use HTML snippets in Markdown"
date: 2022-08-25T13:15:37+05:30
useMermaid: false
tags: ["tips", "writing"]
---

While I was searching for useful snippets for Markdown (I was specifically looking for snippet to insert current time.)

I came across [this](https://jdhao.github.io/2020/06/01/markdown_writing_tricks/) article.

To be honest, it feels like cheating. Like writing HTML manually. (With snippets, work could reduce)

<div style="padding: 15px; border: 1px solid transparent; border-color: transparent; margin-bottom: 20px; border-radius: 4px; color: #31708f; background-color: #d9edf7; border-color: #bce8f1;margin-top: 10px;">
<strong>&#9432; Info:<br/></strong> I don't see myself using it often.
</div>

Did you see what I did there 😉

