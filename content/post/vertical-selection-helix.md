---
title: "Helix: Vertical Selection and Edit"
date: 2024-07-04T06:09:48+05:30
tags: ["helix"]
---

One of the workflows I used in vi/m over last 2 decades is : vertical selection
and edit

So much so that it is part of my muscle memory and I needed to learn the
equivalent in helix.

After some searching, I found it. Here are the steps:

* Go to the column you want to select. Press <kbd>v</kbd> to enter select mode.
* Select the column and to go down and up in the column selection use
<kbd>Shift-C</kbd> or <kbd>Alt-Shift-C</kbd>
* You can repeat the <kbd>Shift-c</kbd> command using the numeric operator if
needed, like `10-Shift-C` to select the column 10 rows vertically.
* Now you can perform any action on the selection e.g.
  * Go to `insert` mode and add `* ` before each column.
  * Or `d` to delete the selected letter from all the rows (in `normal`) mode.
* You can come out of this (multi-select?) mode via `,` in `normal` mode (If
you are in `insert` mode, finish editing and come back to `normal` mode first,
via `Esc`)

[Source](https://www.reddit.com/r/HelixEditor/comments/11l1d6e/comment/jba764x/)
