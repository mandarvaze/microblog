---
title: "How to get 13 Digit Epoch time in Shell (Ruby Version)"
date: 2022-12-20T10:21:59+05:30
tags: ["hacks", "pkm", "productivity"] 
---
I needed this, because
[Dendron](https://pkm.desipenguin.com/notes/d7c0c7c4-be4e-48f5-bc96-fb387704596e/#manually-updating-created-and-updated-front-matter)
uses 13-digit epoch time, for `created` and `updated` timestamps.
While it is not a problem when notes are created from within VSCode (as is 
expected - since Dendron is a VSCode plugin) - I don't use VSCode much.

While Dendron has command line tools to create new notes, and notes are markdown
only the timestamps are problem.

Hence, the need to create 13-digit epoch time.

`date "+%s"` can be used to get the epoch time, it only returns 10 digits.
Since I don't care about millisecond precision in my time stamps, I can always
add 3 extra `0`s at the end, yet it is a hassel.

Hence, the need for solution that does not require manual work 😄 

```shell
ruby -e "puts (Time.now.to_f * 1000).floor"
```
At first, it seemed like it didn't work because I didn't have `puts` 😆

looks like, it is also doing the same - add 3 extra `0`s at the end, 
by multiplying by 1000 - except mathematically 😆

----

Finally, I added this to my espanso configuration file as :

```yaml
matches:
- trigger: ":utime"
  replace: "{{output}}"
  vars:
    - name: output
      type: shell
      params:
        cmd: "ruby -e 'puts (Time.now.to_f * 1000).floor'"
```