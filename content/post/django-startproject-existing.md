---
title: "Django: Create new project in an existing folder"
date: 2024-06-08T11:33:00+05:30
tags: ["python", "django"]
---

Default command for creating a new project is :

 `django-admin startproject my_new_project`

Django will then **create** a folder called `my_new_project` (if it does not exist)

But if folder `my_new_project` exists (even if empty) we get error like : 

`CommandError: '/path/to/my_new_project' already exists`

The way around is to specify a folder name **after** the project name like

`django-admin startproject my_new_project existing_folder`

See the [documentation](https://docs.djangoproject.com/en/5.0/ref/django-admin/#startproject) for details.

