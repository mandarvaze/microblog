---
title: "Vivaldi Browser"
date: 2024-05-21T22:49:37+05:30
tags: ["vivaldi", "browser"]
---

I switched to [^1] Vivaldi browser after reading [this](https://mkennedy.codes/posts/michael-kennedys-unsolicited-advice-for-mozilla-and-firefox/) post by Mike Kennedy

To be fair, I had installed and tried Vivaldi a long time ago. But I think things got better this time around.

I seem to have stuck with it for long time.

One benefit of chromium based browsers (like Vivaldi) is that user has access to vast amount of extensions from Chrome Web store. [^2]

Just today, I explored two other "features" which I'm not sure how I feel about.

Tasks and Notes

It is handy to have these right there, in the browser.

The notes seem quite thought through. It supports folder, and search etc. [^3]

While there is a WYSIWYG editor, the notes are essentially markdown.

There is also option to export notes.

I tried. They are indeed exported as markdown files. Neat!

I may use Notes more than tasks. But only as temporary place. If I'm already in browser, might as well capture it right there.

But later I'll move it to [logseq](/post/logseq-take-2/) and push it to git.

There are other nifty features like web panels, which I still have not explored.

I wish there was a plugin or feature in Vivaldi that automatically closes the inactive tabs after fixed amount of time - like [Arc browser](/post/the-arc-browser/) and then I'll make this one my default browser.

[^1]: My default browser is still Firefox, for my banking, emails etc.
[^2]: So far I've installed only 2 extensions. KeepassXC and Bonjour. I also tried some "free vpn" extension that lets user connect to servers from other countries for 15 minutes (for free). I didn't see the point. So I uninstalled it.
[^3]: Arc also had notes initially, the feature they have since [removed](https://resources.arc.net/hc/en-us/articles/19233788518039-Phasing-Out-Arc-Notes)
