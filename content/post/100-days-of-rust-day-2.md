---
title: "100 Days of Rust : Day 2"
date: 2024-01-11T19:39:37+05:30
tags: ["rust", "learning", "programming language"]
---

Today I started with `Data Types`

While most of it is intuitive, it is useful to go over the documentation.

e.g. For `tuple`, I had expected that to access specific index, syntax would be `mytuple[0]`. 
`mytuple.0` was not I had expected.

I also tried to print the tuple.

In the process, I learnt about pretty printing only from the compiler errors.

The compiler errors are so descriptive 😍

Here is the code in case you are interested

```rust
fn main() {
    let x: (i32, f64, u8) = (500, 6.4, 1);
    println! ("{:#?}",x)
}
```
