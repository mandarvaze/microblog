---
title: "Helix Editor"
date: 2022-05-28T21:49:19+05:30
useMermaid: false
tags: ["helix", "tools"]
---

I came across [Helix Editor](https://helix-editor.com/) on youtube when watching about neovim related video.

It is vim like editor. That is, a modal editor.

But it is different in one fundamental way, it terms of how commands work.

In (Neo)vim, it is action followed by object. So *delete word*  becomes `dw`
But in Helix it is other way round. Object followed by Action.
So it is *word delete* hence `wd`

It takes a bit getting used to.

But in other areas, it is far better that (neo)vim. Lot of things work out of the box.

When I opened a `ruby` file, and `gd` (Go to Definition) worked in the first attempt. Mind blown!

This has never worked in the first try. Definitely not out of the box.

One other thing I liked was context-sensitive menu popups and help texts.

This makes this approachable.

Most of the keybindings are already intuitive.

---

One downside, there are no plugins. But it is in the roadmap.

Since LSP works out of the box, I think I may use it as my editor for coding.

I'll continue using neovim for writing the posts (Although, I wrote this one in helix)
