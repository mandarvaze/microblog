---
title: "Fuzzy Search in fish with fzf"
date: 2022-01-06T15:40:29+05:30
tags: ["terminal", "fish"]
---

I had installed `fzf` some time ago. But default key `**` for invoking `fzf` did not work with fzf.

Upon searching, I came across couple of options. Of those, I chose the [one](https://github.com/PatrickF1/fzf.fish
) with active development.

As of this writing, the last commit was 11 days ago. Very active, I would say. 😄

Installation instruction said  `fisher install PatrickF1/fzf.fish` but when tried, I got an error:
```shell
fisher: unknown flag or command "install"
```
🤔

I tried `fisher add` which seemed like closest option to install.

That didn't work either. 😞

I got
```shell
fisher: cannot add github.com/PatrickF1/fzf.fish -- is this a valid package?
```

Turns out someone else also had same error. The suggestion was to upgrade `fisher` itself.

fisher upgrade also had a minor hiccup. I had to manually remove the old files installed by fisher.

-----

Now that `fzf` was ready, I'm exploring all the cool things it can do.

For starters, interactive history via `Ctrl + R` is amazing.
It is better than default `Ctrl + R` in bash (Reverse history search) or even what fish provides out of the box. (Auto complete commands based on history)

