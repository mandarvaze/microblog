---
title: "Read Paywalled Articles for free - at least some anyway"
date: 2024-06-23T10:44:39+05:30
tags: ["browser"]
---

**If you can afford, please pay!**

On the other hand, paying to read articles is expensive for you, here is an
option (among many others, I'm sure) for you

Install extension like `Click to remove element` from the Chrome Web Store. It works with others Chromium based browsers as well, like Edge, Vivaldi, Arc and many more [^1]

When you see a popup blocking the article, you can use extension like `Click to
remove element` and remove the popup 😄

You may need to remove some more elements like dark background which covers or obscures the article.

Give it a try.

Bonus:

1. Use `Reader view` if/where possible.
2. Use `Print to PDF` on the reader-view page
3. (At least) Vivaldi allowed me to preview the PDF without having to save PDF
locally
4. This idea also works for other popups as well, like Facebook, as per [this](https://addons.mozilla.org/en-US/firefox/addon/remove-html-elements/reviews/2051878/)

[^1]: There is [similar extension](https://addons.mozilla.org/en-US/firefox/addon/remove-html-elements) for Firefox as well.

