---
title: "error: Unknown binary 'rust-analyzer'"
date: 2024-03-30T08:10:18+05:30
tags: ["rust", "learning", "programming language"]
---

As I am setting up my ~~new~~ machine, I came across this error when emacs tried to 
use `rust-analyzer`

```shell
error: Unknown binary 'rust-analyzer' in official toolchain 'stable-x86_64-apple-darwin'.
```

As mentioned in this [SO question](https://stackoverflow.com/q/77453247),
 `which rust-analyzer` did not show any error.[^1]

Luckily, the same SO question also had the solution

I needed to explicitly install it via `rustup component add rust-analyzer`

[^1]: Luckily, the comment in the accepted answer also explained why `which` works.
It is a wrapper, which is created/installed by default, but platform specific
binary needs to be installed explicitly. 
See [this comment](https://stackoverflow.com/questions/77453247/error-rust-analyzer-is-not-installed-for-the-toolchain-stable-x86-64-unknown#comment136550874_77453276)
