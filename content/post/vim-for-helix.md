---
title: "Vim Keybindings for Helix"
date: 2022-09-12T16:39:40+05:30
useMermaid: false
tags: ["helix", "tools", "vim"]
---

Helix is a great editor. I'm trying it more and more these days (Like this post is edited in helix)

While shift from action->object to object->action in itself is big change (muscle memory), there are indeed other differences as well.

Luckily, it is easy to add keybindings to helix.  Turns out someone took this too far, and created a lot of keybindings to make helix more like Vim

Refer to [this](https://github.com/LGUG2Z/helix-vim/blob/master/config.toml) for the entire config.

While I'm not going to use the entire list as is, some keybindings (that trip me in helix) like `0` to go to the beginning of the line, and `$` to go to the end of the line seem worth "borrowing" 😄
