---
title:      "Rails is not ruby"
date:       2023-07-07T19:44:55+05:30
tags:       ["rails", "ruby", "gotcha"]
---

I had written [earlier](/post/language-before-framework/) how learning the
language before the framework is more important.

It is important to know which parts is part of the language, and which comes
from the framework.

This becomes more important when there are defacto web frameworks (rails in ruby,
django in python, phoenix in elixir.. maybe more)
For a newcomer who is learning both the language and framework together (or back to back,
or worse learning the language via the framework tutorials :shudders: )

Here is an example. `to_i` converts a string into an `Integer`

```sh
irb(main):002:0> "5".to_i.class
=> Integer
irb(main):004:0> Integer.seconds
(irb):4:in `<main>': undefined method `seconds' for Integer:Class (NoMethodError)
```

Here is similar example of what I was trying to do

```shell
irb(main):002:0> sleep(ENV.fetch('SOME_DURATION', 30).to_i.seconds)
(irb):2:in `<main>': undefined method `seconds' for 30:Integer (NoMethodError)
```

But it works in `rails` console 🤷‍♂
```shell
$ rails c
Top level ::CompositeIO is deprecated, require 'multipart/post' and use `Multipart::Post::CompositeReadIO` instead!
Top level ::Parts is deprecated, require 'multipart/post' and use `Multipart::Post::Parts` instead!
Chewy console strategy is `urgent`
Loading development environment (Rails 6.1.7.4)
[1] pry(main)> sleep(ENV.fetch('SOME_DURATION', 30).to_i.seconds)
=> 30
[2] pry(main)>
```

Luckily, I was looking at [this](https://stackoverflow.com/a/60021905) for `sleep` syntax

Otherwise, I would have spent a lot of time, confused.
