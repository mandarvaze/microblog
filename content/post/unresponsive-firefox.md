---
title: "When Firefox does not open"
date: 2024-08-10T19:19:32+05:30
tags: ["misc"]
---

Today, after applying latest update of macOS Monterey, I restarted my laptop.

But Firefox won't start 😢

No error, no nothing, no window either.

At first, I uninstalled existing version, downloaded a fresh one from internet
(using Vivaldi). I assumed that maybe my OS update broke something, and
reinstalling might help.

It didn't.

Then, I started Firefox from the terminal hoping to see error, if any [^1]. 

Didn't help.

Next, I decided to remove the cache. [^2] 


Assuming that one of the page was taking too long to load. I wanted to really
remove "history" of all the windows and all the tabs across all those windows. I
didn't know how. So I started by clearing cache.

Normally I would do this from inside Firefox. But today I didn't have that
option. Hence, tried directly removing it from the terminal [^2]

Didn't help.

Final option: Remove the profile folder itself. [^3]

Worked! 🎉

😌

[^1]: `cd /Applications/Firefox.app/Contents/MacOS` followed by `/firefox`
[^2]: `cd ~/Library/Caches/Firefox/Profiles` and then `rm -rf <profile_folders>`
[^3]: `cd ~/Library/Application Support/Firefox && rm -rf *`
