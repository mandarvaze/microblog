---
title: "Hugo Modules"
date: 2022-01-04T17:14:02+05:30
tags: ["hacking", "meta"]
---

Today I spent some more time modifying the [IndieFeed theme](https://github.com/mandarvaze/indiefeed)

As I mentioned [earlier](https://microblog.desipenguin.com/post/deployed-microblog/), I started using hugo modules instead of `git submodule` from the beginning. One of the downside of it was I could not preview my changes without pushing the changes to github first.

Sometimes that might be OK, most times it is not.

How do theme developers work in such scenario. Turns out there is a hack just for that.

So much so that documentation mention *for Local development* at multiple places 😄

The directive is `replace`.  Read the documentation [here](https://gohugo.io/hugo-modules/use-modules/#make-and-test-changes-in-a-module)

I did this in `go.mod` of *this* project. But `hugo server` kept complaining that `go.mod` was missing from my local folder where I work on the theme. (It is not under `themes` folder of this project)

Turns out I need to do the following in **my theme directory**.

`hugo mod init github.com/mandarvaze/indiefeed` which created `go.mod` there. Now `hugo server` had no complaints ✌️

I also removed `theme` entry from my `config.yaml`

```diff
-theme: ["github.com/mandarvaze/indiefeed"]
+
+module:
+  imports:
+  - path: github.com/mandarvaze/indiefeed
+
```

These steps documented [here](https://gohugo.io/hugo-modules/use-modules/#initialize-a-new-module) and [here](https://gohugo.io/hugo-modules/use-modules/#use-a-module-for-a-theme) respectively.
