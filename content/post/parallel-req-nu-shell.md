---
title: "How I generated 2000 parallel requests using nu-shell"
date: 2024-02-20T23:25:40+05:30
tags: ["terminal", "rust"]
---

At work, I need to load test new framework I had deployed.

Usually, I work with QA team. They use JMeter (with Azure Load test) for such task.

But today, the QA person was busy with other tasks, and I didn't want to get blocked.

Since I am learning rust, (nu-shell is built in rust) I remembered that it may
be possible to run parallel requests in nu-shell.

and it is!

The important feature is `par-each` [^1] which executes the command parallely.

`http` [^2] is built-in command for HTTP operations

```sh
1..2000 | wrap i |par-each { 
  (http post 'https://my.url/path?param=4' 
    --headers [Authorization 'Basic xxxx'] '' --full).status  
}

```

[^1]: Read `par-each` documentation [here](https://www.nushell.sh/commands/docs/par-each.html)
[^2]: `http` [documentation](http://www.nushell.sh/commands/docs/http.html)
