---
title: "Updating LunarVim"
date: 2022-04-19T21:53:28+05:30
useMermaid: false
tags: ["vim", "tools", "neovim"]
---

Once I got Neovide working with LunarVim, I realized that I had not updated LunarVim for a long time.

I was already in `rolling` branch, so I did `git pull origin rolling`

Big mistake.

Things broke.

After RTFM, I noticed that there is `:LvimUpdate` command in LunarVim.

But by this time, my config was so broken that nothing worked 😞

Luckily, I found the old instructions that helped me initially.

Following those instructions, I was able to get "up and running"

Well, almost.

I still had to run `:PakcerSync` from normal `nvim` before I could get Neovide with latest lvim

🎉
