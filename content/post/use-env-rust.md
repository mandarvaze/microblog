---
title: "How to use .env files with your rust project"
date: 2024-03-17T07:08:43+05:30
tags: ["rust", "learning", "programming language"]
---

It is very important that secrets are stored in environment variables during the runtime.

During the development, it makes sense to use `.env` file. 
In my last project, there were probably 40+ variables in the `.env`. 
Although that project was in RoR the idea of `.env` itself is not new.

But for me, using `.env` with `rust` is new.

For my current (self) assignment, I needed to store an API Key.
Hence, the use of `.env` file.

Luckily, there is a crate for that 😉 - called [`dotenv`](https://docs.rs/dotenv/latest/dotenv/)

To use it in your project :

1. `cargo add dotenv`
2. At earliest place in your code, add `dotenv().ok();` 


