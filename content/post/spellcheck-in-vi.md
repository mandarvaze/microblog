---
title: "Spellcheck in Vi/m"
date: 2023-07-28T12:48:11+05:30
tags: ["vim", "tools"]
---

I've been using vi for 20+ years and somehow I didn't know that it has
spell check built in 🤷‍♂

Enable it by simply `:set spell`

Now misspelled words are shown in red background [^1] (depending on the terminal's
capability this might vary - I assume)

Off course, this is nowhere near full fledged LSP like `ltex` which I use with
emacs and helix. `ltext` does more than spell check.

Yet having built-in spell checker is always nice.

[^1]: Works with vim as well.