---
title: "Use different init directory in emacs 29+"
date: 2023-09-07T09:41:43+05:30
tags: ["emacs"]
---

Recently when reading about [emacs Bedrock](https://sr.ht/~ashton314/emacs-bedrock/)
I learnt that emacs 29+ has command line option that lets the user pass custom
init directory via `emacs --init-directory=MYDIR` option. [^1]

This makes it easy to try out different starter kits without touching your stable/working config

Currently, I use [Minemacs](/post/minemacs/) on my personal machine.
In past I would rename my `.emacs.d` to try new config. (Then switch it back)

This makes things easier.

[^1]: Refer to [this commit](https://git.savannah.gnu.org/cgit/emacs.git/commit/?id=8eaf04de83fd967c2ab69a4c1dfe44a6a10aa912)
