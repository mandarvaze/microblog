---
title: "Left Stck.me"
date: 2022-02-11T23:15:42+05:30
useMermaid: false
tags: ["career change", "personal"]
---

Today was my last day at [stck.me](https://stck.me) (Formerly Scrollstack.com)
I spent more than a year working with the wonderful engineering team there.
I learnt a lot from everyone on the team, and made some new friends (I already knew a lot of folks from this team before I joined) during my stay.

I will continue to cheer stck.me from the sidelines

-----

Some of my contributions during my time with scrollstack:

* Improve the SEO of the hosted blogs by auto generating the Sitemap from the posts
* Integrate Honeybadger error monitoring tool for proactively addressing the issues in production setup
* POC to generate PDF from Post [Source](https://github.com/mandarvaze/url2pdf)
* Multiple file support for Digital Goods like Gumroad
* Monthly Active User Metrics. Although we did not end up using it, I learnt about [rsyslog](/post/rsyslog/) and `logrotate` in the process
* Refactor tiptapy to use more Jinja and less python. This helped reduce the python code. [Shekhar](https://github.com/shon) helped a lot on this process. 🙏
