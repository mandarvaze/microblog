---
title: "Rename File in Emacs"
date: 2023-06-23T06:07:00+05:30
tags: ["emacs"]
---

First impression people have is that Emacs is an editor.
So in an editor one should just `save as` to different name/path. [^1]

But `save as` is different from renaming a file. `save as` creates a copy 
of the file with a different name/path. So one has two files with old and new
paths.

But rename leaves just one file at thenew path.

Older versions of Emacs (< Emacs 26) did not have this functionality built-in
So either one had to use `dired` or people wrote their own functions.

But now (rather from emacs 26 onward) has this built-in via `M-x rename-file`
(and there is `delete-file` as well)

[^1]: Yes, I know one can rename (or delete) the file in VSCode as well :)
