---
title: "Now I have Email Address on my own domain"
date: 2022-01-17T21:32:31+05:30
tags: ["ops", "admin"]
useMermaid: false
---

I have owned this domain for a couple of years now (maybe more)
But it never occurred to me to have my email address on that domain.
I suppose I always assumed that it would involve setting up own servers and/or cost of server etc.

Usually domain registrars provide some free emails, but their interface is just usable.
Also, it might become one-more-email I'll forget to check 😆

But for my recent secret 🕵 project, I needed a professional email. Not personal email.

Turns out [zoho](https://www.zoho.com/mail/zohomail-pricing.html) has a free forever plan that works well for my scenario.

The plan covers 5 free users, so it worked for me.

Zoho has great documentation. They walked me through setting up all the `TXT`, `MX`, `SPF` and `DKIM` records.

It was really easy.

So now I have `@this-domain` email.

I already have and use zoho mail. So I do not need to check one-more email either 😄
