---
title: "Emacs: How to exclude files when searching in a project"
date: 2022-11-07T12:27:33+05:30
useMermaid: false
tags: ["emacs", "tools"]
---

I use `SPC /` a lot to find stuff inside a project. Since starting on Ruby 
project, a lot of my search results, specifically the first ones are inside
the spec file (which is test case in Ruby)

I may want those instances as well, but more often than not, I prefer the search
results in the source code before test cases.

The project is huge, and I have to scroll a lot before I see non-spec code, 
which becomes tiresome.

While I was looking for solution, I came across two things :

1. `deadgrep` (I think it is a play on `ripgrep`)
2. `.ignore` file

`.ignore` file is similar to `.gitignore` which is specific to `git`

While search tools like `ag` and `ripgrep` honor `.gitignore` in my case, test
files are part of the git repo (as they should be) and are thus not in 
`.gitignore`

This is where `.ignore` came in handy.

I added `*_spec.rb` in a `.ignore` file created in the root of the project.

Voila!

Now search results are much better 😌 

