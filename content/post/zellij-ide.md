---
title: "How to create IDE like experience in terminal using Zellij"
date: 2024-03-30T10:11:46+05:30
tags: ["rust", "terminal", "zellij"]
---

## (undocumented?) Zellij Keybindings

Undocumented, because these don't show up in the default configuration, which shows (I assume) most useful key bindings.
I had to look for these, and found them in Github discussions/issues.

* `Ctrl p d`
  * `Ctrl p` is for `pane`, but `d` after that (which I assume stands for `down`) is not documented.
  * This creates a new terminal in horizontal split fashion [^1]
* `Ctrl n -` to reduce the size of the terminal.
  * By default, it is split 50-50
  * Usually, you'll want more screen real estate for the editor (Helix in my case, but works for (n)vi(m) or emacs-in-terminal as wlel)

There is also a possibility of running a `floating` pane. This may be used for running a server process, which mostly one doesn't need to look at constantly.

It also save screen real estate.

You can start a floating pane via `Ctrl p w` 

Since this a toggle, new floating pane will be created if one does not exist. If one already exists, it will be toggled between hidden and shown.


[^1]: Documented/default `Ctrl p n` creates a pane in vertical split fashion, to the right.
