---
title:      M-x butterfly
date:       2023-11-03T22:39:22+05:30
tags:       ["emacs"]
---

It started with a post on `Emacs Stories` Telegram group.
Someone posted a pretty picture with Emacs-logo-on-a-butterfly and a comment `M-x butterfly`

But the actual command  `M-x butterfly` is boring (to me at least)

I wanted the nice image.

Some nice soul on the group directed me to [this page]( https://commons.wikimedia.org/wiki/File:Mx-butterfly.svg)

First I changed the icon of my ~~doom~~ emacs to this image.

I followed [these](https://www.idownloadblog.com/2023/01/19/how-to-change-app-icons-mac/) instructions.

No immediate success.

Because, I have Emacs installed from `brew`, so one in `/Applications` folder is a symlink to the actual file in `/opt/..`

So initially the instructions did not work, until I opened the real file.

and then it worked!

Now I wanted more.

I have had centaur logo on my Doom dashboard for some time now.
I changed that to that of this butterfly via the following line in `config.el` of my Doom emacs

`(setq fancy-splash-image (concat doom-user-dir "misc/Mx-butterfly.png"))`

The original image is HUGE.

So first attempt did not work, until I resized the image to 230x230 (with resolution of 144)

Butterfly on the Dashboard, butterfly as the icon!

---
Here is an obligatory [XKCD comic](https://xkcd.com/378/)
