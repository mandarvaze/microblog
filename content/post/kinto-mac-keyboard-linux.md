---
title: "Kinto : Mac Keyboard layout on Linux"
date: 2024-11-19T22:15:02+05:30
tags: ["macOS", "linux"]
---

After using Apple laptop for nearly 10 years, I have gotten used to Apple's
shortcuts where most places use `Cmd` instead of `Ctrl`

After switching to Linux, initially I fumbled.

Then I had to be alert, which I did most of the times, but when in the zone, I
would still do `Cmd+c` to copy, and when pasted would be disappaointed that it
did not work 😄

Then I came across [kinto](https://kinto.sh)

Installation went through fine, but at the end I got

> Namespace Vte not available

error. 

The [issue](https://github.com/rbreaves/kinto/issues/302) is already reported on
github, but it is stil open.

But the discussion contains a solution/workaround

`sudo apt install gir1.2-vte-2.91`

After this, I had to manually start the `kinto-gui.py`

Funny thing: I had just started retraining muscle memory, so I'm still fumbling 😂
