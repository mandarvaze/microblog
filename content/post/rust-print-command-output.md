---
title: "Rust: How to print Command Output"
date: 2024-12-30T17:28:55+05:30
tags: ["rust"]
---

I was going thru `struct Command` in `std::process` [^1]

Obviously, I tried the very first code snippet in the rust playground. There is
a very convinient ▸ to run the sample code.

The code ran without any errors.

But no output 🤔

<!--more-->

Ohh, there is no `println!`. That is why.

No, that was not it. (I mean it was, but it didn't help.)
Adding `println!("{:?}", hello);` produced this output

```sh
[104, 101, 108, 108, 111, 10]
```

Earlier, without `:?` - rust compilter told me

> the trait `std::fmt::Display` is not implemented for `Vec<u8>`

The output is a vector (array) of `u8` [^2]

I kept on reading the doc, and later came across `io::stdout().write_all` [^3]

So in order to see the output of the command, I had to add  `use` at the top 
and replace `let hello = ..` with `write_all`

```rust
use std::io::{self, Write};
...
..
io::stdout().write_all(&output.stdout);
```

Final code looks like [this](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=74772ca13b9b2b38511b0c4d8d15f397)

[^1]: See [doc](https://doc.rust-lang.org/nightly/std/process/struct.Command.html)
[^2]: As you can see [here](https://www.ascii-code.com) it is indeed `hello` represented as numbers. `h` is 104 and so on. Ending with 10 which is line feed.
[^3]: There is one for `stderr` as well.
