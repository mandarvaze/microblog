---
title: "Enable Syntax based Fold in Neovim"
date: 2022-07-07T09:48:13+05:30
tags: ["vim", "tools", "neovim"]
useMermaid: false
---

When working on large(ish) code files, sometimes it is easier to fold rest of the code from that file and focus on one "block" at a time.

I use the word "block" because based on the context, it means different things. It could be as big as a class (if the file contains multiple classes) or as small as a `if-else` block.

But this functionality is not always turned on.

One way to test it out is `:set foldmethod=syntax`

It would for that file, for that session. Once you close the file/neovim instance you lose the setting.

Other/better way is to add `vim.opt.foldmethod = "syntax"` in your neovim config. (Assuming you are using lua for your configuration- why would you not ? 😉)
