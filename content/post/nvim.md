---
title: "Nvim"
tags: ["exploring", "vim", "tools", "neovim"]
date: 2021-12-15T17:13:17+05:30
---

I have been using `nvim` for a while. Initially with VS Code, and then as replacement for vi/vim from the terminal.

Since I use Doom emacs as may primary editor, I use `nvim` only for occasional quick edits here and there.

So I had not paid attention, but `nvim` the ecosystem is getting better.

TIL that there are two ways to configure `nvim`. Via `VimScript` and `Lua`

Need to look into each and decide which one to use.

I also did not know there were multiple package managers for `nvim`.
Decided on `VimPlug` without too much research 🤞
