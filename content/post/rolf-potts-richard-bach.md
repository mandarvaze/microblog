---
title: "Rolf Potts and Richard Bach"
date: 2022-10-20T16:09:08+05:30
useMermaid: false
tags: ["writing", "podcasts"]
---

This may seem like strange title for the post, compared to my other posts, and it is.

In case you don't know any of these gentlemen, both of them are writers.

Rolf Potts is known for Vagabonding. Made (more) popular by Tim Ferriss.

Richard Bach may be known for "Jonathan Livingston Seagull" to most people. But I've read a lot of his books.

That does not look like they should be the topic of the same entry.
So why are they in the same post ?

Recently, I listened to Rolf on Tim Ferris podcast, and I noticed both are similar in more than one way.

1. Both are writers
2. Richard was barnstormer in his youth(?), Rolf is known for vagabonding (The activity. The book came later)
3. Both married late in their life (Richard was married once, before he met Leslie)
4. Both married actresses
5. Both call their wives "Soul mates" (Richard and Leslie are no longer together, and Rolf married recently during the pandemic)

-----

If you are interested, you can read about 
* [Richard Bach](https://en.wikipedia.org/wiki/Richard_Bach) 
* [Rolf Potts](https://en.wikipedia.org/wiki/Rolf_Potts) on their Wikipedia pages.