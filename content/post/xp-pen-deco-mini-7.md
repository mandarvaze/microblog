---
title: "XP Deco mini 7"
date: 2022-12-07T05:51:47+05:30
tags: ["art", "hobbies"]
---

I purchased myself [XP-pen Deco mini 7](https://amzn.to/3hfeeyF)[^1] 🎉

Was debating between this and iPad for so long.

This won, cause :

* Much cheaper
* Can be connected to Android phone, Windows PC or macOS

Initial impressions :

* Feels like writing on a piece of paper (as opposed to, on a glass surface)
* The pen is BIG! (Not used to such big pen or stylus)

Still getting used to various sketching software.

I'll start with something easy (Started with Autodesk Sketchbook - Big mistake - Got lost)

Using Zoho [Notebook](https://play.google.com/store/apps/details?id=com.zoho.notebook&hl=en_US&gl=US)
on mobile to draw was already easy with fingers. With pen, it is much nicer.

Drawing on mobile (smaller screen) using this tablet, seemed easier than drawing
on laptop screen (Mostly because I have not found a software that works for me)

I'm so happy!

[^1]: Affiliate link