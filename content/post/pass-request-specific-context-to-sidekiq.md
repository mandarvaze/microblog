---
title: "Pass request specific context to Sidekiq"
date:  2023-06-26T10:40:53+05:30
tags:  ["rails", "ruby", "sidekiq"]
---

In a multi-tenant rails application, usually we extract the tenant, before we process it further.
Sometimes, the process requires us to process the data in background (via Sidekiq) and because
the background job also needs to query the DB, it also needs to set tenant.

Turns out, we can pass "some" data from the controller to the sidekiq job via `ActiveSupport::CurrentAttributes`
While, sidekiq may need to set the tenant again (for more DB queries) but one can save at least one query (to find out the account itself)

Apart from the tenant example, controller may have more stuff in the context, which one can pass to Sidekiq.

You can read the original article [here](https://www.mikeperham.com/2022/07/29/sidekiq-and-request-specific-context/)

