---
title: "Installed `most` pager on macOS"
date: 2022-01-05T17:28:16+05:30
tags: ["tools"]
---

Today while I was browsing thru [The Tao of tmux](https://leanpub.com/the-tao-of-tmux/read#leanpub-auto-read-the-tmux-manual-in-style) - I came across a better pager called `most`

I think it is a play on default pagers called `less` and `more`

Unfortunately, the official website almost makes it impossible to find stuff about it.

Documentation is non existent.

I found elsewhere that most linux distro make it available via their package manager.

I did not find any mention of macOS though.

Installing from source requires something called `S-lang` interpreter. In any case, I avoid installing tools by compiling them locally.

I prefer and trust the package managers.

Just on a whim, I tried `brew info most`, turns out it *is* available. Installing via `brew install most` was a no-op.

I set it as universal pager in `fish` via `set --universal PAGER most`

For now, I have tested it only with `man`. The man pages look nice with syntax highlighting.
