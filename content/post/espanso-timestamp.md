---
title: "Insert timestamp anywhere using Espanso"
date: 2022-11-26T22:14:22+05:30
tags: ["tools", "productivity", "espanso"]
---
I wrote about [Espanso](/post/espanso-2/) long time ago. I have been using 
Espanso since then.

Recently, I had a need to add timestamp in a note. Espanso already comes with
a trigger to insert date via `:date`. As the name suggest, it just inserts
the date.

So I added the following to the `match/base.yml` file.

```yaml
  - trigger: ":timestamp"
    replace: "{{timestamp}}"
    vars:
      - name: timestamp
        type: date
        params:
          format: "%b %d, %Y %I:%M%p"
```

As might be obvious, now I get timestamp just by typing `:timestamp` anywhere.

-----

If you are unsure where the file is located, refer to the 
[documentation](https://espanso.org/docs/configuration/basics/#structure)
