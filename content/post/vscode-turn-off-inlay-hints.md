---
title: "How to Turn Off Inlay Hints in VS Code"
date: 2024-11-25T16:41:25+05:30
tags: ["rust", "tools"]
---

I initially tried to turn off the type hints via `rust-analyzer` extension setting, but that did not work 😞

Turns out it is very complicated (at least for me)
[documentation](https://rust-analyzer.github.io/manual.html#inlay-hints) did not help 😞

Here is what worked for me. Thanks to SO [^1]

- Open Command Palette `Cmd+Shift+P`
- Select `Preferences: Open User Settings (JSON)` from the drop down
- Add the following to existing settings
  - Or create new one, if empty. You may need to enclose it within `{ }` though.

```json
	"editor.inlayHints.enabled": "offUnlessPressed"
```

This will turn off the type hint inlays.

But if/when required, it can be enabled via `Ctrl+Alt`

[^1]: [SO Answer](https://stackoverflow.com/a/72338341)
