---
title: "Pixey.org"
date: 2022-12-13T06:04:37+05:30
tags: ["art", "social media"]
---
Pixey is instance of [Pixelfed](https://pixelfed.org/) server (Just like
there are several servers running Mastodon software)

Just as Mastodon is Decentralized version of twitter, Pixelfed is 
decentralized version of Instagram.

Here is my [pixey profile](https://pixey.org/mandarvaze)
For some reason, when in full view mode, the image appears black. 🤷‍♂
(This is true only for sketched drawn and exported from 
 [ok!so app](https://okso.app/))