---
title: "VSpaceCode"
date: 2023-03-09T09:25:00+05:30
tags: ["tools", "vscode", "editor"]
---
I have written [earlier](/post/using-vscode/) that I've started using VSCode
occasionally for work. Finding files and `find-in-files` is much better and
faster.

But I do miss modal editing.

That is where VSpaceCode comes in.

VSpaceCode is *Spacemacs like keybindings for Visual Studio Code* 

I had come across VSpaceCode almost two years [ago](https://github.com/VSpaceCode/VSpaceCode/issues/125#issuecomment-797593415)

I was using VSCodium at that time (hoping it is faster than VSCode - it isn't - on my old machine)
and could not install VSpaceCode.

Fast-forward 2 years later, I have fast M2 machine and I use VScode.

I installed VSpaceCode extension pack.

I'm still getting used to it. I had used Spacemacs long time ago, but mnemonic
keybinding for Doom are closer to Spacemacs, and thus VSpaceCode

What I most liked about this extension pack is that there is `magit` 🎉
How awesome is that.

I used to make changes in VSCode and just for git I used to switch to Emacs.
Now (in theory) I don't have to switch 😄

(I wrote *In theory* because I might switch to Emacs from time to time anyway,
but now at least not for using `magit`)