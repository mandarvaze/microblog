---
title: "Access Google Bard via API"
date: 2024-01-21T18:07:51+05:30
tags: ["llm", "bard"]
---

Yesterday, I was trying to access Bard API via Rust

The task is WIP because :
1. I'm still learning Rust (Although on [Day 5](/post/100-days-of-rust-day-5/) 
   I was able to access a URL from rust code)
2. I didn't know how to access Bard API

It started with a Youtube video that:
* Uses python code which uses `Bard` library 
  * Not sure if it is official one or third party one
  * In either case it won't be directly useful in rust code
* Uses a hack to get session cookie from browser (which will expire sooner or later)

At that time, I didn't know whether Google has made Bard API accessible via an API

Turns out, they have!

There used to be a waitlist - that URL is now `404`

Instead https://makersuite.google.com/ takes you to https://makersuite.google.com/app/prompts/new_freeform

On the left panel, first item is **Get the API Key**

Select `Develop in your own environment` (Assuming you are doing this for personal project like myself)

Needless to say, copy the key and keep it safe.[^1] 

They've listed the following as a sample. (Add your API key **after** `key=`)

```shell
curl \
  -H 'Content-Type: application/json' \
  -d '{"contents":[{"parts":[{"text":"Write a story about a magic backpack"}]}]}' \
  -X POST https://generativelanguage.googleapis.com/v1beta/models/gemini-pro:generateContent?key=
```

Enjoy!

[^1]: Don't commit to VCS by accident. Additional commit to remove it does not really make it "go away". It is available in older versions 😄
