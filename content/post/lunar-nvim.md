---
title: "Lunar Nvim"
date: 2022-01-24T16:13:11+05:30
useMermaid: false
tags: ["exploring", "vim", "tools", "neovim"]
---

I came across some starter kits for nvim.
Earlier I tried adding individual plugins and learning all the nvim basics, but since I'm pretty happy with Doom Emacs [^1]

But Lunar nvim seems stable and provides a lot of functionality out of the box. 👍

[prerequisite](https://www.lunarvim.org/01-installing.html#prerequisites) page asks to ensure that I have `cargo`.  I knew it a package manager(?) for `rust`, but didn't know whether I could install it standalone. Turns out I can't. `brew` also suggested that I install `rust` 😄 which I did.

The installation script asked me whether I wanted to install npm, python and rust dependencies (as if it was optional). I didn't know what all I might miss if I chose not to install those. My aim was to get a decent nvim setup without having to tinker with manual installations. So I accepted those options.

After that, installation went well. 

It created a wrapper script `~/.local/bin/lvim` which sets up environment variables, and calls `nvim`

Best thing is that it also uses `space` as a `leader` key. So it worked well for my Doom Emacs muscle memory 🎉

It seems to have cool built-in features and sane defaults.

I have since `brew uninstall vim`

I also wanted and created an alias for `view` command to `lvim -R` (which is like `nvim -R` but with `lvim` configuration loaded, I think.)

But `view` command comes with the OS 

`/usr/bin/view` is a link to `/usr/bin/vim` - which I could not remove, even with `sudo`. I got `Operation not permitted` each time 🤷‍

For now, I have settled with an alias `lview`, which ~~I am~~ my fingers are likely to forget, but I will train them 😄

----

[^1]: Who knows ? That might change if Lunar Nvim works really well 😆
