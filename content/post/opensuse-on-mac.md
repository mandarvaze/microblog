---
title: "Installed openSUSE on Macbook Pro (Intel)"
date: 2024-10-30T16:20:32+05:30
tags: ["linux"]
---

My Macbook Pro (early 2015) isn't getting any OS updates for some time.

Only version it supports is Monterey (which is already couple major versions
 behind the latest)

`brew` has also started complaining as `This version is not supported`

So I've been considering installing linux on it for some time anyway.

I finally pulled the trigger and installed **openSUSE Tumbleweed**

I had tried the live version, and knew and it generally works.

Their website suggests to use Offline image to install (and use Live  version
only for testing - even though there is a way to install from within the live
session 🤷‍♂)

But ..

Offline image installation failed[^1], mostly because it could not connect to the
network. 

So I did what  I was planning anyway.

Booted into live environment, connected to the network, and then installed from
there. It worked! 🎉

**Finally, I booted into Linux on my Apple Macbook Pro**


[^1]: Error message are not helpful at all.

From live mode installer, I got error like `exit status 1` No other info. I went
ahead with `ignore` (Since I didn't know what went wrong)

During the offline installation, error dialog offers the user to connect to the debugger, but without information about what went wrong, debugger is not helpful.


