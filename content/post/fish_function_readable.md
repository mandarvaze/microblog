---
title: "Fish function to read webpage in Terminal"
date: 2022-01-16T22:17:02+05:30
tags: ["terminal"]
useMermaid: false
---

I'm really digging [reading web posts in the terminal](https://microblog.desipenguin.com/post/readability/).

After using it for some time, I realized that typing same arguments after the URL (That I usually copied to the clipboard from some other place) is getting cumbersome.

So I created a fish function out of it (*If you see yourself doing the same task over and over, it is a good place to automated it*)

Here is the simple function
```shell
➜ function readweb
      readable $argv | w3m -T text/html
  end
```

Now save it to a file, to reuse it.

```shell
➜ functions readweb > ~/.config/fish/functions/readweb.fish
```

Just paste the URL after typing `readweb`, and hit <kbd>RET</kbd> 🎉

```shell
➜ readweb https://microblog.desipenguin.com
```

[Reference](https://github.com/jorgebucaran/cookbook.fish#how-do-i-create-a-function-in-fish)
