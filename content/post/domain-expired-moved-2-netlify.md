---
title: "Domain Expired : Moved to Netlify DNS"
date: 2023-10-13T09:32:44+05:30
tags: ["misc", "meta"]
---

Last night when I wanted to check this site, but I started getting security error related to certificate.
But it did not say domain expired.

I know Netlify automatically renews the cerificates, so I was confused.

But I was too sleepy to troubleshoot.

This morning, for some reason I checked my other email account which I almost never use except for domain registrar 😄 

The `Domain has expired` email was smiling at me.

Once I renewed the domain, using Netlify DNS was relatively straight forward.
Once all sites were "transferred", I deleted all the DNS records from Digital Ocean


[^1]: I have not been writing regularly for a while. Maybe that is why I didn't notice  🤷‍♂


