---
title: "UV: Superfast pip replacement written in Rust"
date: 2024-06-07T23:11:06+05:30
tags: ["python", "rust"]
---

It is (almost) drop-in replacement for `pip`. We just need to invoke it as `uv pip` instead of `pip`

## How to install ?

* `pipx install uv` : This makes it available everywhere.
* Other alternatives are `curl` ([recommended](https://github.com/astral-sh/uv?tab=readme-ov-file#getting-started)) and `brew` (on macOS)

## It is superfast

I tried installing `Django`

```shell
$ uv pip install Django
Resolved 3 packages in 138ms
Downloaded 3 packages in 2.53s
Installed 3 packages in 228ms
 + asgiref==3.8.1
 + django==5.0.6
 + sqlparse==0.5.0
```
This took 3s (As opposed to 33s taken by `pip`)

Installing `ruff` took less than a second 🤯

```shell
$ uv pip install ruff
Resolved 1 package in 269ms
Downloaded 1 package in 782ms
Installed 1 package in 9ms
 + ruff==0.4.8
```

## Create a virtual environment

`uv` is not just a `pip` replacement. It can also create a virtual environment for you.

Use `uv env` to create a virtual environment.

By default, it is created at `.venv`.

But if you pass an argument, you can use some other name e.g. `uv venv my_venv`

It is a standard virtual environment. So `source .venv/bin/activate` and `deactivate` works as you would expect.
