---
title: "Ruby for Python Programmers"
date: 2022-01-14T19:56:02+05:30
useMermaid: false
tags: ["ruby", "learning", "programming language"]
---

While I noted similarities and difference between Ruby and Python, as I was learning the basics [here](https://microblog.desipenguin.com/post/ruby-syntax-1/), [here](https://microblog.desipenguin.com/post/ruby-syntax-2/) and [here](https://microblog.desipenguin.com/post/ruby-syntax-3/) there is official document on the Ruby site. See [this](https://www.ruby-lang.org/en/documentation/ruby-from-other-languages/to-ruby-from-python/)
