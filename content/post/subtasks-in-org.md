---
title: "How to track subtasks in Org"
date: 2023-05-11T07:24:47+05:30
tags: ["emacs"]
---

*Continuing from [previous post](/post/org-for-todos/) ..*

Turns out there is a specific way to handle subtasks in org-mode.

I used something like following -  which did not work

```org
* TODO Main task [0/3]
- [x] Subtask 1
- [ ] Subtask 2
- [ ] Subtask 3
```
As we can see, I expected to see `[1/3]` but I continue getting `[0/3]`

Correct way to do this is :

```org
* TODO Main task [1/3]
  ** DONE Subtask 1
  ** TODO Subtask 2
  ** TODO Subtask 3
```

With the correct syntax, I don't even need to `C-c C-c` to update `/` in the 
main task. As soon as any subtask chnages from `TODO` to `DONE` (or vice versa)
the header automatically changes 🎉

