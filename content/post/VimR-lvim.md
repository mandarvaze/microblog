---
title: "Got VimR working with LunarVim"
date: 2022-02-26T13:56:01+05:30
useMermaid: false
tags: ["published a blog post", "vim"]
---

I had almost [given up](/post/vimr/) trying to use `lvim` config with GUI. But I persisted (and pestered folks on github issue 🙂)
Finally, I got it working.

I wrote about [How to get VimR to use Lunarvim configuration](https://learnings.desipenguin.com/post/vimr-lunarvim/) so that others can also benefit from it.
