---
title: "Omakub"
date: 2024-06-12T13:21:50+05:30
tags: ["terminal", "omakub"]
externalUrl: https://omakub.org/
---

Yesterday, I came across this new script released by Basecamp.

The one-line pitch is:

> Turn a fresh Ubuntu installation into a fully-configured, beautiful, and
> modern web development system by running a single command.

This was started by DHH, but now has a lot of contributors.

Since I'm not on Ubuntu, I can't directly use it. But I'm tempted to set up
Ubuntu on a spare (?) machine just to try this out.

Since this is how DHH wanted his machine to be setup, it installs apps like Zoom
and Signal etc among others.

But as the intro post says :

> the heart of the pre-configuration lies in the terminal

Since I also spend a lot of time in the terminal on macOS, it was something I
could try. Luckily, most of the tools are available for macOS too.

I really liked `tokyo-night` everywhere.

Here is what I have done so far.

1. [Switched to Alacritty from Wezterm](/post/alacritty-thin-strokes-macos/)
2. Configured Alacritty and Zellij [^1] based on omakub config
3. Installed [flameshot](https://flameshot.org/). Earlier, I used to use
 [Zappy](https://zapier.com/zappy) for annotating screenshots (To be shared with
bug report or a fix.)
4. Installed `eza` replacement for `ls` I had tried `exa` - before `eza` was
forked out of it since `exa` was unmaintained, and for some reason, forgot to
install it when I reinstalled macOS recently.
5. Made same font as omakub my default for Alacritty

Things I did not install:
1. `mise` : I already use `asdf`, so I don't see much use switching. mise also
has task runner functionality, but I use `just` in place of `make`, so I'm good.
2. ~~Neovim [^2] : Happy with Emacs and Helix. Thank you very much.~~ [Update](/post/omakub-lazyvim/)
3. Lazygit : I tried to use it. But I couldn't learnt the keybindings and felt
like I can't use it. For now, I'm happy with `magit` when in Emacs, and just
plain ol' terminal when writing in Helix
4. lazydocker seems interesting. I might try it when I need to interact with
docker a lot
5. Typora: Since I use Helix for markdown (like these posts), I skipped that
section of the demo video. omakub also installs VScode (which I already have)
which can very easily be used for markdown, why an editor just for markdown ? It
might make sense for DHH - who I assume writes lot more text than me.

Overall, this exploration was much fun.

[^1]: Since I was already using Zellij, this was nothing more than theme change.
[^2]: It is not like I've not spent enough time [configuring neovim](/tags/vim/)
"just right" 😄
