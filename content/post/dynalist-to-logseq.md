---
title: "Dynalist to Logseq"
date: 2021-12-19T17:12:17+05:30
tags: ["note-taking", "pkm"]
---

Almost a decade ago, I came across Workflowy.

It is a simple outliner program. Think of a simple white paper of unlimited length, and digital.

That is all it was. No other frills.

One of its shortcomings was no mobile client. One could hack to get it working on mobile, but there was no app.
Then I came across Dynalist.

I used it for a long time. I have it installed on my mobile even today.

As with every app, the data is in the cloud.

I have been always wary of small services that may one day disappear. Bitten once, twice shy. (Anyone remember Posterous ?)

Logseq reminds me of Dynalist. But first and formost, it is local-first tool. All my notes are always on my machine. 

I could back it up the way I see fit. Most obvious would be git.
One of the YouTube video mentions github integration, that auto syncs the contents with github. I'll need to check that later.

This is a long way to say, I've started migrating my notes from Dynalist to Logseq.

