---
title: "What does demunge mean in clojure repl ?"
date: 2024-09-08T13:03:25+05:30
tags: ["clojure", "programming"]
---

I started learning `clojure` yesterday.

Initial useful (for me) command in clojure REPL : `dir` [^1]

As per the example given in the [documentation](https://clojure.org/guides/repl/basic_usage#_dir),
`(dir clojure.repl)` produced list of commands (?) - one of which is `demunge` 

I had no idea about it, and so I tried `dir(demunge)` and it returned `nil` 😆

I could not find it in the documentation, so I asked ChatGPT about it, and it explained the following: [^2]

Since `clojure` runs on top of JVM, compiler converts the code into java bytecode.

As part of this processes, the names are "munged" to suit java ecosystem like `someNamespace__someFunction__123`

`demunge` is opposite of that.

`demunge` is used during java interop and not much used a lot in day-to-day clojure.

`(demunge "someNamespace__someFunction__123")` would generate :

> a more readable version of the name

There are several caveats like :

- Exact behaviour depends on how the names were munged
- This functionality may not be available in all the versions of clojure/REPL

[^1]: This seemed familiar because python also has similar functionality
[^2]: This is paraphrased, not copied verbatim from ChatGPT output. This should improve my understanding
