---
title: "Read The Docs Carefully"
date: 2022-02-01T19:24:41+05:30
useMermaid: false
tags: ["ruby", "learning", "programming language"]
---

Continuing with my Ruby language learning journey, I wanted to use it for more than `Hello world`.

So I decided to try python's `request` equivalent.

Since I didn't know where to begin, I just searched for `requests` on [RubyGems](https://rubygems.org/)

Turns out Gem by the exact name [exists](https://rubygems.org/gems/requests) and with description `Because Requests for Python is awesome` 🤗
But it isn't updated in close to 5 years now.

I also wanted to use something native (i.e. not ported from other languages - although nothing wrong with that)

Initially I tried `net/http` from the standard library.

Several people had already suggested Faraday by then. So that is what I chose.

The code was straight forward, yet it kept failing.

I asked for help on the Telegram group, but people kept suggesting based on their hunch, without reading the code I had shared.[^1]

I know it is not SO, so I shouldn't expect too much, still when one feels stuck and other's suggestions aren't useful, one feels frustrated 🤷‍

Finally, I decided to dig deep (I thought I had already done that, but I guess not 😄)

Turns out when you create a connection, you only specify the server in the connection object, and specify the path when you make a request.
Makes sense looking back. (*I had specified the entire URL, including the path*)

Second most important one was, and related to the first one, or because of the first one, I specified `'get'` as param to `conn.get()`
It is copy/paste from the [Usage](https://lostisland.github.io/faraday/usage/) page of Faraday documentation.

I assumed `'get'` string tells the connection to `GET` the URL. (Looking back, if we are already using `conn.get`, why would we specify string `'get'` again. But .. 🤷‍)
Reason I thought that because I had already specified the path in `connection`.

The problem was I kept getting HTML output, when I asked for `json`. I thought **that** was the problem 😆

It wasn't until I decided to dig down the rabbit hole and reached [this](https://lostisland.github.io/faraday/middleware/json-response) page I realized what may be the actual problem.

Here, they are passing `'json'` to `conn.get`. **That** can't be right, because they are already specifying `f.response :json` in `Faraday.new`

This is when I went to actual [http://httpbingo.org](http://httpbingo.org) and noticed that `json` is a page that returns JSON response. 💡

**Lessons Learned:**

* Don't copy/paste blindly
* Don't assume URLs are placeholders (Most times they are, until they are not)
* Think! (This happened late, but better late than never 😄)


[^1]: To be fair, someone shared working code, which was bit different from mine, in a sense it did not use connection, but by then I had already solved the problem myself.
