---
title: "Rust: How non-primitive types are available in the default scope"
date: 2024-05-27T08:34:30+05:30
tags: ["rust", "learning", "programming language"]
---
In rust, `str` is a primitive type, but many non-primitive types are also in scope by default.

e.g. We do not need to add `use` statement to use `Vec` - which is NOT a primitive type.

It comes from `std::vec`

So `Vec::new()` is really `std::vec::Vec::new()`

`Vec::new()` works because Rust inserts this at the beginning of every module: 

```rust
use std::prelude::v1::*;
```
This makes `Vec` (and `String`, `Option` and `Result`) available by default.

---

*I learnt this from [learn rust in half an hour](https://fasterthanli.me/articles/a-half-hour-to-learn-rust#the-libstd-prelude)*
