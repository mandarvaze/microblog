---
title: "First Ruby MR"
date: 2022-04-06T22:10:42+05:30
useMermaid: false
tags: ["ruby"]
---

I just looked at the last post here. It was in late Feb 2022
I just realized that I have not posted anything in entire of March 2022

The reason for this microblog was to make it low-friction so that it does not go the way of my other blogs with month(s) long and sometime year long silence.
But then why didn't I post anything here for a month ?

I started working with a startup that is bigger than ones I was working with last decade or so.
and they have processes, and meetings. 

I get the importance of both - at bigger scale.

But then I got busy in meetings. Team meetings (some that go on for 2 hours. Every week) and 1:1 meetings, more of collaboration (these I like)

Hardly leaving time for anything else.

Hence no update for entire of March.

Now that I've kinda settled, I have just enough time to tell you about my First Ruby MR.

------

Since I started working here, this is technically my second MR. But the first one was updating the README 😄

This MR is also not real code change. This is more of configuration for `rspec` and set of changes to remove the secrets that were inadvertently checked into git.

I just felt great so write code after a long time 😄
