---
title: "Back to DDG"
date: 2021-12-16T16:27:09+05:30
tags: ["exploring"]
---

[Few days ago](https://twitter.com/mandarvaze/status/1466204219434700803), I came across you.com search engine.
I tried it for solid 15 days. There was no set plan for "trial"

It presents the result in card like fashion, which I was OK with. But I think rendering take a bit longer. So much so that I noticed the delay.

Switching back to trusty DDG 😇
