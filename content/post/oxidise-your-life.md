---
title: "Oxidise Your Life"
date: 2024-02-04T12:04:00+05:30
tags: ["rust"]
externalUrl: https://www.youtube.com/watch?v=dFkGNe4oaKk
---

This youtube video mentions the following tools:

* exa
* bat
* zellij
* mprocs
* ripgrep
* irust
* bacon
* cargo-info
* ncspot
* porsmo
* speedtest-rs
* rtx-cli

Of these I installed zellij, which is replacement for tmux (or screen)

I also installed mprocs, irust

I already have ripgrep

I had tried `exa` and `bat` in past, they are good but novelty item.

Installing `bacon` failed during the compile step :(

I've also installed nu-shell

I'll write about nu-shell [separately](/post/nu-shell/), after giving it enough time
