---
title: "100 Days of Rust : Day 4"
date: 2024-01-13T22:14:35+05:30
tags: ["rust", "learning", "programming language"]
---

Started with (optional) assignment at the end of "Programming Concept"
chapter : Convert temperature between Celsius and Fahreinheit. [^1]

Since the algorithm itself is not part of the learning the language, I asked 
Google Bard about the formula.

It was nice working on some code after a while (Aside from work, I mean)

Once again, I realized that compiler is very helpful 😍

I started simple.

Breaking down functionality into smaller functions. (Makes me think of unit 
testing them. Note to self : Read up about unit testing in rust)

**I learnt about handling default case in `match`**. (Once again, I asked Bard)

Why did I need default case ? Cause the compiler told me that I need one 😄

I also learnt :
* that unused/ignored return value can be stored in `_` (like python and ruby)
* numeric data types like integer and floats can not be mixed. If you want result in `float`, everything *must* be float. Respect!

[^1]: Code available on [github](https://github.com/mandarvaze/learning-rust/tree/main/temparature)
