---
title: "Floating Terminal in Neovim"
date: 2022-05-16T16:02:51+05:30
useMermaid: false
tags: ["vim", "tools", "neovim"]
---

Today I discovered that I can open a nice floating Terminal in the middle of the screen by pressing `Ctrl+\`

Thanks to `toggleterm` plugin.

Earlier I used to use `:terminal` command, which would open a terminal in another tab. I had to switch tabs to use the terminal and going back to editing the files.

With `toggleterm` I press `Ctrl+\`, do my terminal tasks, and toggle it again to send it the background. (At least that is what I think happens, since next time I summon it, I can continue where I left off, not a new shell.)

On other benefit was that it behaves like normal shell. Let me explain.

When I used to use `:terminal` the shell would open in normal mode. To type commands, I had to explicitly enter the insert-mode. While a minor inconvenience, it still feels odd.

