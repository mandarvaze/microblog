---
title: "Terminal : How to copy files showing progress"
date: 2024-10-24T06:00:44+05:30
tags: ["terminal", "tips"]
---

While copying large amounts of files (lots of photos) I used to use
`Finder` in the macOS. Good ol' drag-n-drop

Usually it is reliable, but lately I'm facing 
[some problems](/post/macOS-disk-not-ejected) when copying files to external
disk.

`Finder` itself would become unresponsive.

So I decided to use terminal instead.

It worked (kinda) but it took 51 minutes to finish the copy (There were a 
lot of files)

and as you know traditionally `cp` does not show progress.

That is when I discovered `rsync` 
I had used it ~~a long time ago~~  decades ago, but never for local file copy.

I always used it as it was originally meant to be used [^1] to copy files
to and from another machine.

Turns out using it for local file transfers is a good option especially since 
it has `--progress` option.

The command I used :

`rsync -vhz --progress /path/to/source-dir/* /path/to/destination-dir` 

- `v` : Verbose
- `z` : Compression
- `h` : Human readable
- `progress` : Duh!


[^1]: `r` in `rsync` stands for `remote`
