---
title: "Obsidian has vim keybindings"
date: 2024-01-05T23:05:59+05:30
tags: ["tools", "vim", "editor"]
---

Today I discovered that Obsidian supports vim style keybindings out of the box.

But it knows that vim is not for everyone.

So it checks whether you know what you are doing (when you enable the setting,
which is OFF by default) by having you enter a command in a pop up 😄

TBH, one can easily find out that command and "cheat". 
On the other hand, if you don't know vim, and still want to turn on the
 setting (by cheating) - you deserve what you get 😄

In case you don't know, you can enable it from the `Editor` section of the settings.

So far most things I use (from muscle memory) have worked so far.

---

Two other things related to using vim style editing in Obsidian are :

1. Turn ON the line numbering.
2. Install and use community plugin named `Relative Line numbering`

---

I've also installed `MAKE.md` plugin (more about it later) which adds `/` commands
for easy editing.

When using vim keybindings, it works only in `insert` mode (as it should)
Otherwise `/` invokes search, as it does in vim

