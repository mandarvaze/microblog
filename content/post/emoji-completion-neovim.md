---
title: "Emoji Completion for Neovim"
date: 2022-05-16T06:26:40+05:30
useMermaid: false
tags: ["vim", "tools", "neovim"]
---

I've started watching this YouTube [playlist](https://youtube.com/playlist?list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ) `Neovim from scratch`

It is a great learning resource.

I use a program called `espanso` for hotkey expansion across the OS, that has emoji completion option. But it does not work with Vim. Maybe because of modal editing 🤷

While it took me some time to figure out, setting it up is quite straight forward.

1. Install the plugin. Add the following '~/.config/nvim/lua/user/plugins.lua' file
`use "hrsh7th/cmp-emoji" -- Markdown emoji` (Search for `-- cmp plugins` in the file)

2. Add it as a source for `nvim-cmp` Add the following '~/.config/nvim/lua/user/cmp.lua' file
`{ name = "emoji" },` (Search for `sources =` in the file)

Restart nvim to be sure (But not required.)

Now as soon as you type `:` you get suggestion like this: 

![emoji completion in nvim](/image/emoji-completion-nvim.png)

Isn't it 🆒 😄
