---
title: "Obsidian"
date: 2024-01-02T23:45:22+05:30
tags: ["exploring", "note-taking", "tools"]
---

Obsidian is not new to me. I come across it from time to time, but never paid much attention.

That changed when I [migrated](/post/new-pkm-site/) my PKM site to use Obsidian-zola.

Technically, it use Zola, and has nothing to do with Obsidian (for Publishing)

The "source" is maintained in Obsidian, but it is just collection of markdown files, which can easily be 
maintained outside.

But then I got Obsidian desktop installed. I used it for making minor edits to markdown files.

But not much.

Recently, I came across Obsidian plugin called `Advanced Slides`, and down the rabbit hole I went 😄

There are (as of this writing) 1300+ community plugins. Lot of them (statisticially speaking) must be unmaintained.

So when installing a community plugin, I check `last updated` date. If it is > 1 year, it is probably stale.

Something that is updated within months, is better.

I came across some good ones (I think)

Currently, I'm exploring Plugin called ExcaliBrain.



And before I forget, Happy New Year 2024 to you and your family.

-----

*My first post of 2024*
