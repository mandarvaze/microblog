---
title: "After Factory Resetting the Phone"
date: 2023-03-21T09:22:19+05:30
tags: ["misc"]
---

(*Read part 1 [here](/post/resetting-phone/)*)

After the initial setup, I installed the apps that I use the most.
I had taken backup of my Google Authenticator codes via a QR code, which I sent to myself.

I was able to scan it back from my Desktop Telegram app and all was well.

Restoring Microsoft Authenticator - also used for work - was not so easy.
I couldn't open an IT helpdesk ticket without logging into the Service Portal, which required me to be authenticated via MS Authenticator 😄

Luckily, I was able to work with IT person in my timezone via Teams DM and got it sorted 😌

I installed few other apps that I know I always use. (Some of them require setup/login - which I have not yet done)

But I lost lot of (all?) of customizations. Simple things like ringtones.

I also lost a few contacts - but google sync got most of them back (I think.) Looking back, I should have synced my contacts just before I did a factory reset. (Hindsight 20/20 and all that)

I had a lot of music downloaded - I have a backup, so I copied some of them back.

I had a bunch of recurring alarms that were lost. But I recreated the most used ones from memory (Some I didn't need, cause they are part of my memory now)

Some customizations were impossible to recreate. e.g. I used to use Google Indic keyboard - which isn't available any more. It is now part of GBoard (I think) but it is slightly different. I can type indic languages, but it took some time to get used it.

On the plus side, I'm sure lot of "junk" data and apps were cleaned up. I might have installed some app - just to try out, and never used it more than a few times. (And I don't remember what it was - so I'm unlikely to reinstall it)

The phone is working as expected.

It does feel new (Physically the phone was always in good shape)

`---`

Armed with this experience - I'm gonna upgrade macOS now.

My current version - Catalina - is unsupported by homebrew - and whole lot of new apps are not available on this version.

I've already done with files backup.

It is a 12+ GB download.

So all I need is some courage and bandwidth (Plan to start the download at night - hopefully by morning - download would have been complete 🤞
