---
title: "How to specify the size for the embeded image in Obsidian"
date: 2024-07-31T10:49:23+05:30
tags: ["obsidian", "note-taking", "pkm"]
---

TL;DR: When inserting a link (especially of the image) one can add pipe followed
by size.
This size is used for displaying embedded image inside the note.

For example, I have a markdown and excalidraw mixed note named *Dashboard* 

It references itself (for the excalidraw image on the back of the note) like 
`![[Dashboard]]` [^1]

This results into an image that just shows small preview that is not useful,
especially if image is wide (and/or tall).

But now I can specify it as `![[Dashboard]]|640x480`, which results into decent
sized preview.

Refer to [this segment](https://www.youtube.com/watch?v=P_Q6avJGoWI&t=388s) in
the YouTube video to see it in action.

 [^1]: There is more to this which I'll cover separately.
