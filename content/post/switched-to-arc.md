---
title: "Switched to Arc"
date: 2023-04-09T22:20:29+05:30
tags: ["tools", "browser"]
---

I wrote about Arc browser earlier [here](/post/the-arc-browser/). At that time, I was still on macOS catalina, version of macOS not supported by many, including `brew` and Arch browser.

Now that I have [successfully upgraded](/post/upgrade-to-monterey/) - I was able to get Arc working on my personal machine as well.
I did not wish to make this default on my work machine - wasn't sure if it was supported (based on Chromium, so it should work, still if something does not work, getting support from IT won't be easy. Better stick to main stream browsers for office work)

But on personal machine, anything goes.

So as soon as I installed Arc, I made it the default.

TBH, I still use firefox for
* Gmail
* Netbanking

But that is it.

I have been enjoying the feature for which I made the switch, namely auto-expiring tabs 😄

Initially, I was bit surprised - what happened to that thing I was reading last night ? Ohh - it expired.

Off course, If I really *need* it, I can make sure it stays, or open it again.

But that is the point.

Most of the times, we don't.
