---
title: "100 Days of Rust : Day 7"
date: 2024-01-29T09:15:53+05:30
tags: ["rust", "learning", "programming language"]
---

*Technically this may be more like day 8 or 9, cause I did read some stuff
from the rust book in last few days, and made note
[here](https://pkm.desipenguin.com/programming/rust/rust/)*

> Nothing improves your understanding better than **doing**
>
> -- Me 😄

I was trying `accessing the individual fields in tuple struct using dot
notation via the index` 

Since the rust book does not have an example of it, I used 
[rust playground](https://play.rust-lang.org/) (Awesome resource BTW) and 
just printed stuff.

```rust
// struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

fn main() {
    // let black = Color(0, 0, 0);
    let origin = Point(1, 2, 3);

    println!("{}", origin.2);
}
```

While this worked, I got the following long warning by the compiler

```shell
--> src/main.rs:2:14
  |
2 | struct Point(i32, i32, i32);
  |        ----- ^^^  ^^^
  |        |
  |        fields in this struct
  |
  = note: `#[warn(dead_code)]` on by default
help: consider changing the fields to be of unit type to suppress this warning while preserving the field numbering, or remove the fields
  |
2 | struct Point((), (), i32);
  |              ~~  ~~
```

It took me some time to get this. At first, I thought this was an error. 
(When I read carefully, I realized it is only a warning)

Turns out it was telling me about *unused* data 😄

It wasn't clear to me (at least) 🤷‍♂

Then I printed all three fields, and the error went away 🎉

(and I learnt that indices are zero based - ha ha)
