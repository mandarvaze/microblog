---
title: "Helix Open File on Side"
date: 2022-10-13T09:14:04+05:30
useMermaid: false
tags: ["helix", "tools"]
---

When I am working on a project, it is highly likely that I would open other files from that project.
In order to do that, in Helix I would do `SPC F`. This opens up file picker for that workspace.

I can then either navigate using arrow keys, or most likely start typing the name of the file and the file will be highlighted.
As a side benefit, the file is shown on the right pane as a preview.

If I hit `Enter` the file will be opened in the same buffer. While I can switch between buffers using `SPC b`)
I may want to work on both files at the same time, say for comparing the code etc.

In such cases, while the file is selected in the file picker, press `Ctrl+v` to open the file in a vertical split to the side
🎉

----

*I learnt this trick via [this youtube video](https://youtu.be/xHebvTGOdH8)*

