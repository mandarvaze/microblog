---
title: "Emacs Mode in Shell"
date: 2022-12-05T16:46:52+05:30
tags: ["emacs", "tools"]
---

I have been using Emacs for several years now. But I switched to
(and stayed with) Emacs only because of evil mode.

I had been vi user for decades before that. Even today, I use (Neo)vi(m)
occasionally.

Right now, I'm writing this in Helix editor, which is lot closer to modal editing
of vi, than of Emacs.

So there.

I am aware that various shell support `vi mode`, but the default is Emacs.

But I never invested time to either turn on vi-mode or learn Emacs keybindings

Till last week.

One of my colleagues told me about `Ctrl-A` to move to the beginning of the line
and `Ctrl-E` for the end of the line.

This has been so helpful.

Now I am motivated learn more Emacs keybindings.

I might also try to turn of `vi mode` for zsh.

