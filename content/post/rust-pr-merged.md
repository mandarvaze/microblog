---
title: "My PR related to Rust project got merged 🎉"
date: 2025-02-19T13:13:35+05:30
tags: ["rust"]
---

I'm so happy that my PR [^1] to a new and upcoming #rust framework called
[Cot](https://cot.rs/) was merged yesterday.

[@m4tx](https://github.com/m4tx) - Thanks for accepting my contribution 🙏

<!--more-->

Although it is a PR for the documentation, I'm so glad that I was able to
contribute to a Rust project.

Why is the important ?

- It is **not** my personal project
- It showcases that even though I'm new to Rust, the concepts like - making
even sample code secure - is a transferrable skill 💪
- Gives me hope that now I may be able to contribute code as well 🤞
- I consider this "foot in the door"
- Boosts my confidence.


[^1]: [The PR](https://github.com/cot-rs/cot-site/pull/8/files) 

