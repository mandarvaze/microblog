---
title: "Interruption is the Enemy of Productivity"
date: 2022-07-08T16:45:54+05:30
tags: ["podcast", "work"]
useMermaid: false
---

Yesterday, I listened to the episode of rework podcast, titled [Interruption is the Enemy of Productivity ](https://www.rework.fm/interruption-is-the-enemy-of-productivity/) 😄

The link has full transcript of the episode, or listen via your favourite podcast app.

Some quotes (edited little to make sense) from the episodes :

> people know that the time they get the real “work done” is when they don’t have the interruptions.

> If that time only exists outside of work, that is where they will find it. So it’s early mornings, it’s late nights. It’s weekends. It’s a lot of the overwork 

> [work is] pushed into the margins because the main bulk of the working hour is just jammed full of these interruptions. 

> You can’t get work done at work, that the hours you actually, have dedicated to sit in front of your computer are eaten by the interruptions.

> Coordination is work too ..... It's not the nourishing work.
