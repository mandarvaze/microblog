---
title: "Homework for Life"
date: 2024-05-22T07:39:29+05:30
tags: ["misc", "podcast"]
---

In [One percent better podcast](https://www.onepercentbetterproject.com/the-podcast) [^1], Joe interviews [Matthew Dicks](https://matthewdicks.com)

Matthew explains the Homework for Life [^2] as (paraphrased)

> At the end of each day, write down important "stories" (not memories) of the day.
> Else life passes by, and we don't remember what you did 12 years from today. (Cause all days look the same in past)

He is a teacher. He asks his students to narrate stories (on a Monday, start of the school)
He mentions that 10-year olds can come up with excellent stories. One example he mentions is 

> There was water on the floor. There was blood.

The last sentence makes the listener wanting to know more.[^3] That is good story.

Few other points about stories:

* **Location and action are important**
* You should mention the location, say classroom and let the reader imagine it for themselves. Everyone will imagine it differently. As long as the specifics of the classrooms are not relevant to the plot, there is no point talking about the details.
* Reader is tracking the points. Say you mentioned that the girl had green eyes, and the green eyes are never mentioned till the end, what was the point of mentioning the eye color ? Maybe just say "Beautiful Girl" and let everyone imagine their own beauty.

-----

I've started doing this from yesterday.

Instead of "reporting" on the incidents of the day (in a journal) which can be dry and repetitive, tell a story.

At least for last two days, I was able to do that 🤞

[^1]: I could not find direct link to the podcast with notes and all. So I had to search for "Matthew Dicks" but that also doesn't work directly because not all episodes are listed. They are loaded when one scrolls down to older episodes. This one was roughly in June 2023.
[^2]: Ali Abdaal also mentioned this in his [newsletter](https://aliabdaal.com/newsletter/homework-for-life/)
[^3]: If you are curious to know what happened, listen to the episode 😆
