---
title: "Obsidian.nvim plugin"
date: 2024-01-09T21:05:55+05:30
tags: ["exploring", "note-taking", "tools"]
---

After using vim keybindings inside Obsidian, I came across [obsidian.nvim](https://github.com/epwalsh/obsidian.nvim) plugin
that makes working with Obsidian from inside Neovim pleasurable.

This is definitely better than vim keybindings in Obsidian [^1]

But I had not not used neovim in a long time. After I upgraded to [Monterey](/post/upgrade-to-monterey/), 
I had not installed neovim again.

So getting it (as in neovim) was a bit of work. But I had my old config saved, so at least I didn't have
to start from the ground floor. (On the other hand, having old/out dated config could be worse than starting
from the ground floor 😄)

Then getting the plugin working (with my `Neovim from scratch` config) was some more work.

But I got it working.

Getting `:ObsidianToday` working in `nvim` was a great feeling

[^1]: Other than `evil-mode` in Emacs, vim emulation in every single editor leaves a lot to be desired.
