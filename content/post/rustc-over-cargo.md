---
title: "Rust: When rustc Beats Cargo for quick trial code"
date: 2025-01-14T09:00:39+05:30
tags: ["rust"]
---
Recently, I needed to ~~test~~ try some functionality (of getting user's home
directory) [^1]

Setting up a throw away project via `cargo new` is certainly possible
but seemed overkill.

<!--more-->

A little bit of googling, presented me with an option of using just
the rust compiler, without having to setup a project.

Just create a rust source file, say `test.rs` and compile it via
the Rust compiler as : `rustc ./test.rs`

(If there are no errors) It creates a binary called `test` 🎉 [^2] 

I know what you are thinking.

**Rust playground!**

Yes, So far in my rust learning journey, that is what I have been using
and it works for most cases. But,

- It requires Internet (`rustc` works locally)
- Sometimes you need to test something locally, like getting files
  from root directory. You may not have access to root folder in
  rust playground

While I was gloating about my epiphany, I remembered I had installed `irust` [^3]

It is `ipython` like interactive REPL for rust.

At first, I was skeptical how well it will work.

But it works well.

At least for this particular case anyway.

and it is also local (No internet required!)

Here is the output of `irust` session :

``` rust
In: use std::env;
In: match env::home_dir() {
..: Some(path) => println!("Your home directory, probably: {}", path.displat()),
..: None => println!("Impossible to get your home dir"),
..: }
error[E0599]: no method named `displat` found for struct `PathBuf` in the current scope
 --> src/main.rs:6:66
  |
6 | Some(path) => println!("Your home directory, probably: {}", path.displat()),
  |                                                                  ^^^^^^^
  |
help: there is a method `display` with a similar name
  |
6 | Some(path) => println!("Your home directory, probably: {}", path.display()),
  |                                                                  ~~~~~~~

In: match env::home_dir() {
..: Some(path) => println!("Your home directory, probably: {}", path.display()),
..: None => println!("Impossible to get your home dir"),
..: }
Out: Your home directory, probably: /home/mandar
()

In:  
```

[^1]: I'm gonna write about my new project in a separate post, soon. This is related to that project.
[^2]: It is possible to generate a binary with other name using `-o` option. But we are already talking about throw-away code
[^3]: [`irust`](https://github.com/sigmaSd/IRust) is so awesome that it deserves a separate post
