---
title: "Bundler"
date: 2022-01-27T21:12:32+05:30
tags: ["ruby", "learning", "programming language", "python"]
useMermaid: false
---

If I were to explain `bundler` to pythonista (like myself) I would say

> Bundler in Ruby land is like `poetry` in python land, except it does not create sandbox environment

To explain it a little more. It tracks dependencies in a `Gemfile` (and `Gemfile.lock`) which then goes in your repo.
Other engineers sharing your code would then run `bundle install`, and they get exact same Gems (including versions) on their machines.

In order to add entries to the `Gemfile` one does `bundle add`, which updates the `Gemfile` **and** installs it locally

This is somewhat like `poetry add` updating `pyproject.toml` and `poetry.lock`
`Gemfile` is like `requirements.txt` which one updates manually.

(Just like `poetry`) one needs to create base/empty `Gemfile` via `bundle init`
As I found out, one can't directly `bundle add` (Although it might be a good enhancement to create `Gemfile` if one is not found 🤔)

Also, I think the documentation should mention `bundle add` much earlier.

If I had not used `poetry` I could not have guessed about it.

----

Ref: Bundler [documentation](https://bundler.io/docs.html)
