---
title: "Conventional Commits"
date: 2024-05-02T18:47:39+05:30
tags: ["tools", "git"]
externalUrl: "https://www.conventionalcommits.org/en/v1.0.0/"
---

Few days ago, I heard about this on some podcast (maybe changelog ? 🤷‍♂)

I have been using similar notation for my commits - [even for this blog](https://gitlab.com/mandarvaze/microblog/-/commits/master)[^1] - but I did not know it is a specification.

The idea is that very first word of the commit message should indicate what sort of commit is it.

Most common ones are : `fix`, `feat` (To indicate bugfix and a feature)

Other useful ones are: `docs`, `style`, `refactor`, `test`

Then there is `chore` (catch all ? 😄)

I've used some of these at work as well.

As the website mentions, benefit of such style is that some tools can parse the git log to automatically generate the changelog.

Website also mentions, that 

> it’s not the end of the world if a commit lands that does not meet the Conventional Commits specification. It simply means that commit will be missed by tools that are based on the spec.


[^1]: Since the commits are just addition or updates to markdown files, most of the tags are not used here. I only use `Add` and occasional `Update`. I do believe it is in the spirit of conventional commits 😄

