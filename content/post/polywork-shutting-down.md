---
title: "Polywork Shutting Down"
date: 2025-01-01T09:53:10+05:30
tags: ["misc"]
---

This morning, I got an email notification that polywork.com shutting down.

Polywork was a good idea.

I never updated it regularly because I was worried about such thing (it going 
away) But I liked the idea of putting regular "updates" and I started **this**
microblog.

<!--more-->

<blockquote class="quoteback" darkmode="" data-title="Appflowy" data-author="Mandar Vaze" cite="https://microblog.desipenguin.com/post/appflowy/">
<p>One of the reason I created this microblog is for the same reason. Otherwise, Polywork seems nice for such “What am I doing right now” notes.</p>
<p>What if Polywork goes away one day ? Once bitten twice shy.</p>

<footer>Mandar Vaze <cite><a href="https://microblog.desipenguin.com/post/appflowy/">https://microblog.desipenguin.com/post/appflowy/</a></cite></footer>
</blockquote>
<script note="" src="https://cdn.jsdelivr.net/gh/Blogger-Peer-Review/quotebacks@1/quoteback.js"></script>

In fact, I had considered `polywork.desipenguin.com` as well. 

But then, I changed my mind for the same reason.

Polywork is a specific website which if goes away, may not make sense in the 
future. microblog on the other hand is a concept (I'm aware of https://micro.blog/)

I'm sad to see Polywork being shutdown.

Their landing page has no mention of it ¯\_(ツ)_/¯. 

But New Signups are disabled. I saw proper message only on one page.

Other places, Sign up leads to 404. Kinda bad, but then what does it 
matter if the website is going away in a month.
