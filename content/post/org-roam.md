---
title: "Org Roam"
date: 2023-02-10T12:07:33+05:30
tags: ["org", "emacs"]
---

Since I started using [minemacs](/post/minemacs/) few days ago, I discovered
some new things like org-roam

As you might know, it is implementation of concepts from Roam research in org-mode
(That may not be accurate description, but close enough)

I had tried org-mode long time ago, when I moved from vi/m to Emacs.

But later, as I wanted to share (publish) my notes, I settled on markdown format.
(Yes, I'm aware that org-mode allows publishing notes, but for now 
I prefer native markdown files)
Yet, org seems useful for personal notes since it is integrated within Emacs.

I had also written about [Stashpad](/post/stashpad/) and I use as 
my `Developer notebook` ever since.

Since these are just snippets of data/information/instructions that are not
necessarily useful for others and/or are specific to the work and thus private,
these make perfect candidates for storing in org-roam

org-roam are still text files.

Stashpad notes OTOH are stored in proprietary (binary?) format (I don't know 
this as a fact, just guessing)

So I've started moving my notes from Stashpad to Org-roam
