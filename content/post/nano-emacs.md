---
title: "Nano Emacs"
date: 2023-02-23T22:12:44+05:30
tags: ["emacs", "tools"]
externalUrl: https://github.com/rougier/nano-emacs
---

NANO emacs looks quite amazing, polished.
Unfortunately, evil-mode is not turned ON by default.

One thing I'm going to try is to combine NANO theme (and some related
packages) with Minemacs.

Minemacs has evil mode and keybindings which are close to Doom and I had
no problem switching to it.

NANO emacs README has instructions on how to install just the theme and other
parts, so that might do the trick.

🤞
