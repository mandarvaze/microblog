---
title: "Grammar Check with Helix Editor"
date: 2022-05-30T15:57:40+05:30
tags: ["helix", "tools"]
---

One of the thing that I thought was missing from Helix editor was integration with a decent grammar checker tool.
On Emacs and neovim, I use `ltex-ls`.

To my surprise, it works well (I'm using it already) with Helix. 

It is probably not well documented though. I had to look through the github issues, but I did come across the [solution](https://github.com/helix-editor/helix/pull/2182#issuecomment-1103762804).

It is straightforward though.

1. Download the latest release of ltex-ls from [here](https://github.com/valentjn/ltex-ls/releases). I preferred the one which comes with jre. Size increases, but it is self-contained. One can choose smaller download if jre is already installed in your setup.
2. Extract it somewhere (e.g., `~/ltex-ls`) then create a symbolic link to `bin/ltex-ls` from somewhere on your path (e.g. `/usr/local/bin/`) like this :
`sudo ln -s ~/ltex-ls/bin/ltex-ls /usr/local/bin/ltex-ls`
3. Add the following to `languages.toml` file:
```toml
[[language]]
name = "markdown"
language-servers = [ "ltex-ls" ]
file-types = ["md"]
scope = "source.markdown"
roots = []
```
4. Restart Helix.
5. Enjoy 🎉
