---
title: "macOS : Whichspace"
date: 2024-07-09T21:32:58+05:30
tags: ["macos"]
externalUrl: https://github.com/gechr/WhichSpace
---

I have been using macOS for almost 9 years, and never used the workspaces
feature (Not sure when it was introduced)

But after looking at [Omakub](/post/omakub/) I started using this feature.

Essentially, I divided the apps as follows. (Or at least that is what I
wanted 😄) Each app (window) on its own workspace.

* Alacritty
* Firefox
* Vivaldi
* Whatsapp and Telegram (Social apps) together on separate workspace.

But somehow, some of the browser windows (I think new ones, after I moved
existing windows to different workspace) moved to different ones.

Suddenly, it started feeling "jumpy"

(I think) I eventually figured out how to ensure that all the firefox windows
remain in the same workspace [^1]

While looking at all these, I came across Amethyst (Still exploring, maybe
upcoming post) and also WhichSpace (finally app mentioned in the title of this
post 😆)

This is really tiny utility. It shows the desktop number (like 1, 2, 3, 4) that
is all.

But it is useful (at least I think so)

[^1]: In the dock, right click on Firefox icon -> Options -> Assign to -> This
Desktop. The default is `None` hence the seemingly erratic behaviour.
