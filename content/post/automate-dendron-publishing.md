---
title: "🌱 Automate Dendron Publishing"
date: 2021-12-23T18:38:10+05:30
tags: ["hacking", "pkm"]
useMermaid: true
---

I have been using Dendron for some time now as a PKM tool.
I started when it was using 11ty to generate site out of the notes.

It would create the site in `docs` folder, using `Dendron: Site Build` in VSCode
I had mapped the `docs` folder to a git repo.

I would push ready-made site to git, and netlify deploy was a no-op
<!-- more -->

{{<mermaid>}}
%%{init: {'themeVariables':{'lineColor': 'lightblue', 'arrowheadColor': 'lightblue'}}}%%
graph LR;
    A{ Write Content in markdown }
    A -->| git push | B[ source git repo ]
    A -->| Dendron: Site Build | C[ Site in `docs` folder ]
    C -->| git push | D[ netlify deploy ]
{{</mermaid>}}


Then recently they switched to `next.js` and publishing changed a lot. (Personally I liked the old look and feel, but that is besides the point)

Now the `.next/out` would contain the Site. 

It would have been OK, changing from `docs` to `.next/out` is no big deal, but with next-based publishing, `out` folder gets removed on each build. 😱

I had tough time figuring out how to publish (I did some hacks, but not really proud of those 😅)

Then I came across a blog post on Dendron's discord channel.

Essentially they figured a way around this.

As a bonus, I don't have to maintain two git repos like before (Remember, I had a git repo just for the generated output)

The [steps](https://github.com/dendronhq/template.publish.netlify/blob/main/dendron-publish-site.sh) from the script are pretty straightforward.
Yet having such simple script and asking netlify to use it as a build command (and use `docs` folder to publish) does make the workflow more efficient.

-------

*If you have not, please check my [Digital Garden](https://pkm.desipenguin.com)*
