---
title: "Waydroid"
date: 2024-11-28T13:29:37+05:30
tags: ["tools"]
---

I wanted to use [Openvibe](https://openvibe.social/) - an app that allows
having a single timeline across multiple social media networks like mastodon and
bluesky. It also supports cross posting (and more)

But currently it only has mobile apps (for both iOS and Android)

While the Desktop app is "in the works" - no specifics are provided.

I do not use mobile apps if at all possible. (Except for podcast and messeging
apps to keep in touch with Family - **on the go**. If I'm at my desk, I use the
Desktop version)

So I thought I was out of luck, till I rememembered about Waydroid.

As it mentions on their site, it runs Android in a container.

Installation went well.

Then it downloaded two images. (`waydroid init` failed during the installation.
It needed `sudo` to work)

Both were lineageOS images.

Everything went smoothly.

But after Lineage OS loaded, I didn't know what to do.

Using mobile device like interface on a desktop is disorienting 😁

I opened the browser (one of the three icons shown on default opening screen)

Opened Google Play Store.

I assumed it would let me install Openvibe.

But it showed my other two (Physical) devices as targets.

I searched a little more, and realized I need to spend more time (As with
everything linux 😊)

(Preferred?) Way to install apps seems to be F-droid.

While I was "looking around" I noticed that this was Android 11. So maybe that
is why Openvibe did not offer to install on that device.

Finally, I `apt remove` ed Waydroid, and removed the downloaded images as well.

I may come back in future.

Couple of things to try (in future)

* Download F-droid APK and install from command line as `waydroid app install
xyz.apk`
* See if OpenVibe is available on F-droid.
  * Even if it is not, there may be other cool apps that I can use 🤞
* Figure out how to get (relatively) latest version of Android (At least 14)
* Figure out way to install `GAPPS` (Google Apps)

