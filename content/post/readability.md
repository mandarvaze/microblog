---
title: "Firefox Reader mode in your Terminal"
date: 2022-01-10T06:41:01+05:30
useMermaid: false
tags: ["published a blog post", "terminal"]
---
As I mentioned in an earlier post, I'm trying to move to more text based workflows.

On that journey, I came across `readable` which creates a clean version of a webpage, just like Firefox Reader mode would do. But without Firefox.

Which means, you can read cleaner website from your terminal (if you want)

Read [Firefox Reader mode in your Terminal](https://learnings.desipenguin.com/post/readability/)
