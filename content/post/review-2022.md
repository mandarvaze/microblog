---
title: "Year 2022 Review"
date: 2022-12-31T07:01:25+05:30
tags: ["misc", "meta"]
---

* **Blogging**
  * I started this blog in Dec 2021, and in year 2022 - I wrote 80+ posts 
and 8 microposts 🎉
  * Switched to theme that supports microposts
* **Editor**
  * Tried Neovim for few months and went down the rabbit hole
  * Till I found [Helix](/tags/helix/) - no/low configuration modal editor
  * Started using Helix Editor exclusively for writing these posts.
  * Still using Emacs for office work
  * Recently I'm using VSCode for work 🤷‍♂
* **Art**
  * Seems like I drew only one sketch (Analog) this entire year 😱
  * Purchased a graphics tablet (No screen)
  * Still getting used to [drawing digitally](/tags/art/)
* **Social Media**
  * Started using [Mastodon](/tags/mastodon/)
  * Also [Pixey](/post/pixey.org/)
  * Although I have not imported my Instagram posts yet
* **Work/Programming**
  * Switched (back to) Zsh after using Fish for a couple of years.
  * [Switched jobs](/post/left-stck.me/)
  * Now working in [Ruby](/tags/ruby/) for almost a year