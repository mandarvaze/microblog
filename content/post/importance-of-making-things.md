---
title: "Importance of Making Things"
date: 2024-05-22T17:18:50+05:30
tags: ["misc", "podcast"]
---

On the knowledge project podcast, Shane parrish interviewed [Kevin Kelly](https://fs.blog/knowledge-project-podcast/kevin-kelly/)

The link above has good high level overview with timestamps. Definitely check it out.

~~One thing that is not covered in the timestamps (hence I can't link to the exact conversation) is about **Importance of making things**~~ [^1]

Kevin says something to the effect of, don't stop making things (that you care about) just because they are not the greatest or the best.
Your "thing" may have influence (or use) for someone in the future in ways you can't imagine.

The examples he gives are :

* No one remembers who or when Piano was invented [^2], but if piano was not invented, Mozart would not be known as genius.
  * He may have made music with something else (if Piano didn't exist) and may not be well known as he is today.
  * Or he may not have made music at all.
* Similarly, no one remembers who or when film camera was invented, but everyone knows George Lucas
  * No movie camera -> No George Lucas
  * He may have become photographer (assuming still photography existed) or a painter
  * He may or may not have become such a success in his alternate career.

Point is : **make things, your work may support future geniuses**

[^1]: Found the interview on Youtube. This topic is discussed in the chapter [definition of success](https://www.youtube.com/watch?v=1OPB0bix6CQ&t=1808s) I realize that Kevin says it differently than what I have written above. But the gist is the same (and so are the examples)
[^2]: I'm sure we can search specifics of when piano (or movie camera) was invented. But that is not the point. We don;t need to search for names Mozart and George Lucas. **That is the point**. Also, I think both piano and movie camera were gradual inventions. Musical instrument similar to piano would have existed and piano we know today would have been an evolution. Same for still camera being stepping stone for movie camera.
