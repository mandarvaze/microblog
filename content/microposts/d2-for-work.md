+++
date = "2023-01-24T06:07:58+05:30"
+++

I used [D2](/tags/d2/) for work today.

So far it was just a curiosity.

But there was an opportunity to use D2 when I had to document an important 
data flow at work.

With [D2 Playground](/post/d2-playground/), it is much easier for my colleagues
to use same simple format to update the diagram in future when needed.