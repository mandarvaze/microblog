---
title: "Set Dendron After Os Upgrade"
date: 2023-05-01T08:09:45+05:30
---

Today I set my [PKM](pkm.desipenguin.com/) project on the laptop, a little
over a month after I [upgraded the OS](https://microblog.desipenguin.com/post/upgrade-to-monterey/)

I just had to clone the repo.

Since this was a new repo cloned, I had to set the `user.email` and `user.name`
git configurations.

It is good that `git` reminds me of that, and also informs me to amend the 
commit with `--reset-author` param. How nice!

---

For the curious, I added [this](https://pkm.desipenguin.com/notes/1563a89c-bcb2-4ef0-b647-45e9c468f43e/#change-output-format-to-csv) entry