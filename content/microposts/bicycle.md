+++
date = "2024-04-19T15:47:52+05:30"
+++

We have a bicycle which was unused for months.

I got it fixed (some oiling, check tyres, brakes etc.) for all of Rs. 90 😄

Today, I rode bicycle for the first time after decades.

Not for long. Maybe 15 minutes.

Since I was not used to it, my legs hurt 😄

But it felt great to ride a cycle after so many years.
