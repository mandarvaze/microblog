+++
date = "2022-12-11T11:42:47+05:30"
+++

[tldraw](https://beta.tldraw.com/) seems interesting.

Drawing program inside your browser.

While it does not have `Save as` functionality, it does have `Print` option
which can be paired with `Print to PDF`

I can live it that.

Need to try out.