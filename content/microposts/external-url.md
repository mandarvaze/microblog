+++
date = "2022-11-26T19:44:42+05:30"
+++

I just used `externalUrl` feature of this theme.
It is kinda cool.

Such post is indicated with a right arrow like →

The title of the blogpost itself is an external link. So when clicked, it 
does not open the post, but the external URL. 

In order to open the post, either click the infinity symbol next to it, or
the date (if on the yearly list page)
