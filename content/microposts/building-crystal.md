+++
date = "2023-03-13T10:29:51+05:30"
+++

I tried to `brew install crystal`

My first attempt seemed like hung, I had to `Ctrl-C` my way out.
Second attempt took 45 minutes.

I have a 7 years old MBP, it is fully functioning machine. In fact this is
being written on that machine. 

45 minutes+ seems like a lot