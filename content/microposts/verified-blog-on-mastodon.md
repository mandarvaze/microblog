---
title: "Verified Blog on Mastodon"
date: 2023-05-08T12:40:48+05:30
---

My [mastodon account](https://indieweb.social/@mandarvaze) is now verified with green tick 😄

I already had a link to my Mastodon account in the footer of this blog
for quite some. But did not have `rel=me` which I added today.

I also did not have a link to this blog from mastodon account - which I added
just now.

As a result, now I have `green check mark` 😄