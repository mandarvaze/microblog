+++
date = "2022-12-05T22:45:35+05:30"
+++

Sometimes it is said that notes are `write-only`
i.e. No one reads them later 🤷‍♂

The same happened with me today.

I was looking for way to get only certain file from `git stash`

I asked `ChatGPT` with mixed results. (It provided correct answers, but 
more verbose. On asking, better explanation was provided, still..)

During such time, stackoverflow shines.

Even without going to SO site, the answer is included right on the search results.

I decided that since it was hard to find, it is worth adding it to PKM.

Turns out I already had it [stored](https://pkm.desipenguin.com/notes/htIulptUQJdrsatC/#get-only-subset-of-files-from-stash)

🤦‍♂🙈

That is why it is called `write-only` 😆