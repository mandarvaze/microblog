+++
date = "2022-12-05T22:35:05+05:30"
+++

I came across fish like auto suggestions plugin for zsh.
When I started to update that, I noticed that my `oh-my-zsh` is two years old
(now that we are near the end of 2022, it is more like close to three)

So I uninstalled and reinstalled `oh-my-zsh`

But the prompt won't look nice.

That is when I realized that starship is not hooked to zsh

and that starship is also due for an update (6 months)

So upgraded `starship` and then hooked it up to `zsh`

Now I can try the autosuggestions plugin. ☺️ 