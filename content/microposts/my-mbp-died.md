+++
date = "2024-02-17T22:38:56+05:30"
+++

My Macbook Pro won't boot anymore :(

I was watching some Youtube videos in the evening, and then at night, it 
just became unresponsive.

Over the next few days, I tried various options.

Booted from external USB. Worked!

Tried Internet Recovery.

Turns out the SSD is recognised, but can't be fixed/formatted/erased.

Looks like a hardware issue.
