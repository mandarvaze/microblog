+++
date = "2024-04-29T21:36:02+05:30"
+++

I got myself a new mobile phone after almost 5 years. 

My old one is still going strong.

But there are no OS updates. I'm stuck at Android 10

Some random facts about the new phone - in no specific order

* It is 3 times the price of the old one
* It is 5G
  * I don't get 5G everywhere, but place where I did get 5G signal, I got 500MB+ download speed (as per speed test) 🤯
* It has Wi-Fi calling. Hopefully, my calls won't get dropped 🤞
  * This was one of the main reasons everyone around me was asking me to upgrade 😄

Since the phone is 5 years newer, it is better - by default - in so many categories.

I'm planning to put custom ROM on the older phone and give it to my kids, for occasional game or for web search for their studies etc.

