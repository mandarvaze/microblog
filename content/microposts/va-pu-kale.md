+++
date = "2023-05-10T06:26:20+05:30"
+++

मला व.पु. काळे ह्यांच्या कथा आवडतात कारण त्यात कथा आणि उपदेश हयाचा चांगला balance असतो.
नुसता उपदेश थोडा बरा वाटतो, पण नंतर bore होतं
हयाच मुळे मला रिचर्ड बाखचं लेखन सुदधा आवडतं

----

I like Va. Pu. Kale's stories because it has great balance of the story and philosophy.
Just philosophy is OK for a while, but too much of it can get boring.
I like Richard Bach's writing for the same reason
