+++
date = "2024-03-12T20:56:20+05:30"
+++

Finally, my Macbook Pro is back!

As part of fixing, they put OS X Yosemite on it. 😭

Practically nothing worked. For some reason, Apple ID login also did not work.
I checked on another machine, username/password was indeed correct.

There was no option to upgrade to macOS Monterey (last supported version on this hardware) from the App Store either.

App store listed macOS Sonoma, which I knew wasn't supported, but tried it anyway. As expected, it said "Not supported" (Duh!)

Finally, resorted to Internet Discovery.

It offered to install macOS Monterey, as expected.

All is well.

(It took some time, downloading etc. But otherwise pretty uneventful)
