+++
date = "2022-12-12T05:54:57+05:30"
+++

[Earlier](/microposts/tldraw/) I thought that tldraw can not save as other
image file formats. I was wrong.

There is indeed `Export as` option, and it works !